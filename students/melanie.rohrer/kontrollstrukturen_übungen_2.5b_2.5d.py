#übung 2.5b
# b. Erweitern Sie die Funktion, sodass diese zwischen geschäftsunfähig (bis
# inklusive 6 Jahre), unmündig (bis inklusive 14 Jahre), mündig minderjährig
# und volljährig unterscheidet. Weiters soll für ein negatives Alter ungeboren
# zurückgegeben werden.
# age = 5
# print("Mit", age, "ist man", legal_status(age) + ".")
# # Ausgabe: Mit 5 ist man geschäftsunfähig.

from turtle import *

def legal_status(age):
    if age <0:
        return "ungeboren"
    elif age <=6:
        return "geschäftsunfähig"
    elif age <=14:
        return "unmündig"
    elif age <18:
        return "minderjährig"   
    else:
        return "volljährig"
    
def volljaehrigkeit_test():
    age = int(numinput("Alter" , "Geben Sie das Alter ein."))
    print("Mit", age, "ist man", legal_status(age) + ".")
            
volljaehrigkeit_test()

# d. Schreiben Sie eine Schleife, welches von 10 bis 0 zählt und statt "0" das
# Wort "Start" ausgibt.

i = 10

while i > 0:
    print(i)
    
    i = i - 1
    
    if i == 0:
        print("Start")
    
    