# f. Schrieben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis
# deren Summe grosser als 10 ist. Im Anschluss soll die Summe ausgegeben
# werden.
# Bitte geben Sie Zahl 1 ein: 2
# Bitte geben Sie Zahl 2 ein: 3
# Bitte geben Sie Zahl 3 ein: 1
# Bitte geben Sie Zahl 4 ein: 8
# Summe: 14

from turtle import *

summe = 0

while summe <= 10:
    i = numinput("Zahl", "Bitte geben Sie Zahl ein")
    summe = summe + i
print(summe)