# g. Schreiben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis der
# Benutzer -1 eingibt und anschliessend die Summe dieser Zahlen ausgibt.
# Bitte geben Sie Zahl 1 ein: 2
# Bitte geben Sie Zahl 2 ein: 3
# Bitte geben Sie Zahl 3 ein: 7
# Bitte geben Sie Zahl 4 ein: -1
# Summe: 12
# 5

from turtle import *

summe = 0
i = 0

while i != -1:
    i = numinput("Zahl", "Bitte geben Sie Zahl ein")
    if i != -1:
        summe = summe + i
print(summe)