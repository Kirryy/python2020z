#übungen grundlagen datentypen 2.1&2.2 (preisliste)
#a Erstellen Sie ein Dictionary Preisliste, welches folgende Produkte mit
#den zugehörigen Preisen speichert: Brot ! 3.2, Milch ! 2.1, Orangen
# ! 3.75, Tomaten ! 2.2.
preisliste = {"Brot":3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}
print(preisliste)
#b Ändern Sie den Preis für Milch auf 2.05.
preisliste["Milch"] = 2.05
print(preisliste)
#c Entfernen Sie den Eintrag für Brot.
del preisliste["Brot"]
print(preisliste)
#d. Fügen Sie neue Einträge für Tee ! 4.2, Peanuts ! 3.9 und Ketchup
# ! 2.1 hinzu.
preisliste["Tee"] = 4.2
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1
print(preisliste)
#e Schreiben Sie ein Programm, welches Sie nach Lebensmittel und den zugeh
# örigen Preisen fragt und diese so lange einem Dictionary hinzufügt,
# bis der Benutzer x eingibt. Im Anschluss soll das Dictionary ausgegeben
# werden.

#leeres dictionary kreieren:
preisliste = {}
#applikation starten:
eingabe = True
while eingabe:
    lebensmittel = input("Lebensmittel:")
    preis = input("Preis:")
    #hinzufügen der items in das dictionary:
    preisliste[lebensmittel] = preis
    repeat = input("Weitere hinzufügen?")
    #ende der eingabe definieren:     
    if repeat == "x":
        eingabe = False 
print("Preisliste:")
# 2.2 a dictionary wird ausgegeben:
for lebensmittel, preis in preisliste.items():
    print(lebensmittel + " kostet " + preis + " CHF. ")





    