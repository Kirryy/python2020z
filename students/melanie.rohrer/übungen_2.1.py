#übungen 2.1
#a
from turtle import *
def dreieck(seitenlaenge, stiftfarbe, fuellfarbe):
    pensize(5)
    pencolor(stiftfarbe)
    fillcolor(fuellfarbe)
    begin_fill()
    right(90)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    end_fill()
    
dreieck(100, "red", "cyan")
right(30)
dreieck(100, "red", "lime")
right(30)
dreieck(100, "red", "yellow")