#Grundlagen Datentypen 2.2 a, e)

#a)

preisliste = {"Brot":3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}

preisliste["Tee"] = 4.2
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1
preisliste["Ei"] = 1.5


for lebensmittel, preis in preisliste.items():
    print (lebensmittel, "kostet", preis, "CHF.")
    
print("------------------")
    
#e) Lebensmittel <2 CHF
    
for lebensmittel, preis in preisliste.items():
    if preis <2:
        print (lebensmittel, "kostet", preis, "CHF.")

           