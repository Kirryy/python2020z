from turtle import *

#a) quadrat
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)

#weiter zur nächsten Zeichnung
penup()
forward(200)
right(270)
pendown()

#d) Dreieck
pensize(5)
pencolor("red")
forward(100)
left(120)
forward(100)
left(120)
forward(100)

#weiter zur nächsten Zeichnung
penup()
right(100)
forward(300)
pendown()

#c) Welle
pensize(5)
pencolor("blue")
left(270)
forward(50)
pencolor("red")
right(100)
forward(50)
pencolor("cyan")
left(100)
forward(50)
pencolor("black")
right(100)
forward(50)

#weiter zur nächsten Zeichnung
penup()
right(120)
forward(350)
pendown()

#e) 3 Dreiecke
pensize(5)
pencolor("blue")
right(190)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

penup()
left(120)
forward(100)
pendown()

#Dreieck grün
pencolor("lime")
right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

#Dreieck rot
pencolor("red")
right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)




