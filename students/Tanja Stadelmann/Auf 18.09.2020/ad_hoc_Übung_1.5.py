from turtle import *

#a) 3 Dreiecke
size1 = numinput("Abfragedialog",
"Bitte Seitenlänge angeben:")
 
pensize(5)
pencolor("red")
fillcolor("yellow")
begin_fill()
right(90)
forward(size1)
left(120)
forward(size1)
left(120)
forward(size1)
end_fill()

#nächstes Dreieck grün
size = numinput("Abfragedialog",
"Bitte Seitenlänge angeben:")

penup()
right(180)
forward(size1)
pendown()
fillcolor("lime")
begin_fill()
left(60)
forward(size)
left(120)
forward(size)
left(120)
forward(size)
end_fill()

#nächstes Dreieck cyan
size = numinput("Abfragedialog",
"Bitte Seitenlänge angeben:")

fillcolor("cyan")
begin_fill()
forward(size)
left(120)
forward(size)
left(120)
forward(size)
end_fill()


