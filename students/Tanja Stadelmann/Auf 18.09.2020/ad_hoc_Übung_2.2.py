from turtle import *

#ad hoc Übung (2.2)

def viereck(seitenlänge, zeichenfarbe, füllfarbe):
    pencolor(zeichenfarbe)
    fillcolor(füllfarbe)
    begin_fill()
    forward(seitenlänge)
    right(90)
    forward(seitenlänge)
    right(90)
    forward(seitenlänge)
    right(90)
    forward(seitenlänge)
    end_fill()

viereck(100, "red", "cyan")
viereck(50, "red", "lime")
viereck(200, "red", "yellow")




    