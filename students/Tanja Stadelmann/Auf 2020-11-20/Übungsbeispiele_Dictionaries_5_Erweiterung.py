#Übungsbeispiele Dictionaries_5_Erweiterung

#Bisherige Übung

berge={}
for x in range (1,4):
    berg_eingabe=input(str(x) + ". Berg?")
    höhe_eingabe= input(str(x) + ". Höhe?")
    höhe_eingabe = int(höhe_eingabe)
    berge[berg_eingabe]=höhe_eingabe
    
for berg, höhe in berge.items():
    print(berg, "ist", höhe, "m(" + str(höhe*3.28), "ft) hoch.")
    
#Erweiterung, in json speichern
    
print(berge)

from json import loads,dumps

with open ("berge.json", "w", encoding="utf8") as f:
    json_string = dumps (berge)
    f.write(json_string)
    
#Erweiterung, nur wenn Datei vorhanden, einlesen
    
try:
    berge = []
    with open("berge.json", encoding="utf8") as f:
        json_string = f.read()
        
except FileNotFoundError:
    berge = []
    


