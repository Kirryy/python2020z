#Einkaufsliste

#Menü erstellen

einkaufsliste = ["Bananen", "Gurken", "Peperoni", "Brot", "Käse"]

while True:
    print("Programmübersicht:")
    print("(1) artikel hinzufügen")
    print("(2) artikel löschen")
    print("(3) artikel suchen")
    print("(4) einkaufsliste leeren")
    print("(5) einkaufsliste speichern")
    print("(6) einkaufsliste laden")
    print("(7) einkaufsliste im csv format exportieren")
    print("(0) exit")
    eingabe = int(input("Eingabe...?"))
    
    #Programmstopp
    
    if eingabe == 0:
        break
    
    #Artikel hinzufügen

    elif eingabe == 1:   
        while True:
            artikel = input("Geben Sie einen Artikel ein, oder x für Exit")
            
            if artikel == "x":
                break
            
            else:
                einkaufsliste.append(artikel)

    print(einkaufsliste)
    
    #Artikel löschen
    
    einkaufsliste_zwei = list(einkaufsliste)
    
    if eingabe == 2: #hier wurde die elif eingabe nicht geschluckt, warum?
        while True:
            artikel_löschen = input("Geben Sie die zu löschenden Artikel an, oder x für Exit")
            
            if artikel_löschen == "x":
                break
            
            elif artikel_löschen in einkaufsliste:
                einkaufsliste.remove (artikel_löschen)
                
            else:
                print("Dieser Artikel ist nicht in der Einkaufsliste vorhanden")
                
    print(einkaufsliste)
    
    #Artikel suchen --> hier ist ein Fehler 
    
    if eingabe == 3:
        suchliste = []
        artikel_suchen = input("Geben Sie die zu suchenden Artikel oder die Buchstabenfolge an")
                
        if artikel_suchen == "x":
            break
                
        for buchstabe in einkaufsliste:
            if artikel_suchen == einkaufsliste:
                suchliste.append(artikel_suchen)
        
        print(suchliste)        
                
    #Einkaufsliste leeren
                
    einkaufsliste_drei = list(einkaufsliste)
                
    if eingabe == 4:
        del einkaufsliste_drei[:]
        
    print(einkaufsliste_drei)
    
    #Einkaufsliste speichern --> mit Fehler
    
#     if eingabe == 5:
#         with open ("einkaufsliste.txt", "w", encoding="utf8") as fileHandle:
#             fileHandle.write(einkaufsliste)
#             
#     print("Die Datei Einkaufsliste finden Sie nun im Laufwerk")


    #Einkaufsliste laden

#     if eingabe == 5:
#         with open ("einkaufsliste.txt", encoding="utf8") as fileHandle:
#             file_content = fileHandle.read()
#             
    #Einkaufsliste im csv Format exportieren
            
    if eingabe == 6:
        from csv import writer
        with open ("einkaufsliste.csv", "w", encoding="utf8") as f:
            csv_writer = writer(f, delimiter = ";")
            for item in einkaufsliste:
                csv_writer.writerow(item)
                

        
        
    
                

