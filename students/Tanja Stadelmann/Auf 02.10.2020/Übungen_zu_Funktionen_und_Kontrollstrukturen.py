# Übungsbeispiele zu Funktionen und Kontrollstrukturen

# Beispiel 2

def berechne_herzfrequenz(alter):
    herzfrequenz = (220 - alter)
    return herzfrequenz

herzfrequenz = berechne_herzfrequenz(25)
herzfrequenz = berechne_herzfrequenz(60)


print("Ihre Herzfrequenz:", herzfrequenz)

# Beispiel 3

def berechne_ticketpreis(alter, km):
    ticketpreis = 2 + (0.25 * km)
    if alter <= 6:
        return "0"
    elif alter <= 16:
        return ticketpreis / 2
    else:
        return ticketpreis

ticketpreis = berechne_ticketpreis (20, 10)

print ("Ihr Ticket kostet", ticketpreis, "Fr.")

# Beispiel 6

from turtle import *

def zeichne_rahmen(laenge, breite, rahmenfarbe, fuellfarbe):
    rahmenfarbe = pencolor
    fuellfarbe = fillcolor
    begin_fill()
    forward(laenge)
    right(90)
    forward(breite)
    right(90)
    forward(laenge)
    right(90)
    forward(breite)
    end_fill()
    
#ich bin mir nicht sicher, wie man die for-Schleife programmieren muss,
#damit es den inneren Rahmen auch zeichnet
    
for zeichne_rahmen in range (1):
    print zeichne_rahmen (laenge/10, breite/10, rahmenfarbe, fuellfarbe)
    
zeichne_rahmen = zeichne_rahmen(100,25, "black", "white")

# Beispiel 11 e)


abfolge = "  " * 7
rauten = "# " * 7

print (abfolge)

i=7
while i <= 7:
    print(abfolge)
    i = i + 1
    
print (abfolge)
    


    
   

    

