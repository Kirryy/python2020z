#Leistungsaufgabe 3

#a)

liste = []

while True:
    stadt = input("Geben Sie die Stadt ein, oder x für Exit: ")
    if stadt.lower() == "x":
        break
    einwohner = input("Geben Sie die zugehörige Einwohnerzahl ein: ")

    liste.append([stadt, einwohner])

liste.sort()

print(liste)

#b)

i = 0
x = 0

while x <10:
    #Vokabel 1 Hund
    hund = input("Was heisst Hund: ")
    if hund.lower() == "dog":
        print("RICHTIG")
        i = i + 1
    else:
        print("FALSCH!")
    x = x + 1
    #Vokabel 2 Katze
    katze = input("Was heisst Katze: ")
    
    if katze.lower() == "cat":
        print("RICHTIG")
        i = i + 1
    else:
        print("FALSCH!")
    x = x + 1
    #Vokabel 3 Huhn
    huhn = input("Was heisst Huhn: ")
    
    if huhn.lower() == "chicken":
        print("RICHTIG")
        i = i + 1
    else:
        print("FALSCH!")
    x = x + 1
    #Vokabel 4 Vogel
    vogel = input("Was heisst Vogel: ")
    
    if vogel.lower() == "bird":
        print("RICHTIG")
        i = i + 1
    else:
        print("FALSCH!")
    x = x + 1
    #Vokabel 5 Maus
    maus = input("Was heisst Maus: ")
    
    if maus.lower() == "mouse":
        print("RICHTIG")
        i = i + 1
    else:
        print("FALSCH!")
    x = x + 1
    #Vokabel 6 Schmetterling
    schmetterling = input("Was heisst Schmetterling: ")
    
    if schmetterling.lower() == "butterfly":
        print("RICHTIG")
        i = i + 1
    else:
        print("FALSCH!")
    x = x + 1
    #Vokabel 7 Kuh
    kuh = input("Was heisst Kuh: ")
    
    if kuh.lower() == "cow":
        print("RICHTIG")
        i = i + 1
    else:
        print("FALSCH!")
    x = x + 1
    #Vokabel 8 Kuh
    schwein = input("Was heisst Schwein: ")
    
    if schwein.lower() == "pig":
        print("RICHTIG")
        i = i + 1
    else:
        print("FALSCH!")
    x = x + 1
    #Vokabel 9 Pferd
    pferd = input("Was heisst Pferd: ")
    
    if pferd.lower() == "horse":
        print("RICHTIG")
        i = i + 1
    else:
        print("FALSCH!")
    x = x + 1
    #Vokabel 10 Meerschweinchen
    meerschweinchen = input("Was heisst Meerschweinchen: ")
    
    if meerschweinchen.lower() == "guinea pig":
        print("RICHTIG")
        i = i + 1
    else:
        print("FALSCH!")
    x = x + 1
    
print ("Sie haben", i, "von 10 Vokabeln richtig beantwortet!")
