# listen.pdf beispiel 3

eingabe = 99

while eingabe !="x" :
    eingabe = input("Welche Kalendertür wollen Sie öffnen (oder x für Exit): ")

    auswahlliste = ["Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball"]

    from random import choice
    
    zufallselement = choice(auswahlliste)
    
    if eingabe == "x":
        break
    
    print (zufallselement)
    
    