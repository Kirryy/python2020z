#author sarah brandenberger
#date 07.10.2020
#hausuebung 4

from turtle import *
from math import *
from random import randint


#Übung 10 a.)
def berechne_herzf():
    alter_2 = int(input("Geben Sie Ihr Alter ein"))
    print("Ihre maximale Herzfrequenz beträgt: ", 220 - alter_2)
  
def preis():
    alter_3 = int(input("Geben Sie Ihr Alter ein"))
    km = input("Geben Sie die Kilometer ein")
    if alter_3 < 6:
        print("Du reist noch gratis, dein Billettpreis beträg CHF 0")
    elif alter_3 < 16:
        print("Dein Billettpreis beträgt: CHF", (2 + km*0.25)/2)
    elif alter_3 >= 16:
        print("Ihr Billettpreis beträgt: CHF", 2 + km*0.25)

wahl = int(input("Geben Sie Ihre Wahl ein"))
while True:
    print("Menü")
    print("1 Herzfrequenz berechnen")
    print("2 Fahrpreis berechnen")
    print("0 zum verlassen des Programms")
    if wahl == 1:
        berechne_herzf()
        continue
    elif wahl == 2:
        preis()
        continue
    elif wahl == 0:
        exit()

#Übungsblatt 11 j.)
muster = "#"
abstand = " "
anzahl_muster = 7
anzahl_abstand = 0
counter = 0
while counter < 6:
    print(abstand*anzahl_abstand, muster*anzahl_muster, abstand*anzahl_abstand)
    anzahl_abstand = anzahl_abstand + 1
    anzahl_muster = anzahl_muster - 2
    counter = counter + 1

  
# ad hoc 2.9 b.)
dreiecke_zeichnen = 0
anzahl_dreiecke = numinput ("Reihen ", "Geben Sie die Anzahl Dreiecke ein: ")
while anzahl_dreiecke > dreiecke_zeichnen: 
        fillcolor("green") 
        begin_fill()
        forward(100)
        left(120)
        forward(100)
        left(120)
        forward(100)
        end_fill()
        penup()
        left(120)
        forward(200)
        pendown()
        dreiecke_zeichnen = dreiecke_zeichnen + 1


#ad hoc 2.9 c.)
dreiecke_zeichnen = 0
anzahl_reihen = 0
anzahl_dreiecke_reihe = numinput ("Reihen ", "Geben Sie die Anzahl Dreiecke pro Reihe ein: ")
reihen = numinput ("Reihen ", "Geben Sie die Anzahl Reihen: ")

def reihenabsatz():
    penup()
    left(270)
    forward(100)
    left(90)
    pendown()
    
def dreieck():
    fillcolor("green") 
    begin_fill()
    forward(100)
    left(120)
    forward(100)
    left(120)
    forward(100)
    end_fill()
    penup()
    left(120)
    forward(200)
    pendown()

while anzahl_dreiecke_reihe > dreiecke_zeichnen: 
    dreieck()
    dreiecke_zeichnen = dreiecke_zeichnen + 1
    while reihen > anzahl_reihen:
        reihenabsatz()
        anzahl_reihen = anzahl_reihen + 1


# ad hoc 3.1 b.) 
text = ("Wenn Fliegen hinter Fliegen fliegen")

for buchstabe in enumerate(text):
    print(buchstabe)
    if "f" == buchstabe:
        print (buchstabe)
    if "F" == buchstabe:
        print (buchstabe)

# ad hoc 3.1 d.) 
text = ("Das ist ein Text zum Lösen der Aufgabe")
einfügetext = ("Das ist der Einfügtext zur Aufgabe")
text = text.replace ("Das ist ein Text zum Lösen der Aufgabe", "Das ist der Einfügtext zur Aufgabe")
print(text, einfügetext)

