#author sarah brandenberger
#date 16.10.2020
#hausuebung 6

from turtle import *
from math import *
from random import randint

#übungsblatt: grundlagen datentypen 2.1 a
preisliste = {"Brot":3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}

#übungsblatt: grundlagen datentypen 2.1 b
preisliste["Milch"]=2.05
print(preisliste)

#übungsblatt: grundlagen datentypen 2.1 c
del preisliste ["Brot"]
print(preisliste)

#übungsblatt: grundlagen datentypen 2.1 d
preisliste["Tee"] =  4.2
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1
print(preisliste)

#übungsblatt: grundlagen datentypen 2.1 e
warenliste = {}
ware = ""
while ware != "x":
    print("Geben Sie einen neuen Warenposten und Preis ein")
    ware = input("Geben Sie eine neue Ware ein")
    preis = input("Geben Sie einen neuen Preis ein")
    break
    if ware == "x":
        break
    warenlise[ware] = preis
print(warenliste)       

#übungsblatt: grundlagen datentypen 2.2 a
for warenposten, preis in preisliste.items():
    print(warenposten, "kostet", preis, "CHF.")
  
#übungsblatt: grundlagen datentypen 2.2 e
preis = float(preis)
if preis <2.0:
    print(warenposten, "kostet", preis, "CHF.")

#übungsblatt: grundlagen datentypen 2.3 c
preisliste_1 = {"Brot":3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}
preisliste_2 = preisliste_1
def preisliste_filtern():
    if "en" in preisliste_2:
        return preisliste_2
        del preisliste_1[preisliste_2]
        print(preisliste_1)
             
preisliste_filtern()
#     

#übungsbeispiele zu listen 5

text = 'Heinz war heute in den Bergen. Es war eine lange Wanderung'
stopwords = ['der', 'die', 'das', 'in', 'auf', 'unter', 'ein',
             'eine', 'ist', 'war', 'es']
text = text.split()
text_1 = text

def stopword_filter():
    for stopwords in text_1:
        text.remove(stopwords)
    print(text)
    
stopword_filter()

#übungsbeispiele zu listen 7
dubletten = ['Gesundheit', 'Wanderung', 'Gesundheit','Gewandtheit', 'Wanderung']
#print(list(set(dubletten)))

dubletten_1 = dubletten

def unique():
    for i in dubletten_1:
        dubletten.remove(i)
    print(dubletten)
    
unique()
 
# übungsbeispiele zu dictionaries 2
wortstatstik = "Der Tag begann sehr gut! Der Morgen war schön."
wortstatstik = wortstatstik.split()

def word_stat():
    for i in wortstatstik:
        anzahl_wort = wortstatstik.count(i)
        print(i.lower(), ":", anzahl_wort)
  
word_stat()

# übungsbeispiele zu dictionaries 3
icao = {"a":"Alfa", "b":"Bravo", "c":"Charlie", "d":"Delta", "e":"Echo", 
        "f":"Foxtrot", "g":"Golf", "h":"Hotel", "i":"India", "j":"Juliett", 
        "k":"Kilo", "l":"Lima", "m":"Mike", "n":"November", "o":"Oscar", 
        "p":"Papa", "q":"Quebec", "r":"Romeo", "s":"Sierra", "t":"Tango", 
        "u":"Uniform", "v":"Victor", "w":"Whiskey", "x":"X-Ray", "t":"Tango",
        "y":"Yankee", "z":"Zulu"}

eingabe = input("Ihr Wort im ICAO ABC geschrieben: ")

def buchstabieren (eingabe, icao):
    eingabe = eingabe.lower()
    buchstabe = ""
    for buchstabe in eingabe:
        if buchstabe in icao:
                buchstabe = icao[buchstabe]
                print(buchstabe)
    return buchstabe
    print(buchstabe)
buchstabieren(eingabe, icao)


# übungsbeispiele zu dictionaries 3 a.   
eingabe = input("Ihr Wort im ICAO ABC geschrieben: ")

def buchstabieren (eingabe, icao):
    eingabe = eingabe.lower()
    buchstabe = ""
    for buchstabe in eingabe:
        if buchstabe not in icao:
            print("das ist kein ICAO Zeichen")
        else: 
                buchstabe = icao[buchstabe]
                print(buchstabe)
    return buchstabe
    print(buchstabe)
buchstabieren(eingabe, icao)

# übungsbeispiele zu dictionaries 3 b.   
eingabe = input("Ihr Wort im ICAO ABC geschrieben: ")

def buchstabieren (eingabe, icao):
    eingabe = eingabe.lower()
    buchstabe = ""
    for buchstabe in eingabe:
        if eingabe == "ä":
            buchstabe = "Alpha - Echo"
            print(buchstabe)
        elif eingabe == "ö":
            buchstabe = "Oscar - Echo"
            print(buchstabe)
        elif eingabe == "ü":
            buchstabe = "Uniform - Echo"
            print(buchstabe)
        elif buchstabe not in icao:
            print("das ist kein ICAO Zeichen")
        else: 
                buchstabe = icao[buchstabe]
                print(buchstabe)
    return buchstabe
    print(buchstabe)
buchstabieren(eingabe, icao)
                
#ad hoc übung 3.3 c
städte = ({"Zürich":370000, "Genf":190000, "Uster":45000})


for i in sorted(städte.values()):
    print (i)