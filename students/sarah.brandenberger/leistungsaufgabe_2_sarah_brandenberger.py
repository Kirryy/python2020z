#author sarah brandenberger
#date 23.09.2020
#leistungsaufgabe 2


from math import *


#leistungsaufgabe 2 a.)
while True: 
    betrag = input("Geben Sie den Betrag ein: ")
    if betrag == "x":
        exit()
    betrag = int(betrag)
    if betrag <=99:
        print("Der Gesamtbetrag ist", betrag, "leider kein Rabatt")
        continue
    elif betrag >=1000:
        betrag = betrag - (betrag * 0.1)
        print("Der Gesamtbetrag ist", betrag, "der Rabatt beträgt 10 %")
        continue
    elif betrag <1000:
        betrag = betrag - (betrag * 0.05)
        print("Der Gesamtbetrag ist", betrag, "der Rabatt beträgt 5 %")
        continue

#leistungsaufgabe 2 b.)
for i in range (1,11):

    print("\n")
    print(i,"er Reihe:") 
    for i in range (1,11):
        print(str(int(i)), "x", str(int(i)), "=", i*i)

