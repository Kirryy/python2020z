#author sarah brandenberger
#date 14.11.2020
#hausuebung 8

from turtle import *
from math import *
from random import randint
from urllib.request import urlopen
from csv import *
from json import loads, dumps

#1.
with urlopen('https://www.ietf.org/rfc/rfc2616.txt') as website_einlesen:
    web_content = website_einlesen.read().decode('utf8')
def word_stat(web_content):
    web_content=web_content.split()  
    häufigkeit = {}
    for wort in web_content:     
        wort = wort.lower()
        if wort not in häufigkeit:
            häufigkeit[wort]=0 
        häufigkeit[wort] = häufigkeit[wort] + 1
    for daswort, anzahl in häufigkeit.items():
        print(daswort, anzahl)
    print(häufigkeit)

word_stat(web_content)

with urlopen('https://www.ietf.org/rfc/rfc2616.txt') as website_einlesen:
    web_content = website_einlesen.read().decode('utf8')
def zeichen_stat(web_content):
    web_content=list(web_content)  
    häufigkeit = {}
    buchstaben = "abcdefghijklmnopqrstuwvxyz"
    buchstaben = list(buchstaben)
    for zeichen in web_content:   
        zeichen = zeichen.lower()
        if zeichen in buchstaben:
            if zeichen not in häufigkeit:
                häufigkeit[zeichen]=0 
            häufigkeit[zeichen] = häufigkeit[zeichen] + 1
    for daszeichen, anzahl_1 in häufigkeit_1.items():
        print(daszeichen, anzahl_1)

zeichen_stat(sorted(web_content))
# 
# 
#2.
with urlopen('https://www.ietf.org/rfc/rfc2616.txt') as website_einlesen:
    web_content = website_einlesen.read().decode('utf8')
def word_stat(web_content):
    web_content=web_content.split()  
    häufigkeit = {}
    for wort in web_content:     
        wort = wort.lower()
        if wort not in häufigkeit:
            häufigkeit[wort]=0 
        häufigkeit[wort] = häufigkeit[wort] + 1
    for daswort, anzahl in häufigkeit.items():
        print(daswort, anzahl)
    with open ("wortstatistik.csv", "w", encoding="utf8") as csv_sichern:
        csv_writer = writer(csv_sichern, delimiter=';')
        csv_writer.writerow(häufigkeit)  

word_stat(web_content)

with urlopen('https://www.ietf.org/rfc/rfc2616.txt') as website_einlesen:
    web_content = website_einlesen.read().decode('utf8')
def zeichen_stat(web_content):
    web_content=list(web_content)  
    häufigkeit_1 = {}
    buchstaben = "abcdefghijklmnopqrstuwvxyz"
    buchstaben = list(buchstaben)
    for zeichen in web_content:   
        zeichen = zeichen.lower()
        if zeichen in buchstaben:
            if zeichen not in häufigkeit_1:
                häufigkeit_1[zeichen]=0 
            häufigkeit_1[zeichen] = häufigkeit_1[zeichen] + 1
    for daszeichen, anzahl_1 in häufigkeit_1.items():
        print(daszeichen, anzahl_1)
    with open ("buchstabenstatistik.csv", "w", encoding="utf8") as csv_sichern:
        csv_writer = writer(csv_sichern, delimiter=';')
        csv_writer.writerow(häufigkeit_1)

zeichen_stat(sorted(web_content))

#3.
while True:
    
    print("Geben Sie 1 zum laden eines Webseitentexts und 2 zum laden eines .txt-Files ein oder x zum Beeenden des Programms")
    wahl = int(input())
    if wahl == 1:
        print("Geben Sie die Webquelle ein, die Bezeichnung mit https:// als Start eingeben")
        webquelle = input()
        with urlopen(webquelle) as website_einlesen:
            web_content = website_einlesen.read().decode('utf8')
        word_stat(web_content)

        continue
    if wahl == 2:
        print("Geben Sie die Text ein, die Endung ist .txt")
        textfilequelle = input()
        with open(textfilequelle, encoding='utf8') as textfile_einlesen:
            web_content  = textfile_einlesen.read()
        word_stat(web_content) 
        continue
    
        

   
