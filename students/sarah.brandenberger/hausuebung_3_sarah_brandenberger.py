#author sarah brandenberger
#date 30.09.2020
#hausuebung 3

from turtle import *
from math import *
from random import randint


#Übungsblatt Grundlagen Kontrollstrukturen
#Aufgabe 2.5 a.)

alter = input()
alter = int(alter)
if alter <= 17:
    status = "minderjährig"
else:
    status = "volljährig"

print("Mit", alter, "ist man", status)

#Übungsblatt Grundlagen Kontrollstrukturen
#Aufgabe 2.5 b.)

alter_1 = input()
alter_1 = int(alter_1)
if alter_1 < 0:
    status = "ungeboren"
elif alter_1 <= 6:
    status = "geschäftsunfähig"
elif alter_1 <= 14:
    status = "unmündig"
elif alter_1 <= 17:
    status = "mündig minderjährig"
else:
    status = "volljährig"

print("Mit", alter_1, "ist man", status)


#Übungsbeispiele moodle
#Aufgabe 2

def berechne_herzf():
    alter_2 = numinput("Alter", "Geben Sie Ihr Alter ein")
    print("Ihre maximale Herzfrequenz beträgt: ", 220 - alter_2)
berechne_herzf()   

#Übungsbeispiele moodle
#Aufgabe 3

def preis():
    alter_3 = numinput("Alter", "Geben Sie Ihr Alter ein")
    km = numinput("Kilometer", "Geben Sie die Kilometer ein")
    if alter_3 < 6:
        print("Du reist noch gratis, dein Billettpreis beträg CHF 0")
    elif alter_3 < 16:
        print("Dein Billettpreis beträgt: CHF", (2 + km*0.25)/2)
    elif alter_3 >= 16:
        print("Ihr Billettpreis beträgt: CHF", 2 + km*0.25)
preis()   


#Übungsbeispiele moodle
#Aufgabe 6

def rahmen(laenge, breite, fuellfarbe, rahmenfarbe):
    pencolor(rahmenfarbe)
    fillcolor(fuellfarbe)
    begin_fill()
    forward(laenge)
    left(90)
    forward(breite)
    left(90)
    forward(laenge)
    left(90)
    forward(breite)
    left(90)
    end_fill()
    
rahmen(100, 200, "red", "blue")


#Übungsbeispiele moodle
#Aufgabe 11 e.)
muster = "#"
abstand = " "
counter = 0
print(muster*7)
while counter < 5:
    print(muster, abstand*3, muster)
    counter = counter + 1
print(muster*7)

print("/n")


#Übungsbeispiele moodle
#Aufgabe 11 g.)
print(muster*7)
counter = 0
anzahl_abstand = 5
while counter < 5:
    print(" "*anzahl_abstand, end="")
    print(muster)
    anzahl_abstand = anzahl_abstand - 1
    counter = counter + 1
print(muster*7)


#ad hoc 2.6 c.)
a = 12
b = 23
c = 5
d = 56
e = 77
f = 18
g = 9

for zahl in (a, b, c, d, e, f, g):
    if a >= zahl: 
        print("Die grösste Zahl ist: ", a)
    elif b >= zahl: 
        print("Die grösste Zahl ist: ", b)
    elif c >= zahl: 
        print("Die grösste Zahl ist: ", c)
    elif d >= zahl: 
        print("Die grösste Zahl ist: ", d)
    elif e >= zahl: 
        print("Die grösste Zahl ist: ", e)
    elif f >= zahl: 
        print("Die grösste Zahl ist: ", f)
    elif g >= zahl: 
        print("Die grösste Zahl ist: ", g)
        
        
for zahl in (a, b, c, d, e, f, g):      
    if a <= zahl: 
        print("Die kleinste Zahl ist: ", a)
    elif b <= zahl: 
        print("Die kleinste Zahl ist: ", b)
    elif c <= zahl: 
        print("Die kleinste Zahl ist: ", c)
    elif d <= zahl: 
        print("Die kleinste Zahl ist: ", d)
    elif e <= zahl: 
        print("Die kleinste Zahl ist: ", e)
    elif f <= zahl: 
        print("Die kleinste Zahl ist: ", f)
    elif g <= zahl: 
        print("Die kleinste Zahl ist: ", g)
        
for zahl in (a, b, c, d, e, f, g):
    print("Der Durchschnitt ist: ", zahl/7)


#ad hoc 2.8
for i in range(6):
    print("Zufallszahl:", randint(1, 43))
