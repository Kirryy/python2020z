#author sarah brandenberger
#date 29.10.2020
#leistungsaufgabe 3

from turtle import *
from math import *
from random import *

städte = {}
while True:
    stadt = input ("Geben Sie den Namen der Stadt ein, oder x zum beenden")
    if stadt == "x":
        for stadt, einwohnerzahl in sorted(städte.items()): 
            print(stadt, einwohnerzahl)
        break
    if stadt != "x":
        einwohnerzahl = input ("Geben Sie die Einwohnerzahl der Stadt ein")
        städte[stadt] = einwohnerzahl
        continue


wörterbuch = {"Hund": "dog", "Katze": "cat", "Huhn": "chicken", "Pferd": "horse", "Löwe": "lion", "Kuh": "cow", "Schaf": "sheep", "Vogel": "bird", "Fisch": "fish", "Maus": "mouse"}

zähler = 0
zähler_richtige = 0

while True:
    for deutsch, englisch in wörterbuch.items():
        print("Was heisst", deutsch, "?")
        antwort = input()
        if antwort == "x": 
            print("Sie haben", zähler_richtige, "von", zähler, "Vokabeln richtig beantwortet")
            break
        if antwort == englisch: 
            zähler = zähler + 1
            zähler_richtige = zähler_richtige + 1
            print("Richtig")
            continue
        elif antwort != englisch: 
            zähler = zähler + 1
            print("Falsch")
            continue       


