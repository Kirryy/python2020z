#author sarah brandenberger
#date .08.2020
#hausuebung_1

from turtle import *
from math import * 


#ad hoc Übung 1.1 a.)
Name = "Brandenberger"
Vorname = "Sarah"
Strasse = "Gschwaderstrasse"
Hausnummer = "25"
PLZ = "8610"
Wohnort = "Uster"


print(Name + ",", Vorname + ",", Strasse + ",", Hausnummer + ",", PLZ + ",", Wohnort)

#ad hoc Übung 1.1 b.)
print(sum(range(1,6)))
print(1-2-3-4-5)
#print(prod(range(1,6)))
print(1*2*3*4*5)
print(1/2/3/4/5)

#ad hoc Übung 1.2 c.)
left(45)
pensize(5)
pencolor("blue")
forward(100)
left(270)

pencolor("red")
forward(100)
left(90)

pencolor("cyan")
forward(100)
left(270)

pencolor("black")
forward(100)

penup()
forward(200)
pendown()

#ad hoc Übung 1.2 e.)
left(45)
pencolor("red")
pensize(5)

forward(100)
left(120)
forward(100)
left(120)
forward(100)

left(240)

pencolor("blue")
pensize(5)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

left(240)

pencolor("lightgreen")
pensize(5)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

left(90)
penup()
forward(300)
pendown()
left(255)

#ad hoc Übung 1.2 e.)
pencolor("red")
pensize(5)

fillcolor("blue")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
end_fill()
left(72)

fillcolor("magenta")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
end_fill()
left(72)

fillcolor("yellow")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
end_fill()
left(72)

fillcolor("turquoise")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
end_fill()
left(72)

fillcolor("lightgreen")
begin_fill()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
end_fill()
left(72)
    
penup()
forward(200)
pendown()
left(45)

#ad hoc Übung 1.5 a.)
laenge = numinput("Seitenlänge", "Bitte Seitenlänge des 1. Dreiecks angeben: ")

fillcolor("yellow")
begin_fill()
pencolor("red")
pensize(5)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()
left(120)


laenge = numinput("Seitenlänge", "Bitte Seitenlänge des 2. Dreiecks angeben: ")

fillcolor("turquoise")
begin_fill()
pencolor("red")
pensize(5)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()
left(120)

laenge = numinput("Seitenlänge", "Bitte Seitenlänge des 3. Dreiecks angeben: ")

fillcolor("lightgreen")
begin_fill()
pencolor("red")
pensize(5)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
end_fill()



