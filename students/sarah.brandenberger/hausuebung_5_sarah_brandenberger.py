#author sarah brandenberger
#date 10.10.2020
#hausuebung 5

from turtle import *
from math import *
from random import randint

#ad hoc 3.2 a.)
gemüse = ["broccoli", "karotten", "tomaten", "blumenkohl"]
element="karotten"

def allIndex(gemüse, element):
    return list(i for i in range(len(gemüse)) if gemüse[i] == element)
    
print(allIndex(gemüse, element))


einkaufsliste = ["Äpfel", "Birnen", "Butter", "Brot"]

def mein_einkauf():
    print(sorted(einkaufsliste))
   
mein_einkauf()


#übungsblatt grundlagen datentypen 1.2 c
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]

for i in jahreszeiten:
    print(i, i[-2:])
 

#übungsblatt grundlagen datentypen 1.2 d
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
    
for i in jahreszeiten:
    if i[-2:] =="er":
        print(i)
        
        
#übungsblatt grundlagen datentypen 1.3 c     
abkürzungen = ["zB", "MfG", "evt", "ca"]

for i in abkürzungen:
       if len(i) < 3:
           abkürzungen.remove(i)
print(abkürzungen)
        
           
# übungsaufgabe zu listen 1      
junge = ['der', 'junge', 'ist', 'sehr', 'freundlich']

def listenfilter():
    for i in junge:
        if len(i) <= 4:
            junge.remove(i)
    print(junge)

listenfilter()
  
           
# übungsaufgabe zu listen 3
überraschung = ["Samiklaus", "Christbaum", "Weihnachtskugel",
                "Stiefel", "Schneeball"]
while True:
    wahl = input("Welche Kalendertur wolle Sie öffnen (oder x fur Exit):")
    if wahl == "x":
        exit()
    wahl = int(wahl)
    if wahl < 25:
        print(überraschung[randint(0,4)])
        continue

# übungsaufgabe zu listen 4
endung = ['Gesundheit', 'Wanderung', 'Heiterkeit',
'Gewandtheit', 'Lustig']

for i in endung:
    if i[-4:] =="heit":
        print(i)

