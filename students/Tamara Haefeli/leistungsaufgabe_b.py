#Autor: Tamara Haefeli
#Datum: 17.09.2020
#leistungsaufgabe_b.py

from turtle import *

reset()


#Eingabefenster für Grösse der Flagge
size = numinput("Grösse","Bitte gib die Länge der Flagge ein:")

speed(10)          #schnell(st)e Zeichengeschwindigkeit
ht()               #Pfeil verstecken (hideturtle)
pu()               #Stift anheben (penup)

pencolor("red")    #Stiftfarbe rot
fillcolor("red")   #Füllfarbe rot

fd(size/2)         #geradeaus die Anfangsstelle anvisieren (forward)


#roter Hintergrund
pd()
lt(90)
begin_fill()       #Beginn des Füllens
fd(size/2)
lt(90)
fd(size)
left(90)
fd(size)
lt(90)
fd(size)
lt(90)
fd(size/2)
end_fill()         #rote Füllung ausführen/beenden


# Variablen definieren für das weisse Kreuz
zahl1 = (size/6)
zahl2 = ((size-(3*zahl1))/2)
zahl3 = ((zahl1)/2)


#weisses Kreuz
pu()
left(90)
fd(zahl1)
pd()
pencolor("white")
fillcolor("white")
begin_fill()
rt(90)
fd(zahl3)
lt(90)
fd(zahl2)
rt(90)
fd(zahl2)
lt(90)
fd(zahl1)
lt(90)
fd(zahl2)
rt(90)
fd(zahl2)
lt(90)
fd(zahl1)
lt(90)
fd(zahl2)
rt(90)
fd(zahl2)
lt(90)
fd(zahl1)
lt(90)
fd(zahl2)
rt(90)
fd(zahl2)
lt(90)
fd(zahl3)
end_fill()

exitonclick()     #schliesst das Fenster bei Klick auf die Grafik

