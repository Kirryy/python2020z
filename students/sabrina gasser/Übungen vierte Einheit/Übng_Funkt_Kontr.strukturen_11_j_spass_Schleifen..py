#Übung 11.j

rauten = "# " * 11
leerzeichen = "  " * 11

i = 1
x = 12
z = 10

while x > 0:
    print(leerzeichen[:i] + rauten[:x] + rauten[:z])
    i = i + 2
    x = x - 2
    z = z - 2
    
#überlegung: der eine Teil ist wie Aufgabe c (also alles ohne die "z"-Bestandteile.
#würde man die z-Bestandteile weglassen, funktioniert die Aufgabe auch (sieht eben aus wie Aufgabe c)
#mit den z habe ich rechts aufgefüllt