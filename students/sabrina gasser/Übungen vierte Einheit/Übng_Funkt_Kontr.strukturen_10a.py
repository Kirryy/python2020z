#angepasste Aufgabe
#gemäss E-Mail nur für die Berechnung von Herzfrequenz und Billetpreis.

eingabe = 1
while eingabe != 0:
    print("========================================================")
    print("Programmübersicht")
    print("1...Preis für eine Fahrkarte berechnen")
    print("2...Berechne Herzfrequenz")
    print("     ")
    print("0...Programm beenden")
    print("========================================================")
    eingabe = int(input("Bitte wählen Sie das Programm:....."))
    
    if eingabe == 1:
        print(" ")
        print(" ")
        print("Gewählte Option: 1")
        print(" ")
        print("Preis für die Fahrkartenberechnung")
        print("--------------------------------------------------------")
        
        alter = input("Wie alt sind sie?:...")
        entf = input("Wie weit fahren sie?:...")
        
        def get_billet_preis(alter, entf):
            alter=int(alter)
            entf=int(entf)
            
            preis = 2 + (entf * 0.25)

            if alter <= 6:
                billet = str(0)
            elif alter <= 16:
                billet = str(int(preis/2))
            else:
                billet = str(int(preis))
    
            return "Dein Billet kostet CHF " + str(billet)

        print(get_billet_preis(alter, entf))
        print(" ")
        print(" ")

    elif eingabe == 2:
        print(" ")
        print(" ")
        print("Gewählte Option: 2")
        print(" ")
        print("Berechne Herzfrequenz")
        print("--------------------------------------------------------")
        zahl = input("Wie alt sind Sie...?")

        def berechne_herzfrequenz(zahl):
            zahl = int(zahl)
            max_freq = 220 - zahl
            return "Deine max. Herzfrequenz ist " + str(max_freq)

        print(berechne_herzfrequenz(zahl))

        print(" ")
    elif eingabe == 0:
        print(" ")
        print("Sie haben das Programm verlassen")    
    else:
        print(" ")
        print(" ")
        print("--------------------------------------------------------")
        print("Fehlerhafte Eingabe. Bitte korrekter Nummer eintragen.")
        print("--------------------------------------------------------")
        print(" ")
eingabe
