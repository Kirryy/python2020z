from turtle import *

speed(10)

#Leistungsaufgabe 1 b)
#ebenfalls ein Schweizerkreuz aber mit Numinput

#Gehe ein wenig mehr in die Ecke oben links, damit man die Flagge guet sieht.
penup()
back(200)
left(90)
fd(200)
right(90)
pendown()

#rotes Hintergrund-Quadrat

laenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
laenge = int(laenge)

begin_fill()
fillcolor("red")
pencolor("red")

forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)
forward(laenge)
right(90)

end_fill()

penup()
#Hebe Stift um in den Innenteil zu wechseln und dort das Kreuz zu zeichnen

#Hinweis zur Länge: die Seitenlänge sind ein Fünftel der Gesamtlänge. Daher jeweils die Länge durch 5.
#Ohne den Platzhalter "laenge/5" wäre sonst bei Anpassung der roten Aussenfläche das Kreuz nicht mehr mittig.
#Für den Start des Kreuz muss zu 3/5 der Fläche gesprungen werden, daher laenge/5*3 
forward(laenge/5*3)
right(90)
forward(laenge/5)
pendown()

#Starte mit weissem Kreuz
begin_fill()
fillcolor("white")
pencolor("white")
forward(laenge/5)
left(90)
forward(laenge/5)
right(90)

forward(laenge/5) #seite rechts 

right(90)
forward(laenge/5)
left(90)
forward(laenge/5)
right(90)

forward(laenge/5)  #seite unten

right(90)
forward(laenge/5)
left(90)
forward(laenge/5)
right(90)

forward(laenge/5)  #seite links

right(90)
forward(laenge/5)
left(90)
forward(laenge/5)
right(90)

forward(laenge/5)  #seite oben

end_fill()

hideturtle()