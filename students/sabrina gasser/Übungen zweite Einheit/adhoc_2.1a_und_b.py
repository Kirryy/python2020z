#Autor: Sabrina Gasser
#Datum: 19.09.2020

from turtle import *

# Übung 2.1 a - Aufgabe bis zum 25.09.2020

# globale Definition weil für alle dieselbe Stiftfarbe und -dicke.
pencolor("red")
pensize(5)
speed(10)

# Definition der Dreiecke
def dreieck(seitenlänge, füllfarbe):
    fillcolor (füllfarbe)
    
    begin_fill()
    forward(seitenlänge)
    right(120)
    forward(seitenlänge)
    right(120)
    forward(seitenlänge)
    end_fill()
    
# Sprung nach rechts 

right(30) # einmalige Ausrichtung für den Start, damit Richtung korrekt ist

dreieck(100, "cyan")
dreieck(100, "lime")
dreieck(100, "yellow")


# Sprung nach rechts 
left(30)
penup()
fd(300)
pendown()



# Übung 2.1 b  (war kein Auftrag)


left(45) #wichtig, dass hier zuerst Anfangswinkel korrekt ist

def viereck(seitenlänge, zeichenfarbe, füllfarbe):
    pencolor (zeichenfarbe)
    fillcolor (füllfarbe)
    
    begin_fill()
    forward(seitenlänge)
    right(90)
    forward(seitenlänge)
    right(90)
    forward(seitenlänge)
    right(90)
    forward(seitenlänge)
    right(165)
    end_fill()
    

viereck(100, "red", "cyan")
viereck(100, "red", "yellow")
viereck(100, "red", "magenta")
viereck(100, "red", "blue")
viereck(100, "red", "lime")