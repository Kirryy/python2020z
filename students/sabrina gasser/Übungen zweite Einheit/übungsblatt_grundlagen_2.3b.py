#2.3 Funktionen mit Ruckgabewert und Parameter
#b) Schreiben Sie eine Funktion, welche die Länge und Breite eines Rechtecks entgegennimmt und die Fläche des Rechtecks an das Programm zurü̈ckgibt.
#Sabrina Gasser
#21.09.2020

def flaeche_rechteck(laenge, breite):
    laenge = int(laenge)
    breite = int(breite)
    flaeche = laenge * breite
    return "Die Fläche beträgt " + str(flaeche)+ " m2"

print(flaeche_rechteck(10,20))
