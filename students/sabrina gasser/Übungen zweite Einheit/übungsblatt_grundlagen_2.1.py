#2.1 Funktionen ohne Rückgabewert und Parameter
#Schreiben Sie eine Funktion guten morgen, welche den Text Guten Morgen! auf dem Bildschirm ausgibt.
#Sabrina Gasser
#21.09.2020

def guten_morgen():
    return "Guten Morgen!"

print(guten_morgen())

    