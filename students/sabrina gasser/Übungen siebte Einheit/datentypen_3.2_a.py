#ursprüngliches Telefonbuch von 3.1 als Liste zusammengefasst und ausgegeben
einträge = [{"Vorname": "Ana", "Nachname": "Skupch", "Phone": "123"}, {"Vorname":"Tim", "Nachname": "Kurz", "Phone":"732"}, {"Vorname": "Julia", "Nachname": "Lang", "Phone": "912"}]
print("Telefonbuch: ", einträge)
print("----")

#1. Überlegung: so könnte ich einfach den gesamten ersten Eintrag ausgeben. Ist dabei aber ein Dictionary. Und eine Funktion fehlt auch.
#print(einträge[0])


def datensatz_ausgeben(einträge):
    #Erklärung: damit auch wirklich "Vorname" usw steht, muss ich für jeden einzelnen einen Printbefehl eintragen
    #über einträge[...] wird der entsprechende Eintrag geholt, der nach dem Doppelpunkt steht im Dictionary
    print("Vorname: ", einträge["Vorname"])   
    print("Nachname: ", einträge["Nachname"])
    print("Phone: ", einträge["Phone"])

datensatz_ausgeben(einträge[0])
#über die Null hole ich den ersten Eintrag im Dic. Mit der [1] wäre es Tim, mit der [2] Julia)
