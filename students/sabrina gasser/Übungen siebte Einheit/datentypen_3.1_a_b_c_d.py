#3.1
#a
ana = {"Vorname": "Ana", "Nachname": "Skupch", "Phone": "123"}
print(ana)

#b  
telefonbuch = []
telefonbuch.append(ana)

print("Telefonbuch:", telefonbuch)

#c

neue_einträge = {"Vorname":"Tim", "Nachname": "Kurz", "Phone":"732"}, {"Vorname": "Julia", "Nachname": "Lang", "Phone": "912"}
telefonbuch.append(neue_einträge)

print(telefonbuch)


#d

telefonbuch = []
while True:
    vorname = input("Vorname: ")
    if vorname == "x":
        break
    nachname = input("Nachname: ")
    phone = input("Phone: ")
        
    daten = {"Vorname": vorname, "Nachname": nachname, "Phone": phone}
    telefonbuch.append(daten)
print("Telefonbuch: ", telefonbuch)
