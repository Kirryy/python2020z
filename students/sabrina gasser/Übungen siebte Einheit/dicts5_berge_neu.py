#Aufgabe 5 - Berge

auflistung = {}

#while Schleife
i=0
nr = 1
while i<3:
    berg = input(str(nr)+" Gib einen Berg ein: ")
    höhe = float(input("Gib die Höhe ein: ")) #wollte hier auch str(nr) eintragen für die Aufzählung, aber gab immer Fehler..
    auflistung[berg] = höhe
    i=i+1
    nr = nr + 1

for berg, höhe in sorted(auflistung.items()):
    print(berg, "ist ", höhe, "m (", int(höhe * 3.28), "ft) hoch")  #int weil sonst viele Kommastellen
    
    
#hinweis für mich: hatte zuerst jede Zeile als Print dargestellt.
#dies weil die "for berg, höhe..." mit Print zeurst eingerückt war. dadurch gab es diesen "Fehler"
   
 
 
# #hier noch als for-Schleife
# for aufzählung in range(1,4):
#     berg = input(" Gib einen Berg ein: ")
#     höhe = float(input("Gib die Höhe ein: ")) #wollte hier auch str(nr) eintragen für die Aufzählung, aber gab immer Fehler..
#     auflistung[berg] = höhe
#     
# 
# for berg, höhe in sorted(auflistung.items()):
#     print(berg, "ist ", höhe, "m (", int(höhe * 3.28), "ft) hoch")  #int weil sonst viele Kommastellen
#     
#  