from random import choice

liste = ("Samiklaus, Christbaum, Weihnachtskugel, Stiefel, Schneeball").split()
eingabe = 0


while eingabe !="x":
    eingabe = int(input("Welche Kalendertür wollen Sie öffnen (oder x für Exit):"))
    #nachfolgende if/else Regel, damit der Nutzer sich auf die Zahlen des Adventkalenders beschränkt
    #sonst würde die Zufallsausgabe bei einer x-beliebigen Zahl weiterhin funktionieren...
    if eingabe <= int(24):
        print(choice(liste))
    else:
        print("Sorry, diese Zahl ist nicht Bestandteil des Aventkalenders. Wähle nochmals")

        