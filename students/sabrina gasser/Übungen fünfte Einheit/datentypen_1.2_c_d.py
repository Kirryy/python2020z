#a - nicht Teil der Hausaufgaben
print("--------")
print("Aufgabe a")
print("--------")
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"] #hier mit normaler Listenlösung
jahreszeiten.sort()
print(jahreszeiten)

#b - nicht Teil der Hausaufgaben
print("--------")
print("Aufgabe b")
print("--------")
jahreszeiten = ("Frühling Sommer Herbst Winter").split( )   #hier Split-Lösung
print(jahreszeiten[:3])    # -> kann so, oder mit 0:3 angegeben werden, aber die Null ist eigtl. nicht nötig, da von Anfang an


#c
#zuerst ohne for-Schleife gelöst...zum ausprobieren
print("--------")
print("Aufgabe c")
print("--------")

jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
print("Frühling", "-", "Frühling"[6:])
print("Sommer", "-", "Sommer"[4:])
print("Herbst", "-", "Herbst"[4:])
print("Winter", "-", "Winter"[4:])

print("--------")

#hier mit Schleife
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
for einzelne_namen in jahreszeiten:
    print(einzelne_namen, "-", einzelne_namen[-2:])


#d
print("--------")
print("Aufgabe d")
print("--------")

jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
for einzelne_namen in jahreszeiten:
    if einzelne_namen[-2:] == "er":
        print(einzelne_namen)
