# ad-hoc übung 3.2 a (s16) - einkaufsliste

#a)
einkaufsliste = ["Äpfel","Birnen", "Butter", "Brot"]
einkaufsliste.sort()
print("Einkaufsliste", einkaufsliste)

# #b)
einkaufsliste2 = []
eingabe = input("Füge Items auf die Einkaufsliste hinzu (gib 'x' ein um die Liste abzuschliessen: ")

while eingabe !="x":
    einkaufsliste2 = einkaufsliste2 + [eingabe]
    eingabe = input("Weiteres Item: ")
else:
    print("Einkaufsliste: ", einkaufsliste2)

#c
einkaufsliste = [
                    ["Äpfel", 1.50],
                    ["Birnen", 2.40],
                    ["Butter", 3.40],
                ]
einkaufsliste.sort()
for items, preis in einkaufsliste:
    print(items, "CHF", preis)
print("zu bezahlender Gesamtpreis:", (einkaufsliste[0][1]+ einkaufsliste[1][1]+ einkaufsliste[2][1]))

 