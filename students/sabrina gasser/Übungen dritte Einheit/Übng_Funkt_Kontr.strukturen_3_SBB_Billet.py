#3. SBB Billet

def get_billet_preis(alter, entf):
    preis = 2 + (entf * 0.25)

    if alter <= 6:
        billet = str(0)
    elif alter <= 16:
        billet = str(int(preis/2))
    else:
        billet = str(int(preis))
    #ich habe bei den zwei Zeilen oben dran "int" hinzugefügt, damit die Zahlen wie auf Aufgabenblatt ganze Zahlen sind.
    #ich erhielt sonst immer Zahlen mit einer Nachkommastelle, z.B. 52.0
    
    return "Dein Billet kostet CHF " + str(billet)

print(get_billet_preis(3, 200))
print(get_billet_preis(8, 200))
print(get_billet_preis(30, 200))
