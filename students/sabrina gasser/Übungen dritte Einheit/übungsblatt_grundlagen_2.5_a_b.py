#sabrina gasser

#2.a

def legal_status():
    age = int(frage)
    if age >= 18:
        status = "volljährig"
    else:
        status = "minderjährig"
    print("Mit " + str(age) + " ist man " + status + ".")
    #str(age) weil sonst kommt Fehlermelung wegen str
    
frage = input("wie alt bist du? ")
legal_status()

#Hinweis: hatte zuerst print(legal_status()) -> dann kam aber am Ende immer ein "none" im Ergebnis. Ohne print(..) -> kein none



#2.b.
def legal_status():
    age = int(frage)
    if age <= 0:
        status = "ungeboren"
    elif age <= 6:
        status = "geschäftsunfähig"    
    elif age <= 14:
        status = "unmündig"
    elif age <= 18:
        status = "mündig minderjährig"
    else:
        status = "volljährig"

    print("Mit " + str(age) + " ist man " + status + ".")
    #str(age) weil sonst kommt Fehlermeldung wegen str
    
frage = input("wie alt bist du? ")
legal_status() 