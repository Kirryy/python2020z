from turtle import *

def bilderrahmen(laenge, breite, stiftfarbe, fuellung):

    pencolor(stiftfarbe)
    fillcolor(fuellung)
    
    begin_fill()

    i=1
    while i <= 2:
        fd(laenge)
        right(90)
        fd(breite)
        right(90)
        i=i+i
    end_fill()
        
    penup()
    fd(laenge/4)
    right(90)
    fd(breite/4)
    left(90)
    pendown()
    
 
def innenflaeche(l, b, stiftfarbe, fuellung):
    pencolor(stiftfarbe)
    fillcolor(fuellung)
    
    begin_fill()
    
    i=1
    while i <= 2:   
        fd(l)
        right(90)
        fd(b)
        right(90)
        i=i+1
    
    end_fill()
        
bilderrahmen(200, 150, "yellow", "black")
innenflaeche(100, 75, "yellow", "white")