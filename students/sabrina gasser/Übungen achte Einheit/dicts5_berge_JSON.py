auflistung = {}

i=0
nr = 1
while i<3:
    berg = input(str(nr)+ " Gib einen Berg ein: ")
    höhe = input(str(nr)+ " Gib die Höhe ein: ")
    höhe = int(höhe)
    auflistung[berg] = höhe
    i=i+1
    nr = nr + 1

from json import loads, dumps

with open ('berge.json', 'w', encoding='utf8') as f:
    json_string = dumps(auflistung)
    f.write(json_string)
    
try:
    with open('berge.json', encoding='utf8') as f:
        json_string = f.read()
        auflistung = loads(json_string)
        
except FileNotFoundError:
    print('berge.json konnte nicht gefunden werden')    
    