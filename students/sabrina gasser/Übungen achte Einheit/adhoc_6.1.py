with open('sicherheitskopie.txt', 'w', encoding='utf8') as text:
    text.write("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod \n")
    text.write("tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.")
#meine Lösung geht nur wenn ich ein \n einfüge... sonst wird Zeile 2 gleich fortlaufend an Zeile 1 angehängt ohne 
       

i = 1        

with open('sicherheitskopie.txt', encoding='utf8') as ausgabe:
    for zeile in ausgabe:
         print(i, ":", zeile)
         i = i + 1
            
try:
    with open('sicherheitskopie.txt', encoding='utf8') as fehler:
        content = fehler.read()
                  
except FileNotFoundError:
    print('sicherheitskopie.txt konnte nicht gefunden werden')   
