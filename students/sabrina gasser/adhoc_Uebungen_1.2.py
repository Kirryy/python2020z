#Autor: Sabrina Gasser
#Datum 14.09.2020

from turtle import *

reset()

#Sprung nach links, damit Aufgabe c & e gleich im Fenster sichtbar sind ohne Fenster zu vergrössern
penup()
back(300)
pendown()

#Aufgabe c
pencolor("blue")
pensize("5")
left(45)
forward(100)

pencolor("red")
right(90)
forward(100)

pencolor("cyan")
left(90)
forward(100)

pencolor("black")
right(90)
forward(100)


#Sprung nach rechts wegen Platz für Aufgabe e
penup()
left(45)
forward(200)
pendown()


#Aufgabe e
pencolor("red")
pensize("5")
forward(100)
left(120)
forward(100)
left(120)
forward(100)

pencolor("blue")
right(60)
forward(100)
right(120)
forward(100)
right(120)
forward(100)

pencolor("lime")
right(60)
forward(100)
left(120)
forward(100)
left(120)
forward(100)
    