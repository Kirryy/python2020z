#2.1
#a)
preisliste = {"Brot":3.20, "Milch": 2.10, "Orangen": 3.75, "Tomaten":2.20}
print(preisliste)

#b)
preisliste["Milch"] = 2.05
print(preisliste)

#c
del preisliste["Brot"]
print(preisliste)

#d
preisliste["Tee"] = 4.20
preisliste["Peanuts"] = 3.90
preisliste["Ketchup"] = 2.10
print(preisliste)


#e
    
lebensmittel_neu = {}

while True:
    lebensmittel = input("Lebensmittel: ")
    if lebensmittel == "x":
        break
    else:
        preis = float(input("Preis: "))
        lebensmittel_neu [lebensmittel] = preis

print("Dictionary:", lebensmittel_neu)