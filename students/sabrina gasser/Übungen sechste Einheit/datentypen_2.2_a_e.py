#a
preisliste = {"Brot":3.20, "Milch": 2.10, "Orangen": 3.75, "Tomaten":2.20, "Tee":4.20, "Ketchup": 2.10}

for lebensmittel, value in preisliste.items():
    print(lebensmittel, "kostet", value, "CHF")

print("-------")

#e

#Preise ein wenig reduziert, weil sont Ergebnis = leer
preisliste = {"Brot":1.20, "Milch":2.10, "Orangen":0.75, "Tomaten":2.20, "Tee":0.20, "Ketchup": 2.10}

for lebensmittel, value in preisliste.items():
    if value < 2.0:
        print(lebensmittel, "kostet", value, "CHF")
