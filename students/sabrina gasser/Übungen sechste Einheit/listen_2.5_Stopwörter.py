l_input = ("Heinz war heute in den Bergen. Es war eine lange Wanderung").split()
#nach langem rumprobieren kam  Split die Lösung - vorher immer Fehler, weil nicht einzelne Wörter ausgelesen

#l_input = l_input.lower()
#irgendwie muss ich die Lösung in Kleinbuchstaben haben weil "Es" wird zB nicht nicht entfernt, aber kriege immer Fehler. Wieso??
stopwords = "der", "die", "das", "in", "auf", "unter", "ein", "eine", "ist", "war", "es"

def stopword_filter(l_input, stopwords):
    l_input_neu = []

    for word in l_input:
        if word not in stopwords:  
            l_input_neu.append(word)
    return l_input_neu

print (stopword_filter(l_input, stopwords))
