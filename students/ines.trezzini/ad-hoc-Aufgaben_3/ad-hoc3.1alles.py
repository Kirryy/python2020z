# ad hoc 3.1 a)
# Variante 1:
text="Die Lage ist hoffnungslos, aber nicht ernst."
print(text.replace(text, len(text)*"*"))

text="Hoihoi!"
print(text.replace(text, len(text)*"*"))

# Variante 2:
text="hallo"
# len(text) nützt nichts, da es nicht gespeichert wird
print("*"*len(text))

# Variante 3 und 4:
zeichenkette=input("Eingeben: ")
print(zeichenkette.replace(zeichenkette, len(zeichenkette)*"*"))

zeichenkette = input("Zeichenkette eingeben: ")
for buchstabe in (zeichenkette):
    buchstabe=buchstabe.replace(buchstabe,"*")
    print(buchstabe, end="")
print(" ")

# ad hoc 3.1b)
eingabetext=input("Eingabe: ")
eingabetext = eingabetext.lower()
eingabezeichen=input("Zeichen: ")
eingabezeichen = eingabezeichen.lower()
print(eingabetext)
for position,element in enumerate(eingabetext):
    print(position,element)

# ????
# Wie kann ich einzelne Zeilen aus dem Ergebnis rauspflücken?

# ad hoc 3.1 c)
ohneleerzeichen = input ("Geben Sie eine zweite Zeichenkette an: ")
print(ohneleerzeichen.replace(" ",""))
# ohne Eingabeaufforderung
print(input().replace(" ",""))

# ad hoc 3.1d)
def replace(text, position, einfügetext): 
    print(text.replace(position,einfügetext))

replace("Dies ist ein Beispiel.","ein Beispiel.","eine Lösung.")
replace("Früher Vogel fängt den Wurm.","Wurm","Käfer")

# Zusatzbeispiel:
a=7
text="Guten Morgen"
a=a+1
# nur a+1 reicht nicht, denn Ergebnis muss abgespeichert werden.
print(a)

text=text.replace("Guten", "Schönen")
print(text)