# Dictionary
# ad hoc 3.3a)

städte={"Zürich":370000,"Genf":190000,"Basel":170000,"Bern":130000}

# Variante 1:
for stadt in städte:
    print(stadt, "hat", städte[stadt], "Einwohner.")

# Variante 2: .items() erzeugt Dictionary
print(städte.items())
for stadt,einwohner in städte.items():
    print(stadt, "hat", einwohner, "Einwohner.")

# Stadt hinzufügen:
städte["Chur"]=30000
städte["Chur"]=20000 #überschreibt ersten Wert städte["Chur"]=30000
print(städte)

# ad hoc 3.3c) Ausgabe aufsteigend sortiert nach Einwohnerzahlen ausgeben
städteliste= {"Luzern":82000, "Genf":190000,"Basel":170000,"Bern":130000,"Zürich":370000,"Zug":30000}

sortierte_städte=[]
for stadt,zahl in städteliste.items():
    sortierte_städte.append([zahl,stadt])
print(sortierte_städte)

for zahl,stadt in sorted(sortierte_städte):
    print(stadt, "hat", zahl, "Einwohner*innen.")

# Alphabetisch sortiert, Variante 1:
def alphabetisch_sortiert(listenname):
    print("Alphabetisch sortiert als Liste: ",sorted(listenname.items()))

alphabetisch_sortiert(städteliste)

# Alphabetisch sortiert, Variante 2:
def alphabetisch_sortiert2(listenname):
    for stadt,einwohner in sorted(listenname.items()):
        print(stadt,einwohner)

alphabetisch_sortiert2(städteliste)

# Alphabetisch sortiert, Variante 3:
städteliste= {"Luzern":82000, "Genf":190000,"Basel":170000,"Bern":130000,"Zürich":370000,"Zug":30000}
for stadt, einwohner in sorted(städteliste.items()):
    print(stadt,"hat",einwohner,"Einwohner*innen.")