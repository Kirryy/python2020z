# ad-hoc 2.6

from turtle import *

# Programm für drei Vierecke
def drei_vierecke():
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    
    penup()
    right(90)
    forward(60)
    pendown()
  
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    
    penup()
    right(90)
    forward(60)
    pendown()
    
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    
    penup()
    right(90)
    forward(60)
    pendown()
    
drei_vierecke()

# fragen, wie viele Vierecke gezeichnet werden sollen
viereckmenge = numinput("Eingabefenster", "Bitte Anzahl Vierecke eingeben:")
zahl=int(viereckmenge)
# man braucht eine Zahl als Input. Mögliche Variante: zahl = int(numinput("Eingabefenster", "Bitte Anzahl Vierecke eingeben:"))
for viereckmenge in range(zahl):
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    right(90)
    forward(50)
    
    penup()
    right(90)
    forward(60)
    pendown()
