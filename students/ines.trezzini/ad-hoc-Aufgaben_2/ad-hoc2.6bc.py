# ad hoc 2.6b)

# Schreiben sie ein Programm, das in Zweierschritten von 100 bis 80 zählt.

counter = 100
zählung=1
while counter >=80:
    print(str(zählung)+". Wert:",counter)
    counter = counter-2
    zählung=zählung+1
    
# ad hoc 2.6c): Schreiben sie ein Programm, das 6 Zufallszahlen zwischen 1 und 42 ausgibt.
# Hinweis: zufallszahl = randint(kleinster_wert, höchster_wert)

from random import randint

for zufallszahl in range (6):
    print(str(zufallszahl+1)+". Zufallszahl:",randint(1,42))
