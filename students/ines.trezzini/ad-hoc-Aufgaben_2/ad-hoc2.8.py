# ad hoc 2.8

# Entwerfen Sie einen Algorithmus, der aus einer Zahlenreihe (z.B. 12, 23, 5,
# 56, 77, 18, 9) das Minimum und das Maximum herausfindet und den Durchschnitt berechnet.

# Python-Funktionen: max(), min(), sum(), len(), eckige Klammern für Liste

def zahlenreihe(zahlen):
    minimum=min(zahlen)
    maximum=max(zahlen)
    durchschnitt=sum(zahlen)/len(zahlen)
    # len() ergibt die Anzahl Elemente in der Liste (zahlen)
    # Durchschnitt = Summe aller Zahlen geteilt durch Anzahl Elemente
    # 200/7 wird viele Nachkommastellen ergeben ...
#     durchschnitt = round(durchschnitt, 2)
    # Ich runde auf zwei Nachkommastellen, damit es hübscher aussieht.
    print("Minimum:",minimum)
    print("Maximum:",maximum)
    print("Durchschnitt:",durchschnitt)
    
zahlenreihe([12,23,5,56,77,18,9])
# Man muss die Zahlen als Liste betrachten, damit es funktioniert, daher müssen die Zahlen in eckige Klammern geschrieben werden.

zahlenreihe([1,1,1])
zahlenreihe([1,10,100,1000])
zahlenreihe([56475,43432,5,6,867867,0,39,13,8])
# Gäbe es auch eine Möglichkeit, die Funktion ohne eckige Klammern zu bewerkstelligen?