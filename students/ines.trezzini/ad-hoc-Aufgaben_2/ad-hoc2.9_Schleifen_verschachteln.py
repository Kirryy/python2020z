# ad hoc 2.9 a)
# Nutzen Sie verschachtelte Schleifen, um vier Dreiecke hintereinander zu zeichnen:
# im Unterricht gelöst

from turtle import *

def zeichnung(füllung):
    fillcolor(füllung)
    begin_fill()
    for x in range(3):
        forward(100)
        left(120)
    end_fill()

def abstand(sl):
    penup()
    forward(120)
    pendown()

for x in range(4):
    zeichnung("blue")
    abstand(100)
        
# ad hoc 2.9b)
# Erweitern Sie Ihr Programm, sodass der Benutzer die Anzahl der gezeichneten Dreiecke frei wählen kann.
# siehe auch ad hoc 2.6

from turtle import*
reset()

def dreieck(sl):
    fillcolor("blue")
    begin_fill()
    fd(sl)
    lt(120)
    fd(sl)
    lt(120)
    fd(sl)
    lt(120)
    end_fill()
    
def abstand(sl):
    penup()
    fd(sl+50)
    pendown()

dreieckmenge = numinput("Eingabefenster", "Bitte Anzahl Vierecke eingeben:")
zahl=int(dreieckmenge)
for dreieckmenge in range(zahl):
    dreieck(100)
    abstand(100)

# ad hoc 2.9c)
from turtle import*
reset()

def dreieck(sl):
    fillcolor("blue")
    begin_fill()
    fd(sl)
    lt(120)
    fd(sl)
    lt(120)
    fd(sl)
    lt(120)
    end_fill()
    
def abstand(sl):
    penup()
    fd(sl+50)
    pendown()
    
def reihenabstand():
    penup()
    right(90)
    forward(100)
    right(90)
# etwas herumzirkeln mit den Winkeln, damit die Dreiecke gleich ausgerichtet sind
    forward(zahl1*150)
# damit die Reihen untereinander stehen: Anzahl Dreiecke * (Dreieckslänge + Abstand) 
    pendown()
    left(180)

dreieckmenge = numinput("Eingabefenster", "Bitte Anzahl Vierecke eingeben: ")
zahl1=int(dreieckmenge)
reihenzahl = numinput("Eingabefenster", "Bitte Anzahl Reihen eingeben: ")
while reihenzahl >0:
    for dreieckmenge in range(zahl1):
        dreieck(100)
        abstand(100)
    reihenabstand()
    reihenzahl=reihenzahl-1
