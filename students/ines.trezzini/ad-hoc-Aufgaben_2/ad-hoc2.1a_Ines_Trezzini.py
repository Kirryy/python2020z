# ad-hoc 2.1 a, basierends auf ad-hoc 1.3 a)

from turtle import *

# Definition des Dreiecks mit vier Parametern
def dreieck(seitenlänge, stiftdicke, zeichenfarbe,füllfarbe):
    print(seitenlänge, stiftdicke, zeichenfarbe, füllfarbe)
#  Bereitet den Stift vor
    pensize(stiftdicke)
    pencolor(zeichenfarbe)
    fillcolor(füllfarbe)
#  Zeichnen des Dreiecks
    begin_fill()
    right(120)
    forward(seitenlänge)
    right(120)
    forward(seitenlänge)
    right(120)
    forward(seitenlänge)
    end_fill()
    
# Aufrufen der Dreiecke bzw. Kreation des Windrads

# Ausrichtung
right(40)

# grünes Dreieck
dreieck(100, 5, "red", "yellow")

# Ausrichtung
right(120)

# blaues Dreieck
dreieck(100, 5, "red", "lime")

# Ausrichtung
right(120)

# gelbes Dreieck
dreieck(100, 5, "red", "cyan")
