# ad-hoc 1.5 a), S. 28

from turtle import *

# grünes Dreieck
# Wenn man Grösse eingeben will: size=numinput und forward(size)
reset()
size = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
left(30)
pencolor("red")
pensize(5)
fillcolor("green")
begin_fill()
forward(size)
left(120)
forward(size)
left(120)
forward(size)
end_fill()

# blaues Dreieck
size = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
fillcolor("cyan")
begin_fill()
forward(size)
left(120)
forward(size)
left(120)
forward(size)
end_fill()

# gelbes Dreieck
size = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
fillcolor("yellow")
begin_fill()
forward(size)
left(120)
forward(size)
left(120)
forward(size)
end_fill()

# Andere Variante möglich: nur einmal Grösse eingeben, nämlich beim "Startquadrat", danach jeweils size*4 etc., damit Dreiecke grösser werden
# numinput nur für Eingabe mit Zahl


