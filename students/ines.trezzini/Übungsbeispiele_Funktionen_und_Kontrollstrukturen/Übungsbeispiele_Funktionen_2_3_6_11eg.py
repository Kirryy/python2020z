# 2) Schreiben Sie eine Funktion, welche das Alter einer Person entgegennimmt und daraus die theoretische maximale Herzfrequenz berechnet.

def berechne_herzfrequenz(alter):
    maximale_herzfrequenz=220-alter
    print(maximale_herzfrequenz)
    
berechne_herzfrequenz(25)

# 3) Schreiben Sie eine Funktion welche basierend auf dem Alter und der Entfernung den Preis für ein SBB Billet berechnet:
def get_billet_preis(alter, kilometer):
    if alter <6:
        print(0)
    elif alter <16:
        distanzpreis=0.25*kilometer
        gesamtpreis=(2+distanzpreis)/2
        gesamtpreis=int(gesamtpreis)
        print(gesamtpreis)
    else:
        distanzpreis=0.25*kilometer
        gesamtpreis=2+distanzpreis
        gesamtpreis=int(gesamtpreis)
        print(gesamtpreis)
        
get_billet_preis(3,200)
get_billet_preis(8,200)
get_billet_preis(30,200)

# 6) Schreiben Sie eine Funktion welche einen Bilderrahmen mit einer bestimmten
# Länge und Breite unter Zuhilfenahme von Schleifen zeichnet. Weiters soll die Funktion noch die Rahmen- und Fülllfarbe entgegennehmen.

# Muss man das also mit Turtle zeichen?

from turtle import*
reset()

# Es soll kein Turtle sichtbar sein, daher muss der Pfeil verborgen werden.
hideturtle()

def zeichne_rahmen(länge,breite,rahmenfarbe,füllfarbe):
    pencolor(rahmenfarbe)
    fillcolor(füllfarbe)
    begin_fill()
    pensize(3)
    seitenlinie=1
    while seitenlinie<5:
        forward(länge)
        left(90)
        seitenlinie=seitenlinie+1
    end_fill()
    penup()
    fd(breite)
    left(90)
    forward(breite)
    right(90)
    pendown()
    fillcolor(füllfarbe)
    begin_fill()
    seitenlinie=1
    while seitenlinie<5:
        forward(länge/2)
        left(90)
        seitenlinie=seitenlinie+1
    end_fill()

zeichne_rahmen(100,25, "black", "white")

# 11e) Spass mit Schleifen
rauten="# "
leerzeichen="  "
leerfläche=6
while leerfläche==6:
    print(rauten*7)
    leerfläche=leerfläche-1
while leerfläche>0:
    print(rauten+(leerzeichen*5)+rauten)
    leerfläche=leerfläche-1
else:
    print(rauten*7)

# 11g) Spass mit Schleifen
rauten="# "
leerzeichen="          " # zehn Leerzeichen
leerfläche=11
while leerfläche==11:
    print(rauten*7)
    leerfläche=leerfläche-1
while leerfläche>1:
    print(leerzeichen[:+leerfläche]+rauten)
    leerfläche=leerfläche-2
else:
    print(rauten*7)