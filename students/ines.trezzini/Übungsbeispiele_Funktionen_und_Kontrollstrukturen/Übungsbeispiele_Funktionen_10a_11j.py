# 10a): Menü für Herzfrequenz und Fahrkarte

eingabe = 1
while eingabe !=0:
    print("="*50)
    print("Programmübersicht:")
    print(" 1 ... Maximale Herzfrequenz berechnen")
    print(" 2 ... Preis für eine Fahrkarte berechnen")
    print(" ")
    print(" 0 ... Programm beenden")
    print("="*50)
    print(" ")
    eingabe=int(input("Gewählte Option: "))
    
    if eingabe ==1:
        print(" ")
        print("Maximale Herzfrequenz berechnen")
        print("-"*31)
        alter=input("Bitte geben Sie Ihr Alter ein: ")
        alter=int(alter)
        maximale_herzfrequenz=220-alter
        print(" ")
        print("Ihre maximale Herzfrequenz ist "+str(maximale_herzfrequenz)+" bpm.")
        print(" ")
    elif eingabe==2:
        print(" ")
        print("Preis für eine Fahrkarte berechnen")
        print("-"*34)
        alter=input("Bitte geben Sie Ihr Alter ein: ")
        alter=int(alter)
        kilometer=input("Wie viele km wollen Sie reisen? ")
        kilometer=int(kilometer)
        if alter <6:
            print(" ")
            print("Die Fahrkarte kostet: 0 CHF.")
            print(" ")
        elif alter <16:
            distanzpreis=0.25*kilometer
            gesamtpreis=(2+distanzpreis)/2
            gesamtpreis=int(gesamtpreis)
            print(" ")
            print("Die Fahrkarte kostet: "+str(gesamtpreis)+" CHF.")
            print(" ")
        else:
            distanzpreis=0.25*kilometer
            gesamtpreis=2+distanzpreis
            gesamtpreis=int(gesamtpreis)
            print(" ")
            print("Die Fahrkarte kostet: "+str(gesamtpreis)+" CHF.")
            print(" ")
    elif eingabe==0:
        print(" ")
        print("Programm beendet")
        print(" ")
        break
    else:
        print(" ")
        print("Ungültige Option!")
        print(" ")
  

# 11j): Spass mit Schleifen
raute="# "
leer=" "
print(raute*11)
i=2
x=9
while i<12:
    print(leer*i+raute*x)
    i=i+2
    x=x-2
