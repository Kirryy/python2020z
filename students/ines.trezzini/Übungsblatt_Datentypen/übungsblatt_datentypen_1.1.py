# Übungsblatt: Datentypen
# 1.1
# a) Erstellen Sie eine Liste jahreszeiten mit den vier Jahreszeiten.

jahreszeiten=["Frühling","Sommer","Herbst","Winter"]
print(jahreszeiten)

# b) Löschen Sie die Jahreszeit "Frühling" aus der Liste.
# Variante 1:
del jahreszeiten[0]
print(jahreszeiten)
# Frühling ist an Stelle 0, Sommer an Stelle 1 etc. Das Element an der genannten Stelle wird gelöscht

# Variante 2:
jahreszeiten=["Frühling","Sommer","Herbst","Winter"]
jahreszeiten.remove("Frühling")
print(jahreszeiten)
# Sobald das genannte Element in der Liste auftaucht, wird es gelöscht
# (aber nur 1x, also das als erstes auftauchende passende Element).


# c. Fügen Sie die Jahreszeit "Langas" der Liste hinzu.
jahreszeiten.append("Langas")
print(jahreszeiten)

# d. Schreiben Sie ein Programm, welches Sie nach Namen fragt und diese so
# lange einer Liste hinzufügt, bis der Benutzer "x" eingibt. Im Anschluss soll die Liste ausgegeben werden.

namensliste=[]
eingabe = 0
while eingabe !="x":
    eingabe=input("Name: ")
    namensliste.append(eingabe)
    if eingabe=="x":
        namensliste.remove("x") # das "x" soll nicht in der Liste auftauchen
        print(namensliste)