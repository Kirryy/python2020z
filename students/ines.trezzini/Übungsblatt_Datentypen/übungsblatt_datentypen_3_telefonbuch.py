# Grundlagen Datentypen: Telefonbuch

# 3.1ab): Dictionary erstellen, Dictionary einer Liste hinzufügen
telefonbuch = {"Vorname":"Ana", "Nachname":"Skupch", "Phone":"123"}
liste_telefonbuch=[]
liste_telefonbuch.append(telefonbuch)
print(liste_telefonbuch)

# Variante:
print("VARIANTE von AUFGABE 3.1ab)")
telefonbuch = {"Vorname":"Ana", "Nachname":"Skupch", "Phone":"123"}
liste_telefonbuch=[]
liste_telefonbuch=liste_telefonbuch+[telefonbuch]
print(liste_telefonbuch)

# 3.1c)
telefonbuch_tim = {"Vorname":"Tim", "Nachname":"Kurz", "Phone":"732"}
telefonbuch_julia={"Vorname":"Julia", "Nachname":"Lang", "Phone":"912"}

liste_telefonbuch=[]
liste_telefonbuch.append(telefonbuch)
liste_telefonbuch.append(telefonbuch_tim)
liste_telefonbuch.append(telefonbuch_julia)
print(liste_telefonbuch)

# Variante 1:
print("VARIANTE von AUFGABE 3.1c)")
liste_telefonbuch= liste_telefonbuch+ [telefonbuch_tim,telefonbuch_julia]
print(liste_telefonbuch)

# Variante 2?:
# telefonbuch = {"eintrag1":{"Vorname":"Ana", "Nachname":"Skupch", "Phone":"123"},
#                "eintrag2":{"Vorname":"Tim", "Nachname":"Kurz", "Phone":"732"},
#                "eintrag3":{"Vorname":"Julia", "Nachname":"Lang", "Phone":"912"}}
# 
# liste_telefonbuch=[]
# liste_telefonbuch.append(telefonbuch)
# print(liste_telefonbuch)

# 3.1d)
liste_telefonbuch2=[]

while True:
    vorname = input("Vorname: ")
    if vorname.lower() == "x":
        break
    personenverzeichnis={}
    personenverzeichnis["Vorname"]=vorname
    nachname = input("Nachname: ")
    personenverzeichnis["Nachname"]=nachname
    phone = input("Phone: ")
    personenverzeichnis["Phone"]=phone
    liste_telefonbuch2.append(personenverzeichnis)
print(liste_telefonbuch2)

# Oder statt while True: vorname={} und dann while vorname!="x":

# 3.2a), bezieht sich auf die Listen von 3.1abc):
def datensatz_ausgeben(telefonbucheinträge):
    for key, value in telefonbucheinträge.items():
        print(key,":",value)
datensatz_ausgeben(telefonbuch)

# für die anderen beiden Einträge:
print("---------------")
datensatz_ausgeben(telefonbuch_tim)
datensatz_ausgeben(telefonbuch_julia)

# 3.3a)
datenstruktur=[{"Vorname": "Ana", "Nachname": "Skupch", "Phone": "123"},
               {"Vorname": "Tim", "Nachname": "Kurz", "Phone": "722"},
               {"Vorname": "Jasna", "Nachname": "Tusek", "Phone": "181"},
               {"Vorname": "Mia", "Nachname": "Meier", "Phone": "999"},
               {"Vorname": "Lisa", "Nachname": "Leiter", "Phone": "345"},
               {"Vorname": "Tom", "Nachname": "Ate", "Phone": "871"}]

ohne_nummer_1 = []
for telefon in datenstruktur:
    if "1" not in telefon["Phone"]:
        ohne_nummer_1.append(telefon)
print(ohne_nummer_1) 
