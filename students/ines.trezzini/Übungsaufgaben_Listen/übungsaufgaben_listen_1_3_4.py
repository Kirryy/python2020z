# Beispiel 1:Liste nach Einträgen filtern (weniger als vier Buchstaben)

l_input = ["der", "junge", "ist", "sehr", "freundlich"]

# Man iteriert über eine Liste rüber und verändert sie dabei (Länge etc.), dadurch können Elemente teils nicht mehr gefiltert werden
# Nur so funktioniert es problemlos: indem man Liste nicht verändert, d.h. eine Kopie der Liste anfertigt mittels unveränderte_liste=list(l-input)
def filter_list(l_input):
    unveränderte_liste=list(l_input)
    for element in unveränderte_liste:
        if len(element)<4: # Variante: if len(element)<=3
            l_input.remove(element)
    print(l_input)
filter_list(l_input)

# Diese Variante funktioniert nur unzuverlässig, z. B. wird die Liste ["der", "typ", "ist", "sehr", "freundlich"] nicht richtig gefiltert
# Erklärung siehe oben.
# def filter_list(l_input):
#     for element in l_input:
#         if len(element)<4: # Variante: if len(element)<=3
#             l_input.remove(element)
#     print(l_input)
# filter_list(l_input)

# Beispiel 3: Adventskalender
from random import choice
auswahlliste = ["Samiklaus","Christbaum","Weihnachtskugel","Stiefel","Schneeball"]
# Wähle ein zufälliges Element aus der Auswahlliste aus. Zufallselement = choice(auswahlliste)

eingabe = 0

adventskalender = []
for tuerchen in range (24):
    zufallselement = choice(auswahlliste)
    adventskalender.append(zufallselement)
# Damit die Elemente bei einer Zahl immer gleich bleiben.

while eingabe !="x":
    eingabe=input("Welche Kalendertür wollen Sie öffnen? (oder x für Exit): ")
    if eingabe=="x":
        break
    eingabe = int(eingabe)
    if eingabe in range(1,25):
#     oder: zufallselement=choice(auswahlliste) und print(zufallselement)
        print(adventskalender[eingabe-1]) # Damit die Elemente bei einer Zahl immer gleich bleiben.
    else:
        print("Ungültige Eingabe, bitte neue Eingabe!")
        
# Beispiel 4: Wörter mit Suffix

l_input2 = ['Gesundheit', 'Wanderung', 'Heiterkeit','Gewandtheit', 'Lustig']

def get_suffix_words(listenname,endung):
    l_input_neu = []
    for element in l_input2:
        if element.endswith(endung):
            l_input_neu.append(element)
    print(l_input_neu)

get_suffix_words(l_input2,"heit")
get_suffix_words(l_input2,"g")
get_suffix_words(l_input2,"G") # kein Ergebnis, da kein Wort auf l endet
get_suffix_words(l_input2,"ng")
get_suffix_words(l_input2,"Lustig")

# Den Einsatz von "endswith" habe ich ergoogelt. Gibt es auch eine andere Möglichkeit?