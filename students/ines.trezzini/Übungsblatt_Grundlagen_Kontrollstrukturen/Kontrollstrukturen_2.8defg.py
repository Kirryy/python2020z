# 2.8d) Schreiben Sie eine Schleife, welches von 10 bis 0 zählt und statt "0" das Wort "Start" ausgibt.

counter = 10
while counter >=0:
    if counter==0:
        print("Start")
    else:
        print(counter)
    counter = counter-1

# 2.8e) Schreiben Sie eine Schleife, welche drei Zahlen entgegennimmt und die Summe aus diesen berechnet.
gesamtsumme = 0
zahlbezeichnung=1
while zahlbezeichnung <4:
    eingabe=int(input("Bitte geben Sie Zahl "+str(zahlbezeichnung)+" ein: "))
    gesamtsumme=gesamtsumme+eingabe
    zahlbezeichnung=zahlbezeichnung+1
    
print("Summe:",gesamtsumme)

# 2.8e) Variante ohne Schleife
zahl1=input("Bitte geben Sie Zahl 1 ein: ")
zahl1=int(zahl1)
zahl2=input("Bitte geben Sie Zahl 2 ein: ")
zahl2=int(zahl2)
zahl3=input("Bitte geben Sie Zahl 3 ein: ")
zahl3=int(zahl3)

print("Summe:",str((zahl1+zahl2+zahl3)))

# 2.8f) Schreiben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis deren Summe grösser als 10 ist.
# Im Anschluss soll die Summe ausgegeben werden.
gesamtsumme = 0
zahlbezeichnung=1
while gesamtsumme <= 10:
    eingabe=int(input("Bitte geben Sie Zahl "+str(zahlbezeichnung)+" ein: "))
    gesamtsumme=gesamtsumme+eingabe
    zahlbezeichnung=zahlbezeichnung+1
    
print("Summe:",gesamtsumme)

# 2.8g) Schreiben Sie eine Schleife, welche so lange Zahlen entgegennimmt, bis der Benutzer -1 eingibt, und anschliessend die Summe dieser Zahlen ausgibt.

gesamtsumme = 0
anzahlbezeichnung=1
stopp_eingabe=int()
while stopp_eingabe !=-1:
    stopp_eingabe=int(input("Bitte geben Sie Zahl "+str(anzahlbezeichnung)+" ein: "))
    gesamtsumme=gesamtsumme+stopp_eingabe
    anzahlbezeichnung=anzahlbezeichnung+1 # man muss +1 angeben, da ansonsten -1 gerechnet wird, was gemäss Beispiel nicht beabsichtigt ist
    
print("Summe:",gesamtsumme+1)