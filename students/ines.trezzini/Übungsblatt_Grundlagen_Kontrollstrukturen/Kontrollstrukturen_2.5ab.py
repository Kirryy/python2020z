#  2.5a) Schreiben Sie eine Funktion, welche ein Alter entgegennimmt
# und je nach Alter den Wert volljährig oder minderjährig zurückgibt.


# Variante 1 (nicht genau nach Vorgabenblatt)
age=input("age = ")
age=int(age)

if age>17:
    print("Mit",age,"ist man volljährig.")
elif age<18:
    print("Mit",age,"ist man minderjährig.")

# Variante 2 (nach Vorgabenblatt)
age=input("age = ")
age=int(age)

def legal_status(age):
    if age>17:
        return "volljährig"
    else:
        return "minderjährig"

print("Mit",age,"ist man",legal_status(age)+".")

# 2.5b) Erweitern Sie die Funktion.
age=input("age = ")
age=int(age)

def legal_status(age):
    if age<=6:
        return "geschäftunfähig"
    elif age<=14:
        return "unmündig"
    elif age<=17:
        return "mündig minderjährig"
    else:
        return "volljährig"

print("Mit",age,"ist man",legal_status(age)+".")