# Übungsbeispiele Dictionaries: Erweiterung zu Aufgabe 5

# Ursprüngliche Fassung der Übung:    
berge={}
zählung=1
while zählung<4:
    name = input(str(zählung)+". Berg: ")
    höhe = input(str(zählung)+". Höhe: ")
    berge[name]=höhe
    zählung=zählung+1
print(berge)

for name, höhe in sorted(berge.items()):
    print(name,"ist",höhe,"m ("+str(float(höhe)*float(3.28))+" ft) hoch.")
    
# Erweiterung: in Datei berge.json speichern
from json import loads, dumps

json_string = dumps(berge)
berge = loads(json_string)

with open("berge.json", "w", encoding="utf8") as f:
    json_string = dumps(berge)
    f.write(json_string)
    
# Erweiterung: Datei berge.json einlesen (mit try,except)
print("---Datei berge.json einlesen---")

# Ohne Fehlermeldung, da die Datei existiert:
try:
    with open("berge.json", "r", encoding="utf8") as f:
        json_string = f.read()
        berge = loads(json_string)
    print(sorted(berge))
except FileNotFoundError:
    print("Diese Datei existiert nicht.")

# Hier mit der Fehlermeldung, da die Datei nicht existiert:
try:
    with open("bergli.json", "r", encoding="utf8") as f:
        json_string = f.read()
        berge = loads(json_string)
    print(sorted(berge))
except FileNotFoundError:
    print("Diese Datei existiert nicht.")