# Leistungsaufgabe 1a): Erstellen Sie die Schweizer Flagge mit der turtle-Grafik und exakt folgendem Aussehen (400x400 Pixel), es darf kein Pfeil/Turtle sichtbar sein!

from turtle import *

# Es soll kein Turtle sichtbar sein, daher muss der Pfeil verborgen werden.
hideturtle()

# Vorbereitung rote Fläche
pencolor("red")
fillcolor("red")

# rotes Quadrat
begin_fill()
forward(400)
right(90)
forward(400)
right(90)
forward(400)
right(90)
forward(400)
end_fill()

# weiter zum Schweizerkreuz
penup()
right(90)
forward(240)
right(90)
forward(80)
pendown()

# Weisses Kreuz: das Schweizerkreuz wird auf die rote Fläche gelegt. Es ist aus fünf Quadraten zusammengesetzt.
# Die Länge von 400 Pixeln für die rote Fläche kann in 5 Teile zu 80 Pixeln unterteilt werden, daher sind die weissen Quadrate 80 Pixel lang.

# Vorbereitung weisse Fläche
pencolor("white")
fillcolor("white")

# der senkrechte Balken des Kreuzes
begin_fill()
forward(240)
right(90)
forward(80)
right(90)
forward(240)
right(90)
forward(80)

# Vorbereitung für den waagrechten Balken des Kreuzes
penup()
right(90)
forward(160)
right(90)
pendown()

# Der waagrechte Balken des Kreuzes. (Der Einfachheit halber wird einfach über den senkrechten Balken gezeichnet.)
forward(160)
right(90)
forward(80)
right(90)
forward(240)
right(90)
forward(80)
right(90)
forward(80)
end_fill()