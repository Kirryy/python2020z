# Leistungsaufgabe 3b)

übungsdictionary={"Haus":"house","Zahnarzt":"dentist","Zahn":"tooth",
                  "Katze":"cat", "Hund":"dog","Insel":"island","Buch":"book",
                  "Fenster":"window", "Tür":"door", "Nippes":"bric-a-brac"}

resultatliste = []

for deutsche_vokabel in übungsdictionary:
    englische_vokabel = input("Was heisst "+deutsche_vokabel+ ": ")
    if englische_vokabel == übungsdictionary[deutsche_vokabel]:
        print("RICHTIG!")  
        resultatliste.append("1_punkt")
    else:
        print("FALSCH!")
#       hier könnte man die Liste mit den Punkte sehen, wenn man print(resultat) einfügt
print("Sie haben", len(resultatliste), "von 10 Vokabeln richtig beantwortet!")
# Die Anzahl Einträge in der Resultatliste wird gezählt.
# Da dort nur bei richtigen Antworten ein Punkt eingetragen wird, ergibt die Länge also die Anzahl Punkte bzw. richtiger Vokabeln.

# Umständlichere Variante:
# i = 0
# x = 0
# 
# while x <10:
#     haus = input("Was heisst Haus: ")
#     if haus.lower() == "house":
#         print("RICHTIG")
#         i = i + 1
#     else:
#         print("FALSCH!")
#     x = x + 1
#     zahnarzt = input("Was heisst Zahnart: ")
#     if zahnarzt.lower() == "dentist":
#         print("RICHTIG")
#         i = i + 1
#       else:
#         print("FALSCH!")
#     x = x + 1
# 
# Und so weiter für alle Vokabeln ...
#     
# print ("Sie haben", i, "von 10 Vokabeln richtig beantwortet!")