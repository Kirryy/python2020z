# Leistungsaufgabe 2

# 2b)
multiplikator=0
for zahlbereich in range (1,11):
    print(str(zahlbereich)+"er Reihe:")
    multiplikator=multiplikator+1
    for zahlbereich in range (1,11):
        print(zahlbereich,"x",multiplikator,"=",zahlbereich*multiplikator)
    print("--------------")

# Variante
multiplikator=1
for zahlbereich in range (1,11):
    print(str(zahlbereich)+"er Reihe:")
    for zahlbereich in range (1,11):
        print(zahlbereich,"x",multiplikator,"=",zahlbereich*multiplikator)
    multiplikator=multiplikator+1
    print("--------------")