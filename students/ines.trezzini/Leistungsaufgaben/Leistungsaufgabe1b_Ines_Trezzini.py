# Leistungsaufgabe 1b): Erweitern Sie das Programm um eine Eingabemöglichkeit (über Eingabefenster) für die Seitenlänge der Schweizer Flagge!

from turtle import *

# Es soll kein Turtle sichtbar sein, daher muss der Pfeil verborgen werden.
hideturtle()

size = numinput("Seitenlänge","Bitte Seitenlänge angeben:")

# Vorbereitung rote Fläche
pencolor("red")
fillcolor("red")

# rotes Quadrat
begin_fill()
forward(size)
right(90)
forward(size)
right(90)
forward(size)
right(90)
forward(size)
end_fill()

# weiter zum Schweizerkreuz
# Die anfängliche Eingabe im Eingabefenster entspricht 100 %. Die Angaben müssen entsprechend der Grössenverhältnisse verkleinert werden, ausgehend von der Eingabe am Anfang.
# Basierend auf 1a) ergeben sich folgende Werte: 400 Pixel=100%; 240 Pixel= 60%; 160 Pixel=40%, 80 Pixel=20%.
penup()
right(90)
forward(size/100*60)
right(90)
forward(size/100*20)
pendown()

# Vorbereitung weisse Fläche
pencolor("white")
fillcolor("white")

# der senkrechte Balken des Kreuzes
begin_fill()
forward(size/100*60)
right(90)
forward(size/100*20)
right(90)
forward(size/100*60)
right(90)
forward(size/100*20)

# Vorbereitung für den waagrechten Balken des Kreuzes
penup()
right(90)
forward(size/100*40)
right(90)
pendown()

# Der waagrechte Balken des Kreuzes. (Der Einfachheit halber wird einfach über den senkrechten Balken gezeichnet.)
forward(size/100*40)
right(90)
forward(size/100*20)
right(90)
forward(size/100*60)
right(90)
forward(size/100*20)
right(90)
forward(size/100*20)
end_fill()