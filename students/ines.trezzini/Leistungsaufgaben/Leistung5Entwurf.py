# Leistungsaufgabe 5

from urllib.request import urlopen
from operator import getitem
from csv import writer

with urlopen("https://www.ietf.org/rfc/rfc2616.txt") as source:
    web_content=source.read().decode("utf8")
    print(web_content)
    
# Buchstabenhäufigkeit berechnen:

def buchstabenstatistik(web_content):
    buchstabenhäufigkeit = {}
    for buchstabe in web_content:
        if buchstabe in buchstabenhäufigkeit:
            buchstabenhäufigkeit[buchstabe] = buchstabenhäufigkeit[buchstabe] + 1
        else:
            buchstabenhäufigkeit[buchstabe] = 1
    liste1=[]
    liste1.append(buchstabenhäufigkeit)
    print("Liste: ", sorted(liste1))

buchstabenstatistik(web_content)

# # Buchstabenhäufigkeit berechnen?:
# 
# def buchstabenstatistik(webseite):
#     buchstabenhäufigkeit = {}
#     for buchstabe in webseite:
#         if buchstabe in buchstabenhäufigkeit:
#             buchstabenhäufigkeit[buchstabe] = buchstabenhäufigkeit[buchstabe] + 1
#         else:
#             buchstabenhäufigkeit[buchstabe] = 1
#     liste1=[]
#     liste1.append(buchstabenhäufigkeit)
#     print("Liste: ", sorted(liste1))
#     print("Dictionary: ",buchstabenhäufigkeit)
# 
# buchstabenstatistik("https://www.ietf.org/rfc/rfc2616.txt")

# Worthäufigkeit berechnen:
def wortstatistik(webseite):
    worthäufigkeit = {}
    wörter = webseite.split()
    # Webseitentext in Wörterliste aufteilen mit .split
    for wort in wörter:
        if wort in worthäufigkeit:
            worthäufigkeit[wort] = worthäufigkeit[wort] + 1
        else:
            worthäufigkeit[wort] = 1
    print(worthäufigkeit)

wortstatistik("https://www.ietf.org/rfc/rfc2616.txt")

# ???????????????????????????