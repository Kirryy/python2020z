# Leistungsaufgabe 2

# 2a)
def rabattschlacht(gesamtsumme):
    if gesamtsumme <100:
        print("Kein Rabatt, Gesamtpreis =",gesamtsumme,"CHF")
    elif gesamtsumme <1000:
        print("5% Rabatt, Gesamtpreis =",(gesamtsumme*0.95),"CHF")
    elif gesamtsumme >=1000:
        print("10% Rabatt, Gesamtpreis =",(gesamtsumme*0.90),"CHF")

gesamtsumme=0
zahlbezeichnung=1
eingabe=1
while eingabe !="0":
    eingabe=input("Geben Sie den "+str(zahlbezeichnung)+". Preis ein: ")
    zahlbezeichnung=zahlbezeichnung+1
    gesamtsumme=float(gesamtsumme)+float(eingabe)
    if eingabe=="0":
        rabattschlacht(gesamtsumme)

# Was muss ich machen, damit es mit x funktioniert?!

# Variante mit Anzeige der Summe vor Abzug des Rabatts:
# ...
# if eingabe=="0":
#     print ("Summe vor Abzug:",gesamtsumme)
#     rabattschlacht(gesamtsumme)
# ...