# Hausaufgabe: Einkaufsliste

einkaufsliste = [{"Artikel": "Butter", "Preis": 3.30, "Menge":"1 Stück"},
                 {"Artikel": "Essig", "Preis": 0.90, "Menge":"1 Liter"},
                 {"Artikel":"Pfeffer", "Preis": 2.00, "Menge":"2 Gläser"},
                 {"Artikel":"Zimt", "Preis": 4.55, "Menge":"5 Packungen"},
                 {"Artikel":"Zucker", "Preis": 2.10, "Menge":"2 Kilo"}]
print("Aktuelle Einkaufsliste: ",einkaufsliste)
print("")

eingabe = 1
while eingabe !=0:
    print("-----------------------------------------------")
    print("Einkaufsliste bearbeiten:")
    print(" 1 ... Artikel hinzufügen")
    print(" 2 ... Artikel löschen")
    print(" 3 ... Artikel suchen")
    print(" 4 ... Einkaufsliste leeren")
    print(" 5 ... Einkaufsliste speichern")
    print(" 6 ... Einkaufsliste laden")
    print(" 7 ... Einkaufsliste im csv-Format exportieren")
    print(" 0 ... Programm beenden")
    print("-----------------------------------------------")
    eingabe=int(input("Option wählen: "))
    
    if eingabe==0:
        print("Programm beendet. Viel Spass beim Einkauf!")
        break
    elif eingabe ==1:
        print("Ihre Wahl: Artikel hinzufügen")
        ergänzter_einkauf={}
        artikel=input("Artikelname eingeben: ")
        ergänzter_einkauf["Artikel:"]=artikel
        preis=input("Artikelpreis eingeben: ")
        ergänzter_einkauf["Preis:"]=preis
        menge=input("Artikelmenge eingeben: ")
        ergänzter_einkauf["Menge:"]=menge
        einkaufsliste.append(ergänzter_einkauf)
        print("Aktuelle Einkaufsliste:", einkaufsliste)
    elif eingabe ==2:
        print("Ihre Wahl: Artikel löschen")
        bereinigte_liste = []
        gelöschter_artikel=input("Zu löschenden Artikel eingeben: ")
        for artikel in einkaufsliste:
            if gelöschter_artikel not in artikel["Artikel"]:
                bereinigte_liste.append(artikel)
        einkaufsliste=bereinigte_liste
        print("Aktuelle Einkaufsliste:", bereinigte_liste)
    elif eingabe ==3:
        print("Ihre Wahl: Artikel suchen")
        suchliste=[]
        suchartikel=input("Suchbegriff eingeben: ")
        originaleinkäufe=list(einkaufsliste)
        for artikel in originaleinkäufe:
            if suchartikel in artikel["Artikel"]:
                suchliste.append(artikel)
        print("Suchergebnis(se):",suchliste)
        print("Aktuelle Einkaufsliste:", einkaufsliste)
#       so werden nur Artikelnamen ausgegeben - if suchartikel in artikel["Artikel"]: print(Suchergebnisse: ",artikel["Artikel"])
    elif eingabe ==4:
        print("Ihre Wahl: Einkaufsliste leeren")
        einkaufsliste.clear()
        print("Ihre Einkaufsliste ist leer!")
        print("Aktuelle Einkaufsliste:", einkaufsliste)
    elif eingabe ==5:
#         Ist hier gemeint, die Liste als txt-Datei zu speichern?
        print("Ihre Wahl: Einkaufsliste als txt-Datei speichern")
        with open ("gespeicherte_einkaufsliste.txt", "w", encoding="utf8") as f:
            einkaufsliste_speichern = f.write(str(einkaufsliste))
        print("Die Liste wurde als 'gespeicherte_einkaufsliste.txt' abgespeichert.")
        print("Inhalt der Datei:", einkaufsliste)
    elif eingabe ==6:
        print("Ihre Wahl: Einkaufsliste laden")
        try:
            with open("gespeicherte_einkaufsliste.txt", "r", encoding="utf8") as f:
                geladene_einkaufsliste=f.read()
            print("Inhalt der Datei:", geladene_einkaufsliste)
        except FileNotFoundError:
            print("Fehler! Die Datei kann nicht gefunden werden. Bitte zunächst die Einkaufsliste mittels (5) abspeichern.")
    elif eingabe ==7:
        from csv import writer
        print("Ihre Wahl: Einkaufsliste im csv-Format exportieren")
        with open("einkaufsliste_excel.csv", "w", encoding="utf8") as x:
            csv_writer = writer(x, delimiter = ";")
            for zeile in einkaufsliste:
                print(zeile)
                csv_writer.writerow(zeile)
        print("Die Liste wurde als 'einkaufsliste_excel.csv' abgespeichert.")
        print("Inhalt der Datei:", einkaufsliste)
# Die Datei enthält zwar die Kategorien "Artikel", "Preis" und "Menge", aber keine Daten. Warum?
    else:
        print("Ungültige Option! Bitte neu wählen.")

