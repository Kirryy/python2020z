from turtle import *

reset()

shape("arrow")
pensize(5)
pencolor("red")
speed(6)

sl=numinput("Eingabefenster", "Bitte Seitenlänge angeben")

fillcolor("yellow")
begin_fill()
lt(150)
fd(sl)

lt(120)
fd(sl)
lt(120)
fd(sl)
end_fill()

sl=numinput("Eingabefenster", "Bitte Seitenlänge angeben")

fillcolor("cyan")
begin_fill()
rt(120)
fd(sl)
lt(120)
fd(sl)
lt(120)
fd(sl)
end_fill()

sl=numinput("Eingabefenster", "Bitte Seitenlänge angeben")

fillcolor("lime")
begin_fill()
rt(120)
fd(sl)
lt(120)
fd(sl)
lt(120)
fd(sl)
end_fill()
