#Übungsblatt basic loops

#1.

def nummer():
    a = int(input("Ihre Zahl: "))
    print(a * 2)
    
#nummer()

#2.

def aufgabe2 ():
    x = float(input("Ihre erste Zahl: "))
    y = float(input("Ihre zweite Zahl: "))
    
    print("Ergebnis: " + str(2.2) + "x" + str(2.0) + " = " + str(x * y))

#aufgabe2()

#2.1
def guten_morgen():
    print("Guten Morgen!")
    
#guten_morgen()

#2.2
def guten_morgen(name):
    print("Guten Morgen " + name + "!")
    
#guten_morgen("Ana")

#2.3a
#name = input("Bitte gib deinen Namen ein: ")

def guten_morgen(name):
    print("Guten Morgen " + name + "!")
#guten_morgen(name)    

#2.3b

#laenge = int(input("Bitte gib die Länge ein: ")) #Eingabe der Länge
#breite = int(input("Bitte gib die Breite ein: ")) #Eingabe der Breite
def flaeche_rechteck (laenge, breite): #Funktion definieren
    flaeche = laenge * breite #Länge und Breite des Rechtecks multiplizieren 
    print("Die Fläche beträgt", flaeche, "m2") #wird ausgegeben
    
#flaeche_rechteck(laenge, breite)
    
#2.3c
    
def inch_to_cm(inch, laenge_in_cm): #Funktion definieren

    print(inch, "inch entspricht", laenge_in_cm, "cm.")
    
#inch_to_cm(1, 2.54)
    
#2.4

def flaeche_rechteck (l, b):
    flaeche = (l * 2.54) * (b * 2.54)
    
    print("Die Fläche beträgt", flaeche, "cm2")

#flaeche_rechteck(1, 2)

#2.5a
    
#age = int(input("Bitte Alter eingeben: ")) # age definieren durch input

def legal_status(age): #Funktion definieren
    if age >= 18:
        return("volljährig")
    else:
        return("minderjährig")
              
#print("Mit", age, "ist man", legal_status(age) + ".")

#2.5b

def legal_status(age): #Funktion definieren
    if age < 0:
        return("ungeboren")
    elif age <= 6:
        return("unmündig")
    elif age <= 14:
        return ("mündig minderjährig")
    elif age >= 18:
        return("volljährig")
            
#print("Mit", age, "ist man", legal_status(age) + ".")
    

#2.6a

counter = 0 #Fängt bei 0 an aufwärts zu zählen
while counter <= 10: # Bis 10 zählen
    print(counter)
    counter = counter + 1 # In einer Schritten aufwärts zählen
    
#2.6b

counter = 1
while counter <= 10:
    print(counter)
    counter = counter + 1

#2.6c
    
counter = 1
while counter <= 10:
        print(counter)
        counter = counter + 3

#2.6d

counter = 10 #fängt bei 10 an 
while counter >= 0:
    print(counter)
    counter = counter - 1

#2.6e
    
counter = 10 #Fängt bei 10 an 
while counter >= 0:
    if counter == 0: # Anstatt 0 wird Start ausgegeben
        print("Start")
    else:
        print(counter) #Ausgabe des counters 
    counter = counter - 1 #zählt in einer schritten abwärts 

