# ad hoc Übungen 1.2

from turtle import *
reset()

# Übung C)

shape("arrow")
pensize(5)
speed(6)

pencolor("blue")
lt(45)
fd(110)
pencolor("red")
rt(90)
fd(110)
pencolor("cyan")
lt(90)
fd(110)
pencolor("black")
rt(90)
fd(110)

exitonclick()


