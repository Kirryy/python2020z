#Übungsblatt Grundlagen Kontrollstrukturen

#Aufgabe 2.1  Funktionen ohne Rückgabewert und Parameter

def guten_morgen():
    print("Guten Morgen!")

guten_morgen()

#Aufgabe 2.2 Funktionen ohne Rückgabewert mit Parameter

def guten_morgen():
    name = input ("Wie heisst du?")
    print ("Guten Morgen" + name + "!")

guten_morgen()
    
#Aufgabe 2.3 a) Funktionen mit Rückgabewert und Parameter

def guten_morgen(name):
    name = input ("Wie heisst du?")
    return ("Guten Morgen" + name + "!")
    

#Aufgabe 2.3 b)

def berechne_rechteck():
    x = int(input ("Bitte die Länge des Rechtecks eingeben"))
    y = int(input ("Bitte die Breite des Rechtecks eingeben"))
    flaeche = x * y
    
    print("Die Fläche beträgt", flaeche, "m2.")
    
berechne_rechteck()