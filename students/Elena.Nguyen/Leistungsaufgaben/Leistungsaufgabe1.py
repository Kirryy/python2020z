#Erstellen Sie die Schweizer Flagge mit der turtle-Grafik und exakt
#folgendem Aussehen (400x400 Pixel), es darf kein Pfeil/Turtle sichtbar sein!
# Beim weissen Kreuz gibt es keine Vorgaben bzgl. Grösse, es muss aber symmetrisch sein und den gleichen Abstand zu allen Seiten der Flagge aufweisen
# Dokumentieren Sie Ihr Programm mit Kommentarzeilen.

from turtle import *

speed(10)
    
#rotes quadrat erstellen  
begin_fill()
fillcolor("red")
pencolor("red") 
fd(400);lt(90);fd(400);lt(90);fd(400);lt(90);fd(400)
end_fill()

#stift bewegen
fillcolor("white")
pu()
lt(180)
fd(50)
rt(90)
fd(50)
pd()

begin_fill()
fd(300);lt(90);fd(300);lt(90);fd(300);lt(90);fd(300)
end_fill()

#kreuz erstellen
fillcolor("red")
begin_fill()
lt(90);fd(100);lt(90);fd(100);lt(90);fd(100);lt(90);fd(100);lt(90);fd(100)
end_fill()

begin_fill()
fd(100);lt(90);fd(100);rt(90);fd(100);rt(90);fd(100);rt(90);fd(100);rt(90)
end_fill()

#stift bewegen
pu()
fd(200)
pd()

begin_fill()
fd(100);rt(90);fd(100);rt(90);fd(100);rt(90);fd(100);rt(90)
end_fill()

#stift bewegen
lt(90)
pu()
fd(100)
pd()

begin_fill()
fd(100);rt(90);fd(100);rt(90);fd(100);rt(90);fd(100)
end_fill()

#turtle verstecken
hideturtle()


    


    