#Übungsaufgabe Listen
#Beispiel 3
from random import choice
auswahlliste = ["Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball"]
zufallselement = choice(auswahlliste)

eingabe=0

while eingabe != "x":
    eingabe= input("Welche Kalendertür wollen Sie öffnen (oder x für Exit): ")
    if eingabe == "x":
        break
    eingabe=int(eingabe)
    if eingabe in range (1,25):
        print(zufallselement)
    else:
        print("Bitte geben Sie eine Zahl zwischen 1 und 24 an! ")
    

#Gibt immer nur ein Name zurück