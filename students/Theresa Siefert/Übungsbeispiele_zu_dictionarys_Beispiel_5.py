# Übungsbeispiele zu dictionarys
#Beispiel 5

berge={}

for x in range (1,4):
    name=input("Berg: ")
    hoehe=int(input("Höhe: "))
    berge[name]=hoehe

liste=[]

for berg, höhe in berge.items():
    liste.append([berg, höhe])

print(sorted(berg, " ist ", höhe, " m " ,  "(",round(höhe*3.28,0),"ft.)", " hoch."))