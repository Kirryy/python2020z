#Ad hoc 2.9 b und c
from turtle import *

def dreieck(farbe):
  fillcolor(farbe)
  begin_fill()
  forward(90)
  left(120)
  forward(90)
  left(120)
  forward(90)
  left(120)
  end_fill()

def abstand():
  penup()
  forward(120)
  pendown()

def reihe():
  penup()
  left(180)
  forward(anzahl_der_dreiecke*120)
  left(90)
  forward(120)
  left(90)
  pendown()


anzahl_der_dreiecke=int(input("Wie viele Dreiecke sollen gezeichnet werden? "))
anzahl_der_dreiecke_reihen=int(input("Wie viele Reihen sollen gezeichnet werden? "))

for y in range(anzahl_der_dreiecke_reihen):
  for x in range(anzahl_der_dreiecke):
    dreieck("blue")
    abstand()
  reihe()
    