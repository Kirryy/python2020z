#2.5 b)
# erweitern auf geschäftsunfähig, unmündig, mündig, minterjährig und volljährig. + Negatives alter (ungeboren)
def legal_status():
    alter=int(eingabe)
    
    if alter <= 0:
        status = "ungeboren"
    elif alter <= 6:
        status = "geschäftsunfähig"
    elif alter <= 14:
        status = "unmündig"
    elif alter <= 17:
        status = "mündig minderjährig"
    elif alter >= 18:
        status = "volljährig"
    return print("Mit", alter, "ist man", status + ".")

eingabe=input("Wie alt ist die Person?")
    
print(legal_status())