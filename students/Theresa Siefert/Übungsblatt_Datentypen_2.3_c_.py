# Übungsblatt Datentypen
#2.3
#c)
preisliste={"Brot": 3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2,"Gurken":1.5}
preisliste_neu = {}

for lebensmittel, preis in preisliste.items():
    if not "en" in lebensmittel:
        preisliste_neu[lebensmittel] = preis

print("Lebensmittel", preisliste_neu)
