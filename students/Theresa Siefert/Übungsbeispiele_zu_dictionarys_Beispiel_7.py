#Übungsbeispiele zu dictionarys
#Beispiel 7
eingabetext=("David Mayer von der HTW hat um 9:00 seine Wohnung in Chur verlassen")
eingabeliste = eingabetext.lower()
eingabeliste = eingabeliste.split()

blacklist=["david", "mayer", "htw", "chur"]

ausgabe= []

for wort in eingabeliste:
    if wort in blacklist:
        ausgabe.append(len(wort)*"*")
    if wort not in blacklist:
        ausgabe.append(wort)

print(ausgabe)


