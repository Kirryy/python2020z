def legal_status(age):
    if age < 0:
        print("Mit", age, "ist man ungeboren")
    elif age <= 6:
        print("Mit", age, "ist man geschäftsunfähig")
    elif age <= 14:
        print("Mit", age, "ist man unmündig")
    elif age < 18:
        print("Mit", age, "ist man mündig & minderjährig")
    else:
        print("Mit", age, "ist man volljährig")

legal_status(14)
legal_status(22)
legal_status(-1)