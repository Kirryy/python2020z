def legal_status(age):
    if age < 18:
        print("Mit", age, "ist man minderjährig")
    else:
        print("Mit", age, "ist man volljährig")

legal_status(14)
legal_status(22)