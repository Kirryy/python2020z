
#2.1 a
preisliste = {"Brot":3.2,"Milch":2.1,"Orangen":3.75,"Tomaten":2.2}

#2.1 b
preisliste["Milch"]=2.05
print(preisliste)

#2.1 c
del preisliste["Brot"]
print(preisliste)

#2.1 d
preisliste["Tee"]=4.2
preisliste["Peanuts"]=3.9
preisliste["Ketchup"]=2.1
print(preisliste)

#2.1 e
ergebnis = {}
eingabe=""
while eingabe != "x":
    eingabe=input ("Name Lebensmittel:")
    if eingabe == "x":
        break
    preis= input ("Preis:")
    ergebnis [eingabe] = preis
print (ergebnis)





