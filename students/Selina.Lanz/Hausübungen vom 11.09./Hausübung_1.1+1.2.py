
# 1.1 a)

print ("Lanz","Selina", "Heiselstrasse", "45", "8155", "Niederhasli")

# 1.1 b) falsch nochmals prüfen

print (1+2+3+4+5)
print (1*2*3*4*5)
print (1/2/3/4/5)
print (5-4-3-2-1)


# 1.2 c)

from turtle import *
reset ()
pencolor ("blue")
pensize (5)
left (45)
forward (120)

pencolor ("red")
pensize (5)
right (90)
forward (120)

pencolor ("cyan")
pensize (5)
left (90)
forward (120)

pencolor ("black")
pensize (5)
right (90)
forward (120)

# 1.2 e)
from turtle import *
reset ()

pencolor ("red")
pensize (5)
forward (100)
right (240)
forward (100)
right (240)
forward (100)

pencolor("green")
forward (100)
right (240)
forward (100)
right (240)
forward (100)

pencolor ("blue")
forward (100)
right (240)
forward (100)
right (240)
forward (100)
