# 2.2 a)

preisliste = { "Brot":3.2, "Milch":2.1, "Orangen": 3.75, "Tomaten": 2.2, "Kiwi": 1.5}
for lebensmittel, preis in preisliste.items():
    print (lebensmittel,"kostet", preis, "CHF.")

# 2.2 e) nur Lebensmittel unter 2CHF, geht leider nicht!!

preisliste = { "Brot":3.2, "Milch":2.1, "Orangen": 3.75, "Tomaten": 2.2, "Kiwi": 1.5}
for lebensmittel, preis in preisliste.items():
    if preis <2:
        print (lebensmittel, preisliste)
        
        
# 2.3 c)   funktioniert nicht...

preisliste = { "Brot":3.2, "Milch":2.1, "Orangen": 3.75, "Tomaten": 2.2, "Kiwi": 1.5}
for lebensmittel, preis in preisliste:
    if "en" in lebensmittel:
        preisliste.pop(lebensmittel)
        
print (preisliste)