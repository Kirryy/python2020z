# Übung 2.1 a)
preisliste ={ "Brot":3.2, "Milch":2.1, "Orangen": 3.75, "Tomaten": 2.2}

# b) ändern des Milchpreises
preisliste ["Milch"] = 2.05

print (preisliste)

# c) enfernen des Eintrags Brot
del preisliste ["Brot"]
print (preisliste)

# d) neue Einträge hinzufügen
preisliste ["Tee"] = 4.2
preisliste ["Peanuts"] = 3.9
preisliste ["Ketchup"] = 2.1
print (preisliste)

# e) Programm hinzufügen

ergebnis = {}
eingabe=""
while eingabe != "x":
    eingabe=input ("Welches Lebensmittel soll hinzugefügt werden?")
    if eingabe == "x":
        break
    preis= input ("Wie teuer ist es?")
    ergebnis [eingabe] = preis
print (ergebnis)




