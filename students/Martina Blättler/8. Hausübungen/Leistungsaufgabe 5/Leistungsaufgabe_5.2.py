alphabet=["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]

from urllib.request import urlopen    
with urlopen("https://www.ietf.org/rfc/rfc2616.txt") as source:
    text_statistik = source.read().decode('utf8')
    from csv import writer
    with open('buchstabe.csv', 'w', encoding='utf8') as f:
        csv_writer = writer(f, delimiter=';')
        for buchstabe in alphabet:
            buchstaben_zeile = buchstabe,text_statistik.count(buchstabe)
            csv_writer.writerow(buchstaben_zeile)
    with open('wort.csv', 'w', encoding='utf8') as f:
        csv_writer = writer(f, delimiter=';')
        text_statistik =text_statistik.lower()
        for wort in text_statistik.split():
            wort_zeile =wort,text_statistik.count(wort)
            csv_writer.writerow(wort_zeile)