alphabet=["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
ressourcen_bezeichnung = input("von wo möchten Sie die Daten holen: web / datei")
if ressourcen_bezeichnung == "web":
    from urllib.request import urlopen
    with urlopen("https://www.ietf.org/rfc/rfc2616.txt") as source:
        text_statistik = source.read().decode('utf8')
        for buchstabe in alphabet:
            print(buchstabe+",",text_statistik.count(buchstabe))
#         text_statistik =text_statistik.lower()
#         for wort in text_statistik.split():
#             print(wort+",",text_statistik.count(wort))
elif ressourcen_bezeichnung == "datei":
    with open("gutenberg.txt", "r", encoding = "utf8") as source:
        text_statistik = source.read()
        for buchstabe in alphabet:
            print(buchstabe+",",text_statistik.count(buchstabe))
#         text_statistik =text_statistik.lower()
#         for wort in text_statistik.split():
#             print(wort+",",text_statistik.count(wort))