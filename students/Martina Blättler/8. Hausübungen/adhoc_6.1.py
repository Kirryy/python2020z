#wie kann ich mehrere Zeilen erstellen, resp. bei der Ausgabe wird egal wie ich es mache immer alles angezeigt. Nicht auf Zeilen aufgeteilt.
zeile_1 = ("Datensicherung (englisch backup [ˈbækʌp]) bezeichnet das Kopieren von Daten in der Absicht,diese im Fall eines Datenverlustes zurückkopieren zu können. Somit ist Datensicherung eine elementare Maßnahme zur Datensicherheit.")
zeile_2 =("Die auf einem Speichermedium redundant gesicherten Daten werden als Sicherungskopie, engl. Backup, bezeichnet. Die Wiederherstellung der Originaldaten aus einer Sicherungskopie bezeichnet man als Datenwiederherstellung, Datenrücksicherung oder (englisch) Restore.") 
sicherungskopie = open("kopie.txt", "w", encoding="utf8")
sicherungskopie.write(zeile_1)
sicherungskopie.write(zeile_2)
sicherungskopie.close()

with open("kopie.txt", encoding='utf8') as ausgabe: 
    for line in enumerate(ausgabe):
        print(line)
        
try:
    with open("kopie.txt", encoding='utf8') as fehlermeldung:
        content = fehlermeldung.read()
except FileNotFoundError:
    print("kopie.txt file is missing")