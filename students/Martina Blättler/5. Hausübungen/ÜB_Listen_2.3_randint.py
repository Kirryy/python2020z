from random import randint
adventskalender = ["Samiklaus", "Weihnachtskugel", "Stiefel", "Schneeball","Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball","Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball","Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball","Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball"]
tag = 1
#wenn es bis mehr Tage gehen könnte, könnte bei der while Schlaufe ein != "x" stehen und der Tag zuerst auf 0 gesetzt werden
while tag != 0:    
    tag = int(input("Welche Kalendertür wollen Sie öffnen (oder 0 für Exit):"))
    element = randint(0,23)
    print(adventskalender[element])
    #diese if Bedingung könnte man entfernen wenn die Anzahl an Tagen nicht begrenzt wäre
    if tag > 24:
        print ("Der Advent hat nur 24 Tage")
        print ("-------------------------------------")