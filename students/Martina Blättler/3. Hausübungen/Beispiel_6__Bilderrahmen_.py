from turtle import *
reset()
penup()
left (90)
forward(200)
left (90)
forward(200)
right(180)
pendown() #Ausrichtung damit alle grössen Platz haben
def zeichne_rahmen(laenge, breite, rahmenfarbe, fuellfarbe):
    pencolor(rahmenfarbe)
    fillcolor(fuellfarbe)
    begin_fill()
    i=0
    while i < 4: #damit die Schleife vier mal ausgeführt wird
        forward(laenge)
        right(90)
        forward(breite)
        i = i+1 #damit die Schleife vier mal ausgeführt wird (immer eine höher)
    end_fill()
    #Stift an einen neuen Standort bringen für den inneren Rahmen
    penup()
    right(90)
    forward(laenge/2.5)
    left(90)
    forward(breite/5)
    pendown()
    #innerer Rahmen zeichenen (gleich wie äusserer einfach kleiner)
    begin_fill()
    i=0
    while i < 4: #Schleife einfach damit man die länge und breite nicht viermal einfügen muss
        forward(laenge/2)
        right(90)
        forward(breite/2)
        i = i+1
    end_fill()
 #mit der Schleife beginnt das zeichnen an einer anderen ausgangslage, als normal? kann das sein, wenn ja wieso?   
    
zeichne_rahmen(100,50,"red","blue")
zeichne_rahmen(200,100,"blue","yellow")