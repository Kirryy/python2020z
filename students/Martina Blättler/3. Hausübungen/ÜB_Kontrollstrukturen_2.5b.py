def legal_status(age):
    if age <=6:
        print ("Mit", age, "ist man geschäftsunmündig.")
    elif age <=14:
        print ("Mit", age, "ist man unmündig.")
    elif age <18:
        print ("Mit", age, "ist man mündig, minderjährig.")
    else:
        print ("Mit", age, "ist man volljährig")
    
legal_status(4)
legal_status(7)
legal_status(14)
legal_status(16)
legal_status(18)
legal_status(30)