preisliste = {"Brot":3.2,"Milch":2.1,"Orangen":3.75,"Tomaten":2.2, "Senf":1.50, "Zwiebel": 0.50, "Banane": 0.90 , "Ketchup":2.1}
#2.2 a
for lebensmittel, preis in preisliste.items():
    print(lebensmittel, "kostet", preis, "CHF")
print("------")

#2.2 f
print ("Diese Lebensmittel sind günstiger als 2.0 CHF:")
for lebensmittel, preis in preisliste.items():
    if preis < 2.0:
        print (lebensmittel, "kostet", preis, "CHF")