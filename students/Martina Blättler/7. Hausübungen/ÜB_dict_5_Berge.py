berge_dict={}
for x in range (1,4):
    berg=input("Welchen Berg möchten Sie erfassen:?")
    höhe= input("Wie hoch ist dieser in Meter?")
    höhe = float(höhe)
    berge_dict[berg]=höhe
    
sortierliste = []
for mountain, height in berge_dict.items():
    sortierliste.append([mountain,height])
for mountain, height in sorted(sortierliste):
    print (mountain, "ist", height, "m.", "(",round(height*3.28,0),"ft.)")