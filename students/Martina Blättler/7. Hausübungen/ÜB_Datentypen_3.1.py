#3.1
#a
ana ={"Vorname":"Ana","Nachname":"Skupch","Phone":"123"}
#b
telefonbuch=[ana]

#c
tim={"Vorname":"Tim","Nachname":"Kurz","Phone":"732"}
julia={"Vorname":"Julia","Nachname":"Lang","Phone":"912"}
telefonbuch.append(tim)
telefonbuch.append(julia)
# weitere möglichkeit: telefonbuch = telefonbuch + [tim,julia]

#d
eingabe = ""
while eingabe != "x":
    eingabe = input("Möchten Sie noch eine Person hinzufügen? Ja/x")
    if eingabe =="x":
        break
    else:
        person={}
        vorname=input("Vorname?")
        person["Vorname"]=vorname
        nachname = input("Nachname?")
        person["Nachname"]=nachname
        phone = input("Telefonnummer?")
        person["Phone"]=phone
        telefonbuch.append(person)
print("Datenstruktur:",telefonbuch)