from turtle import *
reset()
pencolor("blue")
fillcolor("blue")

begin_fill()
anzahl = 1
dreieck = numinput("Dreiecke","Wie viele Dreiecke sollen gezeichnet werden?")
dreieckzahl = int(dreieck)
for anzahl in range(dreieckzahl):
    for i in range(3):
        forward (60)
        left (120)
    penup()
    forward(100)
    pendown()
end_fill()
hideturtle()