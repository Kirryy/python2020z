from turtle import *
reset()
pencolor("blue")
fillcolor("blue")
penup()
left(90)
forward(150)
left(90)
forward(200)
right(180)

def reihen(menge):
    for reihe in range(menge):
        pendown()
        begin_fill()
        dreieck = numinput("Dreiecke","Wie viele Dreiecke sollen gezeichnet werden?")
        dreieckzahl = int(dreieck)
        for anzahl in range(dreieckzahl):
            for i in range(3):
                forward (60)
                left (120)
            penup()
            forward(100)
            pendown()
        end_fill()
        penup()
        right(90)
        forward(200)
        left(90)
        hideturtle()

reihen(3)    