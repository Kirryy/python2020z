from turtle import *
reset()
pencolor ("red")
pensize (3)

def viereck():
    right(45)
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)

counter = 0
anzahl = numinput("Anzahl", "Geben Sie die Anzahl Dreiecke ein")
while counter < anzahl:
    viereck()
    counter = counter + 1
