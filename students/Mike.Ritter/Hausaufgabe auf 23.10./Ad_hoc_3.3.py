# ad hoc Übung 3.3

# Teilaufgabe a)

st={"zürich":370000, "Luzern":10000, "genf":190000, "basel":170000, "bern":130000}
for stadt in st:
    print (stadt, "hat", st[stadt], "Einwohner")

print (st.items())
for stadt, einwohner in st.items():
    print(stadt, "hat", einwohner, "Einwohner")

# Teilaufgabe c)

liste = list(st.items())

def listen_wert(x):
    return x[1]

sorted_st = sorted(st.items(), key=listen_wert)
print(sorted_st)
