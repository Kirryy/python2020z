# ad hoc Übung 1.2

# Teilaufgabe c)

from turtle import *

reset()

pensize(5)

pencolor("blue")
lt(45)
fd(100)

pencolor("red")
rt(90)
fd(100)

pencolor("cyan")
lt(90)
fd(100)

pencolor("black")
rt(90)
fd(100)

# Zurück zum Startpunkt und Abstand für nächste Teilaufgabe schaffen.

penup()
rt(135)
fd(280)
lt(90)
fd(200)
lt(90)
pendown()

# Teilaufgabe e)

pencolor("red")
fd(100)
lt(120)
fd(100)
lt(120)
fd(100)

pencolor("blue")
rt(60)
fd(100)
rt(120)
fd(100)
rt(120)
fd(100)

pencolor("lime")
fd(100)
rt(120)
fd(100)
rt(120)
fd(100)

