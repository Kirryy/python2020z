# ad hoc Übung 1.3

# Teilaufgabe b)

from turtle import *

reset()

pensize(5)

pencolor("red")
fillcolor("cyan")
begin_fill()
lt(35)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
end_fill()
bk(100)

fillcolor("yellow")
begin_fill()
lt(15)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
end_fill()

fillcolor("magenta")
begin_fill()

rt(155)

fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
end_fill()

fillcolor("blue")
begin_fill()

lt(200)

fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
end_fill()

fillcolor("lime")
begin_fill()

lt(195)

fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
end_fill()

