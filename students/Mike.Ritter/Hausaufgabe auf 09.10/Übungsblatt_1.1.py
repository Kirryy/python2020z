# Übungsblatt 1.1

# Teilaufgabe a)

jahreszeiten =["Winter", "Frühling", "Sommer", "Herbst"]
print(jahreszeiten)

# Teilaufgabe b)

jahreszeiten.remove("Frühling")
print(jahreszeiten)

# Teilaufgabe c)

jahreszeiten.append("Langas")
print(jahreszeiten)

# Teilaufgabe d)

name_hinzufügen=input("Name: ")
namensliste=[]


while name_hinzufügen != "x":
    namensliste= namensliste + [name_hinzufügen]
    name_hinzufügen = input ("Name: ")
else:
    print ("Liste: ", namensliste)

