# Übungsblatt 2.8

# Teilaufgabe d)

i=10
while i>0:
    print (i)
    i=i-1
print ("Start")

# Teilaufgabe f)

summe = 0
i=1
while summe <= 10:
    eingabe=int(input("Bitte geben Sie Zahl "+str(i)+" ein"))
    summe=summe+eingabe
    i=i+1
    
print("Summe: ",summe)

# Teilaufgabe g)

summe = 0
i=1
eingabe=int()

while eingabe != -1:
    eingabe=int(input("Bitte geben Sie Zahl "+str(i)+" ein"))
    i=i+1
    summe=summe+eingabe
    
print("Summe: ",summe+1)

    