# Übungsbeispiel 6

from turtle import *



def rahmen_innen(breite):
    penup()
    fd(breite)
    lt(90)
    fd(breite)
    rt(90)
    pendown()
   

def zeichne_rahmen(länge, breite, farbe, füllfarbe):
    pencolor(farbe)
    fillcolor(füllfarbe)
    begin_fill()
    i=1
    while i<=4:
        fd(länge)
        lt(90)
        i=i+1
    end_fill()

    rahmen_innen(breite)
    
    fillcolor(füllfarbe)
    begin_fill()
    i=1
    while i<=4:
        fd(breite*2)
        lt(90)
        i=i+1
    end_fill()

print(zeichne_rahmen(100,25,"black","white"))
 
