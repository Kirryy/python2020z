# Leistungsaufgabe 1

# Teilaufgabe b)

from turtle import *

reset()

# Funktion für Quadrat
def quadrat(sl):
    fd(sl); lt(90); fd(sl); lt(90); fd(sl); lt(90); fd(sl)

# Funktion für Kreuz
def kreuz(size):
    fd(size); lt(90); fd(size); rt(90); fd(size); lt(90); fd(size); lt(90); fd(size); rt(90)
    fd(size); lt(90); fd(size); lt(90); fd(size); rt(90); fd(size); lt(90); fd(size); lt(90)
    fd(size); rt(90); fd(size)

#Quadrat zeichnen
sl = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
pencolor("red")
fillcolor("red")
begin_fill()
quadrat(sl)
end_fill()

#Stift zum Startpunkt
home()
penup()
fd(int(sl/8*3))
lt(90)
fd(int(sl/8))
rt(90)
pendown()

# Kreuz zeichnen
fillcolor("white")
begin_fill()
kreuz(sl/4)
end_fill()

# Turtle verbergen
hideturtle()


