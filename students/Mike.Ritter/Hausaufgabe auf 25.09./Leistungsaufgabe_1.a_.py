# Leistungsaufgabe 1

# Teilaufgabe a)

from turtle import *

reset()

# Funktion für Quadrat
def quadrat(sl):
    fd(sl); lt(90); fd(sl); lt(90); fd(sl); lt(90); fd(sl)

# Funktion für Kreuz
def kreuz(size):
    fd(size); lt(90); fd(size); rt(90); fd(size); lt(90); fd(size); lt(90); fd(size); rt(90)
    fd(size); lt(90); fd(size); lt(90); fd(size); rt(90); fd(size); lt(90); fd(size); lt(90)
    fd(size); rt(90); fd(size)

#Quadrat zeichnen
pencolor("red")
fillcolor("red")
begin_fill()
quadrat(400)
end_fill()

#Stift zum Startpunkt
home()
penup()
fd(150)
lt(90)
fd(50)
rt(90)
pendown()

# Kreuz zeichnen
fillcolor("white")
begin_fill()
kreuz(100)
end_fill()

# Turtle verbergen
hideturtle()


