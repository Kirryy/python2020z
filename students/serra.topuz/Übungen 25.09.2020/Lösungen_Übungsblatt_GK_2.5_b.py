age = input("Wie alt bist du? ")
age = int(age)

def legal_status(age):
    if age <= 0:
        return "ungeboren"
    elif age <= 6:
        return "geschäftsunfähig"
    elif age <= 14:
        return "unmündig"
    elif age <= 17:
        return "mündig volljährig"
    else:
        return "volljährig"
    
legal_status(age)
    
print("Mit", age, "ist man", legal_status(age) + ".")

