# Grundlagen Datentypen: 2.1 Dictionaries erstellen/manipulieren

# a) Preisliste: Produkt + Preis

preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}

print(preisliste)

# b) Preis Milch ändern 2.05

preisliste["Milch"] = 2.05

print(preisliste)

# c) Brot-Eintrag entfernen
del preisliste["Brot"]

print(preisliste)

# d) Neue Einträge: Tee -> 4.2, Peanuts -> 3.9 und Ketchup -> 2.1
preisliste["Tee"] = 4.2
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1

print(preisliste)

# e) Programm: Abfrage Lebensmittel inkl. Preis
# kam nicht weiter

dic = {}

produkt = ""
while True: 
    produkt = input("Bitte geben Produkt und Preis eingeben: ")
    if produkt.lower() != "x":
        break
    preisliste.values(produkt)
    preisliste.keys()

print(dic)