# Übungsbeispiele Dictionaries: 2. Wortstatistik

l_input = "Der Tag begann sehr gut! Der Morgen war schön."

l_input = l_input.lower()

wortstatistik = {}

def word_stat(l_input):
    worte = l_input.split()
    
    for wort in worte:
        if wort in wortstatistik:
            wortstatistik[wort] +=1
        else:
            wortstatistik[wort] = 1
    
    return wortstatistik

print(word_stat(l_input))
    



