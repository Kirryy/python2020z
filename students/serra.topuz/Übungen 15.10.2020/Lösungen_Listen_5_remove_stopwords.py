# Listen: Übungsbeispiele 5

# Remove Stopwords: (Schleifen-Variante)

l_input = input(" ")
# Text einfügen: Heinz war heute in den Bergen. Es war eine lange Wanderung.

split_liste = l_input.split()

stopwords = ["der", "die", "das", "den", "in", "auf", "unter", "ein", "eine", "ist", "war", "es"]

l_input_neu = []

for l_input in split_liste:
    l_input = l_input.lower()
    if l_input not in stopwords:
        l_input_neu.append(l_input)
        
print(l_input_neu)

# Remove Stopwords: (Funktion) = FUNKTIONIERT NICHT

l_input2 = input(" ")
# Text einfügen: Heinz war heute in den Bergen. Es war eine lange Wanderung.

split_liste2 = l_input2.split()

stopwords2 = ["der", "die", "das", "den", "in", "auf", "unter", "ein", "eine", "ist", "war", "es"]

l_input_neu2 = []

def stopword_filter(l_input2, stopwords2):
    l_input2 = l_input2.lower()
    if l_input2 not in stopwords2:
        l_input_neu2.append(l_input)
    else:
        return

stopword_filter(l_input2, stopwords2)
