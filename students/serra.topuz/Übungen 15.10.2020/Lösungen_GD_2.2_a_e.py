# Grundlagen Datentypen: 2.2 Über Dictionaries iterieren

# a) Preisliste ausgeben mit Input
preisliste = {"Brot": 3.2, "Milch": 2.1,
              "Orangen": 3.75,
              "Tomaten": 2.2, "Tee": 4.2,
              "Peanuts": 3.9, "Ketchup": 2.1}

for preis in preisliste:
    print(preis, "kostet", preisliste[preis], "CHF.")
    
    
# e) Lebensmittel mit einem Preis < 2.0 CHF ausgeben

neu_preisliste = {} # neue leere Preisliste/Dictionary

for key, value in preisliste.items():
    if value < 2.0:
        neu_preisliste[key] = value
        
print(neu_preisliste)

# e) An sich gibt es keine Lebensmittel, die weniger als (<) 2.0 CHF kosten
# Beispiel: kleiner (<) als 3.0 CHF

neu_preisliste = {} # neue leere Preisliste/Dictionary

for key, value in preisliste.items():
    if value < 3.0:
        neu_preisliste[key] = value
        
print(neu_preisliste)




