# Listen: Übungsbeispiele 7

# Doppelte Einträge aus Liste entfernen

l_input = ["Gesundheit", "Wanderung", "Gesundheit", "Gewandheit", "Wanderung"]

l_input = list(dict.fromkeys(l_input))

def unique(l_input):
    l_input_neu = []
    for wort in l_input:
        if wort not in l_input_neu:
            l_input_neu.append(wort)
    return l_input_neu

print(unique(l_input))