# Übung: Wörter in Gutenberg.txt zählen

# a) Kleine Übung, wie viele Wörter hat es....

f = open("pg6079.txt", "r", encoding="utf8")

gutenberg = f.read()

words = gutenberg.split()

print("Anzahl aller Wörter im Text:", len(words))

print(gutenberg)

# b) Richtige Aufgabe

occurrences1 = gutenberg.count("Himmel")

occurrences2 = gutenberg.count("Freiheit")

occurrences3 = gutenberg.count("Spiel")

occurrences4 = gutenberg.count("Tanz")

print("Wie oft kommt 'Himmel' im Text vor?", occurrences1)

print("Wie oft kommt 'Freiheit' im Text vor?", occurrences2)

print("Wie oft kommt 'Spiel' im Text vor?", occurrences3)

print("Wie oft kommt 'Tanz' im Text vor?", occurrences4)