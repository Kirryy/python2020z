# Übungsblatt Datentyp: 3.1 Listen von Dictionaries erstellen

# a) Dictionary

datensatz_ana = {"Vorname": "Ana",
                 "Nachname": "Skupch",
                 "Phone": 123}
        
print("----------------------")
    

# b) Dictionary in Liste

telefonbuch = []

telefonbuch = [datensatz_ana]

print(telefonbuch)
    

print("----------------------")


# c) Dictionary in Liste

datensatz_tim = {"Vorname": "Tim",
                 "Nachname": "Kurz",
                 "Phone": 732}

datensatz_julia = {"Vorname": "Julia",
                 "Nachname": "Lang",
                 "Phone": 912}

telefonbuch.append(datensatz_tim)
telefonbuch.append(datensatz_julia)

print(telefonbuch)


print("----------------------")

# c) Dictionary in Liste

telefonbuch_eingabe = {}

datenstruktur = []

while True:
    vorname = input("Geben Sie einen Vornamen ein, oder x zum Beenden: ")
    if vorname.lower() == "x":
        break
    nachname = input("Geben Sie den Nachnamen ein: ")
    phone = input ("Geben Sie die Telefonnummer ein: ")
    telefonbuch_eingabe["Vorname"] = vorname
    telefonbuch_eingabe["Nachname"] = nachname
    telefonbuch_eingabe["Phone"] = phone
    
    datenstruktur.append(telefonbuch_eingabe)
    
print(datenstruktur)