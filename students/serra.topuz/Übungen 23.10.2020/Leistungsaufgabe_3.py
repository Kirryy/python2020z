# Leistungsaufgabe 3

# a) Aufgabe: Städte & Einwohnerzahlen

angaben = {}

while True:
    stadt = input("Geben Sie eine Stadt ein, oder x zum Beenden: ")
    if stadt.lower() == "x":
        break
    einwohnerzahl = input ("Geben Sie die Einwohnerzahl ein: ")
    angaben[stadt] = einwohnerzahl
    
print(angaben)

stadt_einwohnerzahl = []

for stadt, einwohnerzahl in angaben.items():
    stadt_einwohnerzahl.append([stadt, einwohnerzahl])
    stadt_einwohnerzahl.sort()
    
print(stadt_einwohnerzahl)

print("----------------")

# b) Aufgabe: Mini-Deutsch-Englisch Wörterbuch

wörterbuch = {"Tisch": "table",
              "Teppich": "carpet",
              "Teelöffel": "teaspoon",
              "Fernseher": "television",
              "Dusche": "shower",
              "Fenster": "window",
              "Briefkasten": "mailbox",
              "Spiegel": "mirror",
              "Lampe": "lamp",
              "Haushalt": "household"}

ergebnis = 0

for deutsch in wörterbuch:
    english = input("Was heisst " + deutsch + ": ")
    if english == wörterbuch[deutsch]:
        print("RICHTIG!")
        ergebnis = ergebnis + 1
    else:
        print("FALSCH!")

print("Sie haben", ergebnis,"von 10 Vokabeln richtig beantwortet!")