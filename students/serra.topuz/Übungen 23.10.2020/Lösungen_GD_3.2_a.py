# Übungsblatt Datentyp: 3.2 Listen von Dictionaries iterieren

# 3.1 Telefonbuch
datensatz_ana = {"Vorname": "Ana",
                 "Nachname": "Skupch",
                 "Phone": 123}
        
print("----------------------")
    

telefonbuch = []

telefonbuch = [datensatz_ana]

print(telefonbuch)
    

print("----------------------")


datensatz_tim = {"Vorname": "Tim",
                 "Nachname": "Kurz",
                 "Phone": 732}

datensatz_julia = {"Vorname": "Julia",
                 "Nachname": "Lang",
                 "Phone": 912}

telefonbuch.append(datensatz_tim)
telefonbuch.append(datensatz_julia)

print(telefonbuch)


# a) Ausgabe ???

def datensatz_ausgeben():
?

datensatz_ausgeben()
