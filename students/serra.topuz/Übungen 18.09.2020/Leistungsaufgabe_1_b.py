#Leistungsaufgabe 1.a)
from turtle import*
reset()

hideturtle()

#Flaggenteil/rot
def flaggenteil_rot(seitenlaenge, fuellfarbe, zeichenfarbe):
    fillcolor(fuellfarbe)
    pencolor(zeichenfarbe)
    
    begin_fill()
    fd(seitenlaenge);rt(90);fd(seitenlaenge);rt(90);fd(seitenlaenge);rt(90);fd(seitenlaenge)
    end_fill()

flaggenteil_rot(400, "red", "red")



#Vorbereitung Flaggenteil/Kreuz
penup()
rt(90);fd(160);rt(90);fd(80)
pendown()

#Flaggenteil/Kreuz/weiss
def flaggenteil_weiss(seitenlaenge, fuellfarbe, zeichenfarbe):
    fillcolor(fuellfarbe)
    pencolor(zeichenfarbe)

    begin_fill()
    lt(90);fd(seitenlaenge);rt(90);fd(seitenlaenge);lt(90);fd(seitenlaenge);rt(90);fd(seitenlaenge);rt(90);fd(seitenlaenge);
    lt(90);fd(seitenlaenge);rt(90);fd(seitenlaenge);rt(90);fd(seitenlaenge);lt(90);fd(seitenlaenge);rt(90);fd(seitenlaenge);
    rt(90);fd(seitenlaenge);lt(90);fd(seitenlaenge)
    end_fill()

flaggenteil_weiss(80, "white", "white")


