#Leistungsaufgabe 1.a)
from turtle import*
reset()

hideturtle()

#Flaggenteil/rot
fillcolor("red")
pencolor("red")

begin_fill()
fd(400);rt(90);fd(400);rt(90);fd(400);rt(90);fd(400)
end_fill()

#Vorbereitung Flaggenteil/Kreuz
penup()
rt(90);fd(160);rt(90);fd(80)
pendown()

#Flaggenteil/Kreuz/weiss
fillcolor("white")
pencolor("white")

begin_fill()
lt(90);fd(80);rt(90);fd(80);lt(90);fd(80);rt(90);fd(80);rt(90);fd(80);
lt(90);fd(80);rt(90);fd(80);rt(90);fd(80);lt(90);fd(80);rt(90);fd(80);
rt(90);fd(80);lt(90);fd(80)
end_fill()


