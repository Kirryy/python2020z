import random
geschenke=["Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball"]

while True:
    fenster=input("Welche Kalendertür wollen Sie öffnen (oder x für Exit)?")
    if fenster=="x":
        break
    if fenster.isdigit()==False or (int(fenster) <=0 or int(fenster) >= 25):
        print("Bitte eine Zahl zwischen 1 und 24 eingeben")
    else:
        print("Hinter deinem Fenster ist ein",geschenke[random.randint(0, 4)])
