from turtle import *

reset()
seitenlänge = numinput("Eingabefenster", "Bitte Seitenlänge eingeben")
#rotes Quadrat
def flagge(seitenlänge):
    pencolor("red")
    fillcolor("red")
    begin_fill()
    forward(seitenlänge)
    left(90)
    forward(seitenlänge)
    left(90)
    forward(seitenlänge)
    left(90)
    forward(seitenlänge)
    end_fill()

#um die weissen Rechtecke ins rote Quadrat zu bekommen, habe ich dies so gewählt

    home()
    pu()
    forward(seitenlänge / 2.3)
    left(90)
    forward(seitenlänge / 1.7)
    pd()

#zeichnen der weissen Fläche
    fillcolor("white")
    begin_fill()

    left(90)
    forward(seitenlänge / 4)
    left(90)
    forward(seitenlänge / 5.7)
    left(90)
    forward(seitenlänge / 4)
    right(90)
    forward(seitenlänge / 4)
    left(90)
    forward(seitenlänge / 5.7)
    left(90)
    forward(seitenlänge / 4)
    right(90)
    forward(seitenlänge / 4)
    left(90)
    forward(seitenlänge / 5.7)
    left(90)
    forward(seitenlänge / 4)
    right(90)
    forward(seitenlänge / 4)
    left(90)
    forward(seitenlänge / 5.7)
    left(90)
    forward(seitenlänge / 4)

    end_fill()
    hideturtle()
flagge(seitenlänge)
