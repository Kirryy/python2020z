from turtle import *

reset()
#rotes Quadrat
pencolor("red")
fillcolor("red")
begin_fill()
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
end_fill()

#um die weissen Rechtecke ins rote Quadrat zu bekommen, habe ich dies so gewählt
home()
pu()
forward(170)
left(90)
forward(230)
pd()

#zeichnen der weissen Fläche
fillcolor("white")
begin_fill()

left(90)
forward(100)
left(90)
forward(70)
left(90)
forward(100)
right(90)
forward(100)
left(90)
forward(70)
left(90)
forward(100)
right(90)
forward(100)
left(90)
forward(70)
left(90)
forward(100)
right(90)
forward(100)
left(90)
forward(70)
left(90)
forward(100)

end_fill()
hideturtle()

#keine Ahnung, wieso noch ein roter Strich da ist?