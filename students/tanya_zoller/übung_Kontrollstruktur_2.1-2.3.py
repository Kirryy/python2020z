#Übung 2.1
def guten_morgen():
    print("Guten Morgen!")
   
guten_morgen()

#Übung 2.2, Variante 1:

def guten_morgen():
    print("Guten Morgen", "Ana")

guten_morgen()

#Übung 2.2, Variante 2:

def guten_morgen(Ana):
    print("Guten Morgen", "Ana!")
guten_morgen("Ana")

#Übung 2.3 a)


def gruss():
    name = input("Gib einen Namen ein:")
    print("Guten Morgen " + name)

gruss()

#Übung 2.3 b)

def flaeche_rechteck(länge, breite):
    fläche = (länge * breite)
    print("Die Fläche beträgt", fläche, "m2")

flaeche_rechteck(10, 20)