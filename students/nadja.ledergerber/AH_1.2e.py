from turtle import *

reset()

shape("turtle")

pencolor("red")
pensize(5)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

penup() #hebe Stift an
right(120)
pendown() #setze Stift wieder ab

pencolor("blue")
pensize(5)
forward(100)
left(120)
forward(100)
left(120)
forward(100)


left(-60)
pencolor("light green")
pensize(5)
forward(100)
left(-120)
forward(100)
left(-120)
forward(100)
