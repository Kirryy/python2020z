from turtle import *

reset()

size = numinput("Figur", "Bitte Seitenlänge angeben")
int(size)

shape("turtle")

pencolor("red")
pensize(4)
fillcolor("light green")
begin_fill()
left(30)
forward(size)
left(120)
forward(size)
left(120)
forward(size)
end_fill()

penup() #hebe Stift an
right(120)
pendown() #setze Stift wieder ab

pencolor("red")
fillcolor("yellow")
begin_fill()
forward(size*4)
left(120)
forward(size*4)
left(120)
forward(size*4)
end_fill()

left(-60)
pencolor("red")
fillcolor("cyan")
begin_fill()
forward(size*2)
left(-120)
forward(size*2)
left(-120)
forward(size*2)
end_fill()
