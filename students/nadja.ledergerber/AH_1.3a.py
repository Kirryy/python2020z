from turtle import *

reset()

shape("turtle")

pencolor("red")
pensize(4)
fillcolor("light green")
begin_fill()
left(30)
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

penup() #hebe Stift an
right(120)
pendown() #setze Stift wieder ab

pencolor("red")
fillcolor("yellow")
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

left(-60)
pencolor("red")
fillcolor("cyan")
begin_fill()
forward(100)
left(-120)
forward(100)
left(-120)
forward(100)
end_fill()