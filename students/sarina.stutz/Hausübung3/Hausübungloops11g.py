#Autor:Sarina Stutz
#Datum:1.10.2020
#Übungsblatt loops
#Übung 11g, k

obenunten = "# # # # # # #"
leerzeichen = "  " * 5
rautenzeichen = "# "

print (obenunten)
i = 1
while i < 11:
    print (leerzeichen[:-i] + rautenzeichen[:+i])
    i = i + 2
print (obenunten)

j=11
for i in range(1,12,2):
    print(" "*j+i*"#")
    j=j-1



