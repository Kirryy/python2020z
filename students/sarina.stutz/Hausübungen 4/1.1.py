#Autor:Sarina Stutz
#Datum:8.10.2020
#Übungsblatt: basic Data Types
#1.1

#a)
jahreszeiten=["Sommer","Herbst","Winter","Frühling"]
print(jahreszeiten)

#b)
jahreszeiten.remove("Frühling")
print(jahreszeiten)

#c)
jahreszeiten.append("Langas")
print(jahreszeiten)

#d)
namensliste=[]
namenseingabe = 0
while namenseingabe !="x":
    namenseingabe=input("Name: ")
    namensliste.append(namenseingabe)
    if namenseingabe=="x":
        namensliste.remove("x") 
        print(namensliste)
