#Autor:Sarina Stutz
#Datum:7.10.2020
#Übungsblatt loops
#Übung10a

#Programm berechnet Herzfrequenz / Billettpreis nicht???

def berechne_herzfrequenz(alter):
    maximale_herzfrequenz=220-alter
    print (maximale_herzfrequenz)



def berechne_billett_preis(alter,kilometer):
    grundpreis = 2
    entfernung = kilometer * 0.25
    billett_preis = grundpreis + entfernung
    if alter < 6:
        billett_preis = 0
    elif alter < 16:
        billett_preis = billett_preis/2
    print (billett_preis)

eingabe = 1

while eingabe != 0:
    print ("=" * 10)
    print ("Programmübersicht:")
    print ("1 ... Herzfrequenz berechnen")
    print ("2 ... Billettpreis berechnen")
    print (" " * 25)
    print ("0 ... Programm beenden")
    print ("=" * 25)
    print (" ")
    eingabe = int(input("Wählen Sie eine Option: "))
    
    if eingabe == 1:
        print (" ")
        print ("Herzfrequenz berechnen")
        print ("-" * 10)
        alter = int(input("Bitte geben Sie Ihr Alter ein:"))
        print (" ")
        print ("Ihre maximale Herzfrequenz beträgt: ", berechne_herzfrequenz(alter), "Schläge pro Minute.")
        print (" ")
        
    elif eingabe == 2:
        print (" ")
        print ("Preis für eine Fahrkarte berechnen")
        print ("-" * 10)
        alter = int(input("Bitte geben Sie Ihr Alter ein:"))
        kilometer = int(input("Wie viele km möchten Sie reisen?"))
        print (" ")
        print ("Die Fahrkarte kostet: ", berechne_billett_preis(alter,kilometer) , " CHF")
        print (" ")
        
    elif eingabe == 0:
        print(" ")
        print("Programm beendet!")
        print(" ")
        break
    
    else:
      print (" ")
      print ("Option ungültig!")
      print (" ") 




