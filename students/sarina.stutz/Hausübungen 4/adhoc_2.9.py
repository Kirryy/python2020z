#Autor:Sarina Stutz
#Datum:8.10.2020
#Foliensatz 3:Functions and Control Structures
#ad hoc Übung 2.9 b

from turtle import *
reset()
pencolor("navy")
fillcolor("skyblue")

begin_fill()


dreieck = numinput("Eingabe","Wähle die Anzahl Dreicke, die gezeichnet werden sollen:")
zahl = int(dreieck)
for x in range(zahl):
    for i in range(3):
        forward (60)
        left (120)
    penup()
    forward(80)
    pendown()
end_fill()
hideturtle()




