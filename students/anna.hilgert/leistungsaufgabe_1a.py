#Leistungsaufgabe 1a)
#Erstellen Sie die Schweizer Flagge mit der turtle-Grafik und exakt folgendem Aussehen (400x400 Pixel), es darf kein Pfeil/Turtle sichtbar sein!
#Beim weissen Kreuz gibt es keine Vorgaben bzgl. Grösse, es muss aber symmetrisch sein und den gleichen Abstand zu allen Seiten der Flagge aufweisen
#Dokumentieren Sie Ihr Programm mit Kommentarzeilen.

from turtle import *

reset()

def quadrat(sl):#Funktion rotes Quadrat definieren
    fd(sl)
    lt(90)
    fd(sl)
    lt(90)
    fd(sl)
    lt(90)
    fd(sl)

def kreuz(size):#Funktion Kreuz definieren
    fd(size)
    lt(90)
    fd(size)
    rt(90)
    fd(size)
    lt(90)
    fd(size)
    lt(90)
    fd(size)
    rt(90)
    fd(size)
    lt(90)
    fd(size)
    lt(90)
    fd(size)
    rt(90)
    fd(size)
    lt(90)
    fd(size)
    lt(90)
    fd(size)
    rt(90)
    fd(size)

#rotes Quadrat zeichnen
pencolor("red")
fillcolor("red")
begin_fill()
quadrat(400)
end_fill()
#Stift zum Startpunkt der Kreuz-Zeichnung bewegen
home()
penup()
fd(150)
lt(90)
fd(50)
rt(90)
pendown()
#weiss gefülltes Kreuz zeichnen
fillcolor("white")
begin_fill()
kreuz(100)
end_fill()
#Stift unsichtbar machen
hideturtle()


