#leistungsaufgabe 3
#a)
liste = []
while True:
    stadt = input("Geben Sie die Stadt ein (oder x für Stop):")
    if stadt == "x":
        break
    einwohner = int(input("Geben Sie die zugehörige Einwohnerzahl ein:"))
    liste.append([stadt, einwohner])
print(sorted(liste))
    
#b)
ergebnis = []

wörterbuch = {"Haus": "house",
              "Zahnarzt": "dentist",
              "Zahn": "tooth",
              "Stuhl": "chair",
              "Tisch": "table",
              "Dusche": "shower",
              "Blume": "flower",
              "Kerze": "candle",
              "Regal": "shelf",
              "Herbst": "fall"}

for deutsch in wörterbuch:
    abfrage = input("Was heisst "+deutsch+ ":")
    if abfrage == wörterbuch[deutsch]:
        ergebnis.append("Richtig")
        print("RICHTIG!")
        
    else:
        print("FALSCH!")
print("Sie haben", len(ergebnis), "von 10 Vokabeln richtig beantwortet!")

                    
                    