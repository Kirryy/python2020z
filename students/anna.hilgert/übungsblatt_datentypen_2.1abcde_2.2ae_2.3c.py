#übungsblatt datentypen
#dictionaries
#2.1a
preisliste = {"Brot":3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}
print(preisliste)

#2.1b)
preisliste["Milch"] = 2.05
print (preisliste)

#2.1c)
del preisliste["Brot"]
print (preisliste)

#2.1d)
preisliste["Tee"]=4.2
preisliste["Peanuts"]=3.9
preisliste["Ketchup"]=2.1
print (preisliste)

#2.1e)
def dictionary():
    dictionary={}
    while True:
        lebensmittel = input("Welches Lebensmittel soll hinzugefügt werden (oder x für Exit)?")
        if lebensmittel == "x":
            break
        preis = input("Preis:")
        dictionary[lebensmittel] = preis
    return dictionary
print(dictionary())

#2.2a)

for lebensmittel, preis in preisliste.items():
    print (lebensmittel, "kostet", preis, "CHF.")
    
#2.2e) (es hat keine Lebensmittel, die weniger als 2CHF kosten?
pliste_neu = {}
for lebensmittel, preis in preisliste.items():
    if preis < 3.0:
        pliste_neu[lebensmittel] = preis

print(pliste_neu)

#2.3c)
needle = "en"
p_filter = {}
for lebensmittel, preis in preisliste.items():
    if needle not in lebensmittel:
        p_filter[lebensmittel] = preis
print (p_filter)
