#Übungsblatt Grundlagen Kontrollstrukturen
#2.1

def guten_morgen():
    print("Guten Morgen!")
guten_morgen()
    
#2.2

def guten_morgen(name):
    print("Guten Morgen", name)
guten_morgen("Ana!")

#2.3a)

def guten_morgen(name):
    return name
gruss = guten_morgen("Ana!")
print ("Guten Morgen", gruss)


#2.3b)

def berechne_rechteck(laenge, breite):
    flaeche_rechteck=laenge*breite
    return flaeche_rechteck
flaeche=berechne_rechteck(10,20)
print("Diese Fläche beträgt",flaeche,"m2.")

#2.8a)
zählen=0
while zählen<=10:
    print (zählen)
    zählen=zählen+1
    
#2.8c)
zählen=10
while zählen>=0:
    print(zählen)
    zählen=zählen-3
    

    