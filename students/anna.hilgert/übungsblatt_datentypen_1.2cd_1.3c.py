#Übungsblatt Datentypen
#1.2 c)

jahreszeiten = [["Frühling"], ["Sommer"], ["Herbst"], ["Winter"]]

x = 0
while x <= 3:
    print (jahreszeiten[x]," - ", jahreszeiten[x][0][-2:])
    x = x + 1
else:
    exit

#1.2 d)
    
jahreszeiten = [["Frühling"], ["Sommer"], ["Herbst"], ["Winter"]]

x = 0

while x <= 3:
    if jahreszeiten[x][0][-2:] == "er":
        print (jahreszeiten[x]," - ", jahreszeiten[x][0][-2:])
    x = x + 1
else:
    exit
    
#1.3 c)
liste = ["An", "einem", "Tag", "im", "Herbst"]
neue_liste = []
for index in range(len(liste)):
    if len(liste[index]) >= 3:
        neue_liste.append(liste[index])
print (neue_liste)
        