#Übungsblatt: Datentypen
#1.1 a)
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
print (jahreszeiten)

#1.1b)
del jahreszeiten[0]
print (jahreszeiten)

#1.1c)
jahreszeiten = jahreszeiten + ["Langas"]
print (jahreszeiten)

#1.1d)

eingabe = input("Name: ")
liste = []
while eingabe != "x":
    liste = liste + [eingabe]
    eingabe = input("Name: ")
else:
    print("Liste: ", liste)
