#übungsbeispiele dictionaries
#5
dictionary= {}

x = 0
a = 1

while x < 3:
    berg = input(str(a)+". "+"Berg:")
    höhe = int(input(str(a)+". "+"Höhe:"))
    dictionary[berg] = höhe
    a = a + 1
    x = x + 1
for berg, höhe in dictionary.items():
    print(berg, "ist", höhe, "m", "(", int(höhe*3.28), "ft",")", "hoch.")

#6
# dictionary= {}
# angaben = {}
# 
# x = 0
# a = 1
# 
# while x < 3:
#     berg = input(str(a)+". "+"Berg:")
#     höhe = int(input(str(a)+". "+"Höhe:"))
#     gebirge = input(str(a)+". "+"Gebirge")
#     angaben = {"Höhe": höhe, "Gebirge": gebirge}
#     dictionary[berg] = angaben
#     a = a + 1
#     x = x + 1
# 
# sortieren_nach = int(input("Sortieren nach (1) Berg, (2) Höhe oder (3) Gebirge:"))
# # if sortieren_nach == 1:
#     aufoderab = int(input("Nach Berg (1) aufsteigend, oder (2) absteigend sortieren?:"))
#     if aufoderab == 1:
#         for berg, angaben in sorted(dictionary.items()):
#             höhe, gebirge = angaben
#             print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")
# # wie sortiert man alphabetisch absteigend? zuerst sort und dann mit reverse umdrehen?
# #aber geht das bei dictionaries?
# 
# ab hier weiss ich nicht mehr weiter

# sort_höhe = {}
# if sortieren_nach == 2:
#     aufoderab = int(input("Nach Höhe (1) aufsteigend, oder (2) absteigend sortieren?:"))
#     if aufoderab == 1:
#         for berg, angaben in dictionary.items():
#             höhe, gebirge = angaben
#             sort_höhe[angaben] = berg
#         for angaben, berg in sorted(sort_höhe.items()):
#             print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")

# 
# # if sortieren_nach == 3:
# #         aufoderab = input("Nach Gebirge (1) aufsteigend, oder (2) absteigend sortieren?:")
# # for berg, liste in dictionary.items():
# #     print(berg, "ist", höhe, "m hoch und gehört zum", gebirge, "Gebirge.")
# #   
#     
#     
# 7
ausgabeliste = []
eingabetext = ("David Mayer von der HTW hat um 9:00 seine Wohnung in Chur verlassen.")
eingabeliste = eingabetext.lower().split()
blacklist = ["david", "mayer", "htw", "chur"]
for begriff in eingabeliste:
    if begriff in blacklist:
        ausgabeliste.append(len(begriff)*"*")
    if begriff not in blacklist:
        ausgabeliste.append(begriff)
ausgabe = " ".join(ausgabeliste)

print(ausgabe)
        
        