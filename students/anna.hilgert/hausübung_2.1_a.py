#Hausübung ad-hoc Übung 2.1 a
# 2.1a) drei Dreiecke sollen mittels Funktionen realisiert werden

from turtle import *

reset()

pensize(5)
pencolor("red")
def dreieck():
    fd(100)
    lt(120)
    fd(100)
    lt(120)
    fd(100)
    
lt(30)    
fillcolor("green")
begin_fill()
dreieck()
end_fill()

fillcolor("cyan")
begin_fill()
dreieck()
end_fill()

fillcolor("yellow")
begin_fill()
dreieck()
end_fill()
