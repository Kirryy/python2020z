#leistungsaufgabe 5
#text statistiken
#1.

from urllib.request import urlopen

resultat_wörter = {}
resultat_buchstaben = {}
alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l",
            "m","n","o","p","q","r","s","t","u","v","w","x","y","z"]

ressource = input("URL oder Dateiname eingeben")
if "https://" in ressource:
    with urlopen(ressource) as source:
        web_content = source.read().decode("utf8")
        web_content_wörter = web_content.lower().split()
        anz_wörter_gesamt = len(web_content_wörter)
        print(anz_wörter_gesamt)
        for wort in web_content_wörter:
            anz = web_content_wörter.count(wort)
            resultat_wörter[wort] = anz
        print (resultat_wörter)

        web_content_buchstabe = web_content.lower().replace("ö","oe").replace("ü","ue").replace("ä","ae")
        for buchstabe in web_content_buchstabe:
            if buchstabe in alphabet:
                anz = web_content_buchstabe.count(buchstabe)
                resultat_buchstaben[buchstabe] = anz
        resultat = sorted(resultat_buchstaben.items())
        print(resultat)
else:
    with open(ressource, encoding="utf8") as f:
        content = f.read()
        content_wörter = content.lower().split()
        anz_wörter_gesamt = len(content_wörter)
        print(anz_wörter_gesamt)
        for wort in content_wörter:
            anz = content_wörter.count(wort)
            resultat_wörter[wort] = anz
        print (resultat_wörter)
        
        content_buchstabe = content.lower().replace("ö","oe").replace("ü","ue").replace("ä","ae")
        for buchstabe in content_buchstabe:
            if buchstabe in alphabet:
                anz = content_buchstabe.count(buchstabe)
                resultat_buchstaben[buchstabe] = anz
        resultat = sorted(resultat_buchstaben.items())
        print(resultat)
    
from csv import writer
with open("wort.csv","w",encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for wort,anz in resultat_wörter.items():
        csv_writer.writerow([wort,anz])
    
from csv import writer
with open("buchstabe.csv","w",encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for buchstabe,anz in resultat_buchstaben.items():
        csv_writer.writerow([buchstabe,anz])
