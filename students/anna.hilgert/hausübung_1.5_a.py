#Hausübung ad-hoc Übung 1.5 a
#1.5 a) drei Dreiecke über ein Eingabefenster erstellen

from turtle import *

reset()

pensize(5)
pencolor("red")

size=numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
fillcolor("cyan")
begin_fill()
fd(size)
lt(120)
fd(size)
lt(120)
fd(size)
end_fill()

size=numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
fillcolor("yellow")
begin_fill()
fd(size)
lt(120)
fd(size)
lt(120)
fd(size)
end_fill()

size=numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
fillcolor("green")
begin_fill()
fd(size)
lt(120)
fd(size)
lt(120)
fd(size)
end_fill()