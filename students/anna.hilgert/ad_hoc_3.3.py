#ad hoc 3.3
#a)
städte = {"Zürich":370000, "Genf":190000,
"Basel":170000, "Bern":130000}

for stadt, einwohner in städte.items():
    print (stadt, "hat", einwohner, "Einwohner")

#a1)
städte["Chur"] = 30000

#a2)
while True:
    stadt = input("Welche Stadt soll hinzugefügt werden (oder x für Exit)?")
    if stadt == "x":
        break
    einwohnerzahl = input("Einwohnerzahl:")
    städte[stadt] = einwohnerzahl
    
for stadt, einwohner in städte.items():
    print (stadt, "hat", einwohner, "Einwohner")

#b)
for stadt, einwohner in sorted(städte.items()):
    print (stadt, "hat", einwohner, "Einwohner")

#c)

dictionary = {}
for stadt, einwohner in städte.items():
    dictionary[einwohner] = stadt

print(sorted(dictionary.items()))

    
