#einkaufsliste
from csv import writer
from csv import reader
optionen = [["(1) artikel hinzufügen"],
            ["(2) artikel löschen"],
            ["(3) artikel suchen"],
            ["(4) einkaufsliste leeren"],
            ["(5) einkaufsliste speichern"],
            ["(6) einkaufsliste laden"],
            ["(7) einkaufsliste im csv format exportieren"],
            ["(0) exit"]]

einkaufsliste = []
artikel = []
suche = []

while True:
    option = input(optionen)
    if option == "1":
        artikel_name = input("Welcher Artikel? (oder x für exit)")
        artikel_menge = input("Menge?")
        artikel_preis = input("Preis?")
        artikel = [artikel_name.lower(),artikel_menge,artikel_preis]
        einkaufsliste.append(artikel)
        
    if option == "2":
        artikel_löschen = input("Welchen Artikel löschen?")
        if artikel_name == artikel_löschen.lower():
            einkaufsliste.remove(artikel)
            print(einkaufsliste)

    if option == "3":
        print(einkaufsliste)
        artikelsuche = input("Suchbegriff?")
        if artikelsuche.lower() in artikel_name:
            suche.append(artikel)
        print(suche)
        
    if option == "4":
        print(einkaufsliste)
        while len(einkaufsliste) != 0:
            einkaufsliste.pop()
        print(einkaufsliste)
        
    if option == "5":
        with open("einkaufsliste.txt", "w", encoding="utf8") as f:
            f.write(str(einkaufsliste))
            
    if option == "6":
        with open("einkaufsliste.txt", encoding = "utf8") as f:
            f.read()
            print(einkaufsliste)
    
    if option == "7":
        with open ("einkaufsliste.csv", "w", encoding="utf8") as f:
            csv_writer = writer(f, delimiter=";")
            for zeile in einkaufsliste:
                csv_writer.writerow(zeile)   
    
    if option == "0":
        exit
        
