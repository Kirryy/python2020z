#3.1
#a
dictionary = {"Vorname": "Ana",
              "Nachname": "Skupch",
              "Phone":"123"}
print(dictionary)

#b
telefonbuch = []
telefonbuch.append(dictionary)
print (telefonbuch)

#c
dictionary = {"Vorname": "Tim",
              "Nachname": "Kurz",
              "Phone":"732"}
telefonbuch.append(dictionary)
dictionary = {"Vorname": "Julia",
              "Nachname": "Lang",
              "Phone":"912"}
telefonbuch.append(dictionary)
print(telefonbuch)

#d
dictionary = {}
while True:
    vname = input ("Wie lautet der Vorname: (oder x für beenden)")
    if vname == "x":
        print(telefonbuch)
        break    
    dictionary["Vorname:"] = vname
    nname = input ("Wie lautet der Nachname:")
    dictionary["Nachname:"] = nname
    phone = input ("Wie lautet die Telefonnummer:")
    dictionary["Telefonnummer:"] = phone
    telefonbuch.append(dictionary)
      
    
#3.2a

def datensatz_ausgeben(datensatz):
    for index, eingabe in datensatz.items():
        print (index+": "+eingabe)
print(datensatz_ausgeben(telefonbuch[0]))
#warum wird hier am Schluss NONE ausgegeben?

# 3.3a: entfernen aller phones mit 1

telefonbuch_neu = []
for datensatz in telefonbuch:
    if "1" not in datensatz["Phone"]:
        telefonbuch_neu.append(datensatz)
print(telefonbuch_neu)
        




