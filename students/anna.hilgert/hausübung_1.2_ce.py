#Hausübung ad-hoc Übung 1.2 c, e
#1.2 c) Strichdicke=5,Farben: blue, red, cyan, black

from turtle import *

reset()

pensize(5)
pencolor("blue")
lt(45)
fd(100)
pencolor("red")
rt(90)
fd(100)
pencolor("cyan")
lt(90)
fd(100)
pencolor("black")
rt(90)
fd(100)

#1.2 e) drei, gleichseitige Dreicke, berühren sich alle drei an einer Ecke, eines mit roter, eines mit blauer und eines mit grüner Linie, alle unausgefüllt

from turtle import *

reset()

def dreieck():
    fd(100)
    lt(120)
    fd(100)
    lt(120)
    fd(100)
    lt(120)

pensize(5)
pencolor("red")
dreieck()
lt(120)
pencolor("blue")
dreieck()
lt(120)
pencolor("green")
dreieck()