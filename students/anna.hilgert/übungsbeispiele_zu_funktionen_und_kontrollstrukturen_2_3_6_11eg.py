#Übungsbeispiele zu Funktionen und Kontrollstrukturen
#2.
def berechne_herzfrequenz(age):
    return(220-age)
print(berechne_herzfrequenz(25))

#3.

def get_billet_preis(age, distance):
    if age<6:
        return "0"
    if age<16:
        return (2+distance*0.25)/2
    if age>=16:
        return 2+distance*0.25
print(get_billet_preis(30,200))

#6.

from turtle import*
reset()

def nachInnen(breite):
    penup()
    fd(breite)
    lt(90)
    fd(breite)
    rt(90)
    pendown()


def zeichne_rahmen(länge,breite,rahmenfarbe,füllfarbe):
    pencolor(rahmenfarbe)
    fillcolor(füllfarbe)
    begin_fill()
    i=1
    while i<=4:
        fd(länge)
        lt(90)
        i=i+1
    end_fill()
    
    nachInnen(breite)
    
    fillcolor(füllfarbe)
    begin_fill()
    i=1
    while i<=4:
        fd(breite*2)
        lt(90)
        i=i+1
    end_fill()
    
zeichne_rahmen(100,25,"blue","green")
    
#11.e)
raute = "# "
leer = "  "
print(raute*7)
i=5
while i>1:
    print(raute+leer*5+raute)
    i=i-1
else:
    print(raute*7)

#11.g)
raute="# "
leer="  "*5
print(raute*7)
i=10
while i>1:
    print(leer[:+i] + raute)
    i=i-2
else:
    print(raute*7)



  
    
    

