from random import randint, choice

# warum replace nicht funktioniert
#  "u-baum".replace("u", "a") --> "a-baam".replace("a", "o") --> o-boom

sl_dict = {"a": "o", "o": "e", "i": "u", "u": "a", "e": "i"}
woerter_liste = ["apfel", "baum", "torte", "haus"]

raetsel_wort = woerter_liste[randint(0, len(woerter_liste))] # wähle ein wort aus woerter_liste at random, welches dann umgewandelt wird
raetsel_wort = choice(woerter_liste)

frage_wort = ""
for buchstabe in raetsel_wort:   # ['t', 'o', 'r', 't', 'e']
    if buchstabe in sl_dict:
        frage_wort = frage_wort + sl_dict[buchstabe]
    else:
        frage_wort = frage_wort + buchstabe
        
    print(frage_wort)
        

print("Gegeben ist das Wort:", raetsel_wort)
eingabe = input("Raten Sie wie das ursprüngliche Wort hiess: ")

if eingabe == raetsel_wort:
   print("Korrekt!")
else:
   print("Falsch, bitte versuchen Sie es erneut...")

