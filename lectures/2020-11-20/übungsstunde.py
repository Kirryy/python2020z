# Programmierlogiken
#
# Übungsstunde vom 20. November 2020
#

# CSV
# JSON

l = [("Apfel", 3.92), ("Birnen", 4.50), ("Bananen", 1.90)]


from json import loads, dumps
with open("liste.json", "w", encoding="utf8") as f:
    json_string = dumps(l)
    f.write(json_string)