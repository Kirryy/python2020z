# Programmierlogiken
#
# Übungsstunde vom 20. November 2020
#

# CSV
# JSON

from csv import writer

l = [("Apfel", 3.92),
     ("Birnen", 4.50),
     ("Bananen", 1.90)]

with open("liste2.csv", "w", encoding="utf-8") as f:
    csv_liste = writer(f, delimiter=";")
    for zeile in l:
        print(zeile)
        csv_liste.writerow(zeile)
        
    # csv_liste.writerow(l[0])
    # csv_liste.writerow(l[1])
    # csv_liste.writerow(l[2])