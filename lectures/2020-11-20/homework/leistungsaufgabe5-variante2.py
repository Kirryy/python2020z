from urllib.request import urlopen

resultat_wörter = {}
resultat_buchstaben = {}
alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l",
            "m","n","o","p","q","r","s","t","u","v","w","x","y","z"]

ressource = input("URL oder Dateiname eingeben")
if "https://" in ressource:
    with urlopen(ressource) as source:
        web_content = source.read().decode("utf8")
        web_content_wörter = web_content.lower().split()
        anz_wörter_gesamt = len(web_content_wörter)
        print(anz_wörter_gesamt)
        
        # wörter gezählt
        for wort in web_content_wörter:
            if wort in resultat_wörter:
                resultat_wörter[wort] = resultat_wörter[wort] + 1
            else:
                resultat_wörter[wort] = 1

        print (resultat_wörter)

        web_content_buchstabe = web_content.lower().replace("ö","oe").replace("ü","ue").replace("ä","ae")
        for buchstabe in web_content_buchstabe:
            if buchstabe in resultat_buchstaben:
                resultat_buchstaben[buchstabe] = resultat_buchstaben[buchstabe] + 1
            else:
                resultat_buchstaben[buchstabe] = 1
                
        resultat = sorted(resultat_buchstaben.items())
        print(resultat)