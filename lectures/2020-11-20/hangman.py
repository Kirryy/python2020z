# Aufgabe: gegen ist eine Liste von Wörtern:
#
# (1) wählen Sie mittels choice zufällig ein wort aus
# (2) ersetzen sie alle buchstaben durch "."
# (3) der Benutzer wird nun nach einem buchstaben gefragt - wenn der buchstabe im ursprünglichen wort
#     vorkommt, wird er angezeigt.
# (4) ziel ist es, das ursprüngliche wort zu eraten

# vorgangsweise:
# - schreiben sie eine funktion, die alle buchstaben, bis auf die bereits geratenen, in "." übersetzt.
#
# beispiel:
# - wort: apfel / geratene buchstaben: []
#   => Das Wort lautet ..... - welchen Buchstaben soll ich zeigen: i
# - wort: apfel / geratene buchstaben: ["i"]
#   => Das Wort lautet ..... - welchen Buchstaben soll ich zeigen: e
# - wort: apfel / geratene buchstaben: ["i", "e"]
#   => Das Wort lautet ...e. - welchen Buchstaben soll ich zeigen: a
# - wort: apfel / geratene buchstaben: ["i", "e", "a"]
#   => Das Wort lautet a..e. - welchen Buchstaben soll ich zeigen: l
# - wort: apfel / geratene buchstaben: ["i", "e", "a", "l]
#   => Das Wort lautet a..el - welchen Buchstaben soll ich zeigen: ...

# bis 11:41 --> 

from random import choice

woerter_liste = ["apfel", "baum", "torte", "haus"]


nicht_austausch=["a", "e"]
ratewort=choice(woerter_liste)

print(ratewort)
fragewort=""
for buchstabe in ratewort:
    if buchstabe in nicht_austausch:
        fragewort = fragewort + buchstabe
    else:
        fragewort = fragewort + "."
    
    print(fragewort)
