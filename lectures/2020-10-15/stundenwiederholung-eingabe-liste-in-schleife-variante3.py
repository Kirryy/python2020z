# Exkurs: der strip-befehl

text = "   Ana  mag  Jim   "
print("+++" + text.strip() + "+++")



liste = []

while True:   # robustheit gegen gross-/kleinschreibung
    name = input("Geben Sie einen Namen ein (oder 'x' oder nichts für Ende)?")
    if name.lower() == "x" or name.strip() == "":
        break
    
    liste.append(name)
    
print(liste)



