personen = [["Jim",18],
            ["Zoe", 25],
            ["Paul",30],
            ["Susi",18],
            ["Ana",18],
            ["Tom",18],
            ]
 
# aufgabenstellung: entfernen sie alle personen aus der liste,
# die jünger als 20 jahre sind....

# bis 14:47
unveränderte_liste = list(personen)
for person in unveränderte_liste:
    if person[1] < 20:
        personen.remove(person)
        
print(personen)

# variante
resultat = []  # list()
for name, alter in personen:
  if alter >=20:
    resultat.append([name,alter])
    
print(resultat)