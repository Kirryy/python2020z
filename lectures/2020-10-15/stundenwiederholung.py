# Stundenwiederholung vom 15. Oktober 2020

liste = ["Martina" , "Klaus" , "Anna", "Jim"]
liste.append("Urs")

print(liste)

for x in liste:
    print("Guten Tag", x)
    
print(liste[1])
a = "Klaus"
a = "Toni"

liste[1] = "Toni"
print(liste)

print(liste[-2:])
del liste[-2:]     # alternativ: liste[3:5]
print(liste)

liste.remove("Toni")
print(liste)

# wie kann ich überprüfen, ob "Martina" in der Liste vorkommt
print(liste.count("Martina"))
print(liste.index("Martina"))
print("Martina" in liste)
print("Ju" in "Juli")

# print(liste.index("Julius"))

