personen = [["Jim",18],
            ["Zoe", 25],
            ["Paul",30]
            ]
 
print(personen)

print(personen[1][1])
print("Happy Birthday")
personen[1][1] = personen[1][1] + 1
print(personen)

#
a = 25
a = a + 1
a += 1
print(a)
personen[1][1] += 1

# machen Paul älter
personen[2][1] = personen[2][1] + 1
print(personen)

# alternativ in zwei schritten
paul = personen[2]
print(paul)
paul[1] = paul[1] + 1
print(personen)

# Ausgabe aller Personen mit zugehörigem Alter
#   Jim ist 18 Jahre alt.  -> 5 min bis 14:27

# variante 1
for i in range(len(personen)):
    print(personen[i][0], "ist", personen[i][1], "alt")
    
# variante 2
for person in personen:
    print (person[0], "ist", person[1], "Jahre alt")
    

print("---")
# variante 3
for name, alter in personen:
    print(name, "ist", alter, "alt")
    
    
# 