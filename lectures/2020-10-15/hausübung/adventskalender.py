
#Übungsaufgabe Listen
#Beispiel 3
from random import choice
auswahlliste = ["Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball"]

eingabe=0

adventskalender = []
for tuerchen in range (24):
    zufallselement = choice(auswahlliste)
    adventskalender.append(zufallselement)

while eingabe != "x":
    eingabe= input("Welche Kalendertür wollen Sie öffnen (oder x für Exit): ")
    if eingabe == "x":
        break
    eingabe=int(eingabe)
    if eingabe in range (1,25):
        print(adventskalender[eingabe-1])
    else:
        print("Bitte geben Sie eine Zahl zwischen 1 und 24 an! ")
    


