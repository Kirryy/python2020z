
d = {"Zürich": 370000, "Bern": 170000}

# Einwohner für Zürich
print(d["Zürich"])

# Indices können Sie selbst vergeben
d2 = {0: 370000, 1: 170000}
print(d2[0])

d3 = {0: [370000, "Zürich"], 1: 170000, 99: 991023}
print(d3[0][1])
print(d3[99])


d = {"Zürich": 370000, "Bern": 170000}
d["Zürich"] = 370001
print(d)

# hinzufügen von Werten
d["Chur"] = 37000
print(d)