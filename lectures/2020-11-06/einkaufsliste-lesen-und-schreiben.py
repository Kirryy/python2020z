# Aufgabe:
#  (1) Speichern Sie die folgende Einkaufsliste im CSV Format ab (Datei: Einkaufsliste.csv)
#  (2) probieren Sie die Datei in Excel zu öffnen
#  (3) Erweitern Sie Ihr Programm, sodass dieses
#       (a) die Einkaufsliste einliesst, wenn die Datei Einkaufsliste.csv existiert; ansonsten soll die Liste leer sein.
#       (b) die List im Anschluss in einer Schleife ausgibt.
#  (4) Erstellen Sie auch eine Version, wo dies mit mittels CSV sondern mittles JSON basiert... (Einkaufsliste.json)

einkaufsliste = [['Eis', 2.33, 2],
                 ['Eier', 0.33, 10],
                 ['Äpfel', 3.99, 2]]


from csv import writer

with open("einkaufsliste.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter = ";")
    
    for zeile in einkaufsliste:
        print(zeile)
        csv_writer.writerow(zeile)
    
    
# Unterschied: read/write
# variable = f.read()         # ich lese etwas ein und erhalte ein ergebnis; dieses ergebnis wird der variable zugeordnet
# f.write(variable)           # variable enthält den wert, der geschrieben werden soll!

