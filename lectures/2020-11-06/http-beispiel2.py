from urllib.request import urlopen

with urlopen("https://www.gutenberg.org/ebooks/6079.txt.utf-8") as source:
    web_content = source.read().decode("utf8")
    
    print(web_content)

    gutenberg = web_content.split()

    occurrences1 = gutenberg.count("Winter")

    occurrences2 = gutenberg.count("Schnee")

    occurrences3 = gutenberg.count("kalt")


print("Wie oft kommt 'Winter' im Text vor?", occurrences1)

print("Wie oft kommt 'Schnee' im Text vor?", occurrences2)

print("Wie oft kommt 'kalt' im Text vor?", occurrences3)

