ueberschreibung = {}
eingabetext = input("Wie lautet ihr Eingabetext?").lower()
eingabetext = eingabetext.split()
blacklist = input("Was soll auf die Blacklist?").lower()
blacklist = blacklist.split(", ")

ausgabe = []
for wort in eingabetext:
    if wort in blacklist:
        ausgabe.append("*"*len(wort))
    else:
        ausgabe.append(wort)

satz = " ".join(ausgabe)
print ("Ausgabe:", satz)
