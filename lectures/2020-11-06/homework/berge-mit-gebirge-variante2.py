dictionary= {}
angaben = {}

# x = 0
# a = 1
# 
# while x < 3:
#     berg = input(str(a)+". "+"Berg:")
#     höhe = int(input(str(a)+". "+"Höhe:"))
#     gebirge = input(str(a)+". "+"Gebirge")
#     angaben = {"Höhe": höhe, "Gebirge": gebirge}
#     dictionary[berg] = angaben
#     a = a + 1
#     x = x + 1

def get_hoehe(item):
    return item[1]["Höhe"]

def get_gebirge(item):
    return item[1]["Gebirge"]


dictionary = {"Everest": {"Höhe": 8848, "Gebirge": "Him"},
              "Arlberg": {"Höhe": 2000, "Gebirge": "Alpen"},
              "ZCalander": {"Höhe": 2500, "Gebirge": "Alpen"}}

sortieren_nach = int(input("Sortieren nach (1) Berg, (2) Höhe oder (3) Gebirge:"))
aufoderab = int(input("Nach Berg (1) aufsteigend, oder (2) absteigend sortieren?:"))
if aufoderab == 1:
    reverse = False
else:
    reverse = True

if sortieren_nach == 1:
    for berg, angaben in sorted(dictionary.items(), reverse=reverse):
        höhe, gebirge = angaben
        print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")
            
elif sortieren_nach == 2:
    for berg, angaben in sorted(dictionary.items(), key=get_hoehe):
        print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")
    
        
elif sortieren_nach == 3:
    sortierliste = []
    for berg, angaben in sorted(dictionary.items(), key=get_gebirge):
        print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")

