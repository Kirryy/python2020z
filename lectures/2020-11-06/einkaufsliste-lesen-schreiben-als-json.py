from json import loads, dumps

meine_einkaufsliste = [['Eis', 2.33, 2],
                       ['Eier', 0.33, 10],
                       ['Äpfel', 3.99, 2]]

with open("einkaufsliste.json", "w", encoding="utf8") as f:
    json_string = dumps(meine_einkaufsliste)
    f.write(json_string)
    
    
# wieder einlesen
with open("einkaufsliste.json", "r", encoding="utf8") as f:
    json_string = f.read()
    einkaufsliste2 = loads(json_string)
    
print(einkaufsliste2)
