# der split befehl zerlegt einen string in eine liste, welche per default
# anhand der whitespaces zerlegt wird.
l = "das ist ein schöner tag".split()

# alternativ können sie das zeichen (oder die worte) anhand von denen getrennt werden soll angeben
ll = "12,3,4,1,2".split(",")
print(ll)

# das funktioniert auch mit worten
lll = "Ana, Skupch and Tom, Kurz and Martin, Long".split(" and ")
print(lll)

print(l[0])
# letzten zwei listenelemente
print(l[3:5])
print(l[-2:])

# Vergleich bei Strings
s = "Donaudampfschiffsfahrt"
print(s[-2:])

# Ändern sie tag auf morgen...
l[4] = "morgen"
print(l)

# löschen sie morgen
del l[4]
print(l)

lange_liste = l + lll + ["ist"]
print(lange_liste)
print(len(lange_liste))

# der del befehl löscht ein element basierend auf dessen position
del lange_liste[3]
print(lange_liste)

# remove dagegen löscht ein element basierend auf dessen wert (!)
lange_liste.remove("ist")
print(lange_liste)

print("------------")
# verschachtelte liste
addressbuch =  [
                ["Ana", "Ringstrasse 34", "7000 Chur", ["081 123", "081 234"]],
                ["Tom", "Straussengasse 2", "1060 Wien", ["081 123", "081 234"]],
                ["Jim", "Langstrasse 12", "7000 Chur", ["081 123", "081 237"]]
               ]
# wie bekomme ich, die stasse in welcher Ana wohnt?
ana = addressbuch[0]
print(ana)
print(ana[1], addressbuch[0][1])

# erste telefonnummer von jim
print(addressbuch[2][3][0])

