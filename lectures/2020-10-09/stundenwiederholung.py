# Stundenwiederholung vom 9. Oktober 2020
# - geschachtelte Schleifen
# - Datenstrukturen (String, Listen)

# Wir wollen gratulieren: Ana, Zoe, Tom, Jim
# Zu was wollen wir gratulieren
# - zum Geburtstag - "Happy birthday dear Ana"
# - zu Weihnachten
# - zu Ostern
# - wünschen schöne Ferien
#

year = 2020
while year<2031:
    for person in "Ana", "Zoe", "Tom", "Jim":
        for gruss in "Happy Birthday", "Schöne Weihnachten", "Frohe Ostern", "Schöne Ferien":
            print(gruss, person, year)
    year = year + 1   

for year in range(2020, 2031):
    for person in "Ana", "Zoe", "Tom", "Jim":
        for gruss in "Happy Birthday", "Schöne Weihnachten", "Frohe Ostern", "Schöne Ferien":
            print(gruss, person, year)


for name in " Ana ", " Zoe ", " Tom ", " Jim ":
    for gratulation in "Happy birthday dear", "Schöne Weihnachten","Frohe Ostern", "Schöne Ferien":
        for jahr in range (2020,2031):
            print(gratulation + name + str(jahr))
        