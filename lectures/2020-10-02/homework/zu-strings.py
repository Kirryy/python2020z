# Jim told Zoe: "Ana's friend lives overseas".

s = """Jim told Zoe: "Ana's friend lives overseas"."""

# beispiele mit enumerate

for no, zahl in enumerate(range(8, 12)):  # [8, 9, 10, 11]
    print(no, zahl)
    
# zählen mit count
text = "Eingabe"
print(text.count("e"), "liefert 1, da der Vergleich case sensitive ist.")

text = text.lower() # wandelt das text in die entsprechende lowercase darstellung um
print(text)
print(text.count("e"), "liefert 2, da es nur noch kleinbuchstaben gibt.")

# alternativ schreibweise
print(text.lower().count("e"))

print("Church in Chur".lower().count("c"))