#author sarah brandenberger
#date 30.09.2020
#hausuebung 3

from turtle import *
from math import *
from random import randint


#ad hoc 2.6 c.)
a = 12
b = 23
c = 5
d = 56
e = 77
f = 18
g = 9

for zahl in (a, b, c, d, e, f, g):
    if a >= zahl:
        print("Die grösste Zahl ist: ", a)
    elif b >= zahl:
        print("Die grösste Zahl ist: ", b)
    elif c >= zahl:
        print("Die grösste Zahl ist: ", c)
    elif d >= zahl:
        print("Die grösste Zahl ist: ", d)
    elif e >= zahl:
        print("Die grösste Zahl ist: ", e)
    elif f >= zahl:
        print("Die grösste Zahl ist: ", f)
    elif g >= zahl:
        print("Die grösste Zahl ist: ", g)


for zahl in (a, b, c, d, e, f, g):
    if a <= zahl:
        print("Die kleinste Zahl ist: ", a)
    elif b <= zahl:
        print("Die kleinste Zahl ist: ", b)
    elif c <= zahl:
        print("Die kleinste Zahl ist: ", c)
    elif d <= zahl:
        print("Die kleinste Zahl ist: ", d)
    elif e <= zahl:
        print("Die kleinste Zahl ist: ", e)
    elif f <= zahl:
        print("Die kleinste Zahl ist: ", f)
    elif g <= zahl:
        print("Die kleinste Zahl ist: ", g)

for zahl in (a, b, c, d, e, f, g):
    print("Der Durchschnitt ist: ", zahl/7)

