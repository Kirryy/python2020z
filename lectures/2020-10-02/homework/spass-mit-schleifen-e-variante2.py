# variante 2
for no in range(8):
    if no == 0 or no == 7:
        print("# " * 7)
    else:
        print("# " + "  " * 5 + "#")
        
print("---")
# variante 3

print("# " * 7)
for no in range(6):
    print("# " + "  " * 5 + "#")
print("# " * 7)