# Stundenwiederholung vom 2. Oktober 2020

# Schleifen

i=1
while i <= 4:
     print(i)
     i = i + 1
    
print("Ich bin das erste Print ausserhalb der Schleife")


#
for zahl in range(1, 5):  # [1, 2, 3, 4]
    print(zahl)

print("Ich bin das erste Print ausserhalb der Schleife")

#
# Bitte nummerieren Sie alle Buchstaben des Wortes "Morgen!" durch.
#
# Ausgabe:
#  M-0
#  o-1
#  r-2
#  g-3
#  ....
#  -> 7 min bis 8:55

runde = 1
for buchstabe in "Morgen!":
    print(buchstabe + "-" + str(runde))
    runde = runde + 1
    
print("---")

for no, buchstabe in enumerate("Morgen!"):
    if buchstabe != "!":
        print(buchstabe + "-" + str(no+1))
    else:
        print(buchstabe)
    