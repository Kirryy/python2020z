# Stundenwiederholung vom 25. September 2020
#
def guten_morgen():
    print("Guten Morgen")
  
guten_morgen()

# guten morgen mit namen
#
def guten_morgen2(Name):
    return "Guten Morgen " + Name
  
# ausgabe von return-werten mittels

# option 1: direkte ausgabe mittels print
print(guten_morgen2("Ana"))

# options 2: über eine variable
gruss = guten_morgen2("Ana")
print(gruss)

def guten_morgen3(Name, zeit):
    if zeit <= 11:
        return "Guten Morgen " + Name
    else:
        print("x")
        return "Guten Tag " + Name

gruss = guten_morgen3("Ana", 18)
print(gruss)


def guten_morgen3(Name, zeit):
    if zeit <= 11:
        return "Guten Morgen " + Name
    elif zeit <= 16:
        return "Guten Nachmittag " + Name
    elif zeit <= 19:
        return "Guten Abend " + Name
    else:
        return "Gute Nacht " + Name
    
gruss = guten_morgen3("Ana", 8)
print(gruss)
print(guten_morgen3("Ana", 5))
print(guten_morgen3("Ana", 12))
print(guten_morgen3("Ana", 17))
print(guten_morgen3("Ana", 22))

# 
# wiederholungen / schleifen
#
print("Schleifen....")
i = 10
while i < 15:
    print(i)
    i = i + 1

