# Datum:24.09.2020
#Autor:Sarina Stutz
#ad hoc Übung 2.4
def notendurchschnitt(namen, note1, note2, note3, note4, note5, note6):
    note1 = float(note1)
    note2 = float(note2)
    note3 = float(note3)
    note4 = float(note4)
    note5 = float(note5)
    note6 = float(note6)

    notenschnitt = round(((note1 + note2 + note3 + note4 + note5 + note6)/6), 2)
    #round 2) Ergebnis auf 2 Nachkommastellen runden

    return namen + ", Notendurchschnitt: "+ str(notenschnitt)


print(notendurchschnitt("Max Mustermann", 4, 5.5, 6, 4, 5, 4.5))