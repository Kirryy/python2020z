from turtle import *

# quadrat
reset()
forward(100)
left(90)
forward (100)
left(90)
forward(100)
left(90)
forward(100)

# weiter zur nächsten zeichnung
penup()
forward(200)
right(270)
pendown()

# und jetzt das dreieck
pensize(5)
pencolor("red")
forward(100)
left(120)
forward(100)
left(120)
forward(100)
