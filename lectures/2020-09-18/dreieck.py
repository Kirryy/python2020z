from turtle import *

seitenlänge = 200

def dreieck(seitenlänge, zeichenfarbe, füllfarbe):
    # bereite den stift vor
    pencolor(zeichenfarbe)
    fillcolor(füllfarbe)
    
    # zeichnen des dreiecks
    begin_fill()
    right(120)
    forward(seitenlänge)
    right(120)
    forward(seitenlänge)
    right(120)
    forward(seitenlänge)
    end_fill()
    
# dreieck() # nur mit den klammern wird die funktion aufgerufen
# dreieck
a = 7*12

# aufruf des dreiecks
dreieck(seitenlänge, 'red', 'yellow')
dreieck(seitenlänge/2, 'blue', 'green')
dreieck(seitenlänge/4, 'cyan', 'orange')
