#übungen 1.2
from turtle import *

#a
reset()
forward(90)
left(90)
forward(90)
left(90)
forward(90)
left(90)
forward(90)

#übergang zur nächsten übung
reset()

#c
pensize(5)
pencolor("blue")
left(50)
forward(150)
pencolor("red")
right(100)
forward(150)
pencolor("cyan")
left(100)
forward(150)
pencolor("black")
right(100)
forward(150)

reset()
#d
pensize(5)
pencolor("red")
forward(100)
left(120)
forward(100)
left(120)
forward(100)
left(90)

reset()
#e
pensize(5)

# erste Dreieck
pencolor("blue")
left(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

# zweite Dreieck
pencolor("red")
forward(100)
left(120)
forward(100)
left(120)
forward(100)

# dritte Dreieck
pencolor("lime")
forward(100)
left(120)
forward(100)
left(120)
forward(100)

# #baum zeichnen
# reset()
# shape("turtle")
# 
# # Baumkrone
# pensize(5)
# pencolor("black")
# fillcolor("green")
# begin_fill()
# circle (100)
# end_fill()
# 
# # Stamm
# pencolor("black")
# fillcolor("brown")
# begin_fill()
# forward(20)
# right(90)
# forward(300)
# right(90)
# forward(40)
# right(90)
# forward(300)
# right(90)
# forward(20)
# end_fill()
# hideturtle()
