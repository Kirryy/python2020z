from turtle import *

#ad hoc Übung 1.3


#b)
pensize(5)

#Viereck cyan
pencolor("red")
fillcolor("cyan")
begin_fill()
right(140)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

#Viereck gelb
fillcolor("yellow")
begin_fill()
left(200)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()

#Viereck pink
fillcolor("magenta")
begin_fill()
left(190)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()

#Viereck blau
fillcolor("blue")
begin_fill()
left(100)
forward(100)
left(90)
forward(100)
left(90)
forward(100)
end_fill()

#Viereck grün
penup()
left(90)
forward(100)
pendown()
fillcolor("lime")
begin_fill()
left(110)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)
end_fill()

