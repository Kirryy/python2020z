# Ermöglichen Sie es Name und Alter in einer Schleife einzugeben. Diese sollen in
# einem Dictionary gespeichert werden.
# Am Ende sollten Sie dieses noch ausgeben
# - 09:57

personen = {}
while True:
    name = input("Geben Sie einen Namen an? ")
    if name.lower() == "x":
        break
    alter = input("Geben Sie das Alter an, oder x? ")
    personen[name]=alter
    
print(personen)

