l_input = "Der Tag begann sehr gut! Der Morgen war schoen."

def word_stat(eingabe):
    eingabe=eingabe.split()  # 
    häufigkeit = {}
    for wort in eingabe:     # 'Der', 'Tag', 'begann', ...
        wort = wort.lower()
        if wort not in häufigkeit:
            häufigkeit[wort]=0 
        häufigkeit[wort] = häufigkeit[wort] + 1 
    print(häufigkeit)

word_stat(l_input)