l_input = "Heinz war heute in den Bergen. Es war eine lange Wanderung"

stopwords = ("der", "die", "das", "in", "auf", "unter", "ein", "eine",
             "ist", "war", "es", "den")

def stopword_filter(l_input, stopwords):
    l_input_neu = []
    for word in l_input.split():
        if word not in stopwords and word.lower() not in stopwords:
            l_input_neu.append(word)
    return l_input_neu
        
print(stopword_filter(l_input, stopwords))
