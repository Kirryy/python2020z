# Erstellen Sie ein Dictionary, welches folgendes speichert:
# zu jeder Person
#  - Name: Ana Skupch
#  - Geb: 26.11.2001
#  - TelefonNr: +41 81 123
#
# Definieren Sie drei Beispieldatensätze und geben Sie diese im in einer
# Schleife wie Anschluss wie oben aus

#telefonbuch = {'ana': "26.11.2001", ...}

telefonbuch = {"Ana Skupch": {"geb": "26.11.2001",
                              "phone": "+41 81 123"},
               "Zoe Müller": {"geb": "25.11.2001",
                              "phone": "+41 81 321"},
               "Tim Meier": {"geb": "22.11.2005",
                             "phone": "+41 81 007",
                             "email": "t@maier.ch"}
               }

for name, dictionary in telefonbuch.items():
    print ("Name:", name, "Geb:", dictionary["geb"], "TelefonNr:", dictionary["phone"])
    

