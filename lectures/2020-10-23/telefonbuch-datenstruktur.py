# Erstellen Sie ein Dictionary, welches folgendes speichert:
# zu jeder Person
#  - Name: Ana Skupch
#  - Geb: 26.11.2001
#  - TelefonNr: +41 81 123
#
# Definieren Sie drei Beispieldatensätze und geben Sie diese im in einer
# Schleife wie Anschluss wie oben aus

#telefonbuch = {'ana': "26.11.2001", ...}

telefonbuch = {"Ana Skupch": ["26.11.2001", "+41 81 123"],
               "Zoe Müller": ["25.11.2001", "+41 81 321"],
               "Tim Meier": ["22.11.2005", "+41 81 007"]
               }

for name, liste in telefonbuch.items():
    datum, tel = liste
    print ("Name:", name, "Geb:", datum, "TelefonNr:", tel)
    
    
