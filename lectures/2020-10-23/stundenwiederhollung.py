# Stundenwiederholung vom 23. Oktober 2020

text = "hallo"

personen = {"ana":37, "zoe":25, "tim":22}
print(personen["tim"])
print(text[1])

print(personen["tim"]+1)

a = 23
a = a + 1
personen["tim"] = 23
personen["tim"] = personen["tim"] + 1

personen["susi"] = 13
# löschen
del personen["zoe"]

print(personen)

# aufgabe: Liste aller Personen + alter
#   Ana ist 37
#   Zoe ist 25
#   Tim ist 22
# bis 9:44
# option 1
for name, alter in personen.items():
    print (name, "ist", alter)

print("--")
# option 2
for name in personen:
    print(name, "ist", personen[name])

print("--")

for name, alter in sorted(personen.items()):
    print (name, "ist", alter)
# sorted(personen) -> zu spät, die schleife ist vorbei/ausgegeben

# geben sie das dictionary sortiert nach dem alter aus....
# 10:07 -> Herr Ritter

for alter in personen.values():
    print(alter)
    
